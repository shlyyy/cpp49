#include <map>
#include <set>
#include <iostream>
using std::cout;
using std::endl;
using std::map;
using std::set;
using std::string;

void test0()
{
    //map中不能存放关键字相同的元素
    //map中的元素默认情况下按升序进行排列
    //因为其底层实现还是红黑树
    map<string, string> cities{
        {"027", "武汉"},
        {"010", "北京"},
        {"021", "苏州"},
        {"021", "上海"},  //在初始化时，这条没有存进去
        {"0755", "深圳"},
    };

    for(auto & elem : cities){
        cout << elem.first << " --> "
             << elem.second << endl;
    }
    cout << endl;

    //map支持下标访问运算符,时间复杂度为O(logN)
    //与其底层实现有关，不是O(1)
    cout << cities["021"] << endl;//发现只有1条记录

    cities["021"] = "上海";//如果key存在，就会修改value的值
    cout << cities["021"] << endl;

    cities["0518"] = "连云港";//添加一个新元素
    cities["0530"] = "山东菏泽曹县";
    //注意：当key如果没有时，进行读操作会直接创建一条记录
    cout << cities["0728"] << endl;
     
    for(auto & elem : cities){
        cout << elem.first << " --> "
             << elem.second << endl;
    }
    cout << endl;

    //添加新元素还可以使用insert
    std::pair<map<string, string>::iterator, bool>
        //ret = cities.insert({"0571", "杭州"});
        ret = cities.insert({"022", "杭州"});
    if(ret.second) {
        cout << "插入成功" << endl;
    } else {
        cout << "插入元素失败" << endl;
    }
    cout << ret.first->first << " --> "
         << ret.first->second << endl;

    cout << "\n遍历所有元素:" << endl;
    for(auto & elem : cities){
        cout << elem.first << " --> "
             << elem.second << endl;
    }
    cout << endl;
}

void test1()
{
    //容器本身可以混合使用
    map<string, set<int>> words;
    words["abandon"].insert(1);
    words["abandon"].insert(2);
    words["abandon"].insert(3);
    words["amazing"].insert(4);
    words["amazing"].insert(5);
    words["amazing"].insert(6);

    for(auto & elem : words) {
        cout << elem.first << ":";
        for(auto & line : elem.second) {
            cout << line << " ";
        }
        cout << endl;
    }
}

int main()
{
    /* test0(); */
    test1();
    return 0;
}

