#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    Base(int base = 0)
    : _base(base)
    {   cout << "Base()" << endl;   }

    void print() const 
    {   cout << "Base::_base:" << _base << endl;    }

    ~Base() {   cout << "~Base()" << endl;  }

private:
    long _base;
};

class Derived
: public Base
{
public:
    Derived(long base1, long derived)
    : Base(base1) 
    , _derived(derived)
    {   
        cout << "Derived()" << endl;    
    }


    void display() const
    {
        print();
        cout << "Derived::_derived:" << _derived << endl;
    }

    ~Derived() {    cout << "~Derived()" << endl;   }

private:
    long _derived;
};

void test0()
{
    Derived d(1, 2);
    d.display();
}


int main()
{
    test0();
    return 0;
}

