#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point() = default;
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;}
        
    void print() const
    {   
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }
//对派生类开放其访问权限, 不能在类之外直接访问
protected:
    int _ix = 10;
private:
    int _iy;//私有成员只能在本类内部访问
};

//派生类一旦继承了基类，那基类的所有非静态数据成员都会变成派生类的一部分
class Point3D
: public Point//吸收基类成员
{
public:
    Point3D(int ix, int iy, int iz)
    : Point(ix, iy)
    , _iz(iz)
    {   cout << "Point3D(int,int,int)" << endl; }

private:
    int _iz;//添加自己新的成员
};

void test0()
{
    Point pt(3, 4);
    //cout << pt._ix << endl;//error

    cout << "sizeof(Point3D):" << sizeof(Point3D) << endl;
    Point3D pt3d(5, 6, 7);
    pt3d.print();//当采用公有继承时，只有基类的公有成员可以由派生类对象直接调用
    //cout << pt3d._ix << endl;
    //构造函数不能被继承
    //pt3d.Point(11, 12);//error
}


int main()
{
    test0();
    return 0;
}

