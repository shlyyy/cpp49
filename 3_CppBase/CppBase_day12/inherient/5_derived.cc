#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    Base()
    : _base(0)
    {   cout << "Base()" << endl;   }

    void print() const 
    {   cout << "Base::_base:" << _base << endl;    }

private:
    long _base;
};

class Derived
: public Base
{
    //1. 当派生类没有显式定义构造函数时，系统会提供一个默认构造函数
    //那么在初始化基类部分时，会自动调用基类的默认构造函数

};

void test0()
{
    Derived d;
    d.print();
}


int main()
{
    test0();
    return 0;
}

