#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
    A() {   cout << "A()" << endl;  }
    void print() const {cout << "A::print()" << endl;}
};

class B
{
public:
    B() {   cout << "B()" << endl;  }
    void display() const {cout << "B::display()" << endl;}
};

class C
{
public:
    C() {   cout << "C()" << endl;  }
    void show() const {cout << "C::show()" << endl;}
};

//默认继承权限是private的
//多重继承时，每一个基类的继承都要声明继承权限
class D
: public A
, public B
, public C
{
public:
    //基类的初始化的顺序与其在派生类构造函数的初始化列表中的顺序无关
    //只与其被继承时的顺序有关
    D()
    : B()
    , A()
    , C()
    {}
};

void test0()
{
    D d;
    d.print();
    d.display();
    d.show();
}


int main()
{
    test0();
    return 0;
}

