#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    //放在public区域的成员称为该类对外提供的接口、功能、服务
    Point() = default;
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;}
        
    void print() const
    {   
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }
//对派生类开放其访问权限, 不能在类之外直接访问
protected:
    int _ix = 10;

    int gety() const {  return _iy; }
private:
    int _iy;//私有成员只能在本类内部访问
};

//public继承又称为接口继承
class Point3D
: public Point//吸收基类成员
{
public:
    Point3D(int ix, int iy, int iz)
    : Point(ix, iy)
    , _iz(iz)
    {   cout << "Point3D(int,int,int)" << endl; }

    void display() const
    {
        cout << "(" << _ix //在派生类内部是protected型
             //<< "," << _iy //在派生类内部无法直接访问
             << "," << gety()//在派生类内部是protected型
             << "," << _iz
             << ")" << endl;
    }

private:
    int _iz;//添加自己新的成员
};

void test0()
{
    Point3D pt3d(5, 6, 7);
    pt3d.print();
}


int main()
{
    test0();
    return 0;
}

