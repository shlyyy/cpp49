#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
    A() {   cout << "A()" << endl;  }
    void print() const {cout << "A::print()" << endl;}
};

class B
{
public:
    B() {   cout << "B()" << endl;  }
    void print() const {cout << "B::print()" << endl;}
};

class C
{
public:
    C() {   cout << "C()" << endl;  }
    void print() const {cout << "C::print()" << endl;}
};

class D
: public A
, public B
, public C
{
public:
    D()
    {   cout << "D()" << endl;  }
};

void test0()
{
    D d;
    //问题1：成员名冲突的二义性
    //解决方案：通过作用域限定符来解决
    //d.print();//error
    d.A::print();
    d.B::print();
    d.C::print();
}


int main()
{
    test0();
    return 0;
}

