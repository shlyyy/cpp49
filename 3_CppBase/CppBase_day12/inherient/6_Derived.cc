#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    Base(int base = 0)
    : _base(base)
    {   cout << "Base()" << endl;   }

    void print() const 
    {   cout << "Base::_base:" << _base << endl;    }

private:
    long _base;
};

class Derived
: public Base
{
public:
    //1. 当派生类有显式定义构造函数时，系统会提供一个默认构造函数
    //那么在初始化基类部分时，会自动调用基类的默认构造函数
    //2. 如果不希望基类部分的初始化再调用默认构造函数，就必须要
    //在派生类构造函数的初始化列表中显式调用基类相应的构造函数
    Derived(long base, long derived)
    : Base(base) //对于基类的初始化是直接通过类名完成
    , _derived(derived)
    {   cout << "Derived()" << endl;    }

    void display() const
    {
        print();
        cout << "Derived::_derived:" << _derived << endl;
    }

private:
    long _derived;
};

void test0()
{
    Derived d(1, 2);
    d.display();
}


int main()
{
    test0();
    return 0;
}

