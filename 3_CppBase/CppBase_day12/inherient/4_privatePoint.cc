#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point() = default;
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;}
        
    void print() const
    {   
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }
//对派生类开放其访问权限, 不能在类之外直接访问
protected:
    int _ix;

    int gety() const {  return _iy; }
private:
    int _iy;//私有成员只能在本类内部访问
};

class Point3D
: private Point//吸收基类成员
{
public:
    Point3D(int ix, int iy, int iz)
    : Point(ix, iy)
    , _iz(iz)
    {   cout << "Point3D(int,int,int)" << endl; }

    void display() const
    {
        //print();//ok
        cout << "(" << _ix //在派生类内部是private型
             //<< "," << _iy //在派生类内部无法直接访问
             << "," << gety()//在派生类内部是private型
             << "," << _iz
             << ")" << endl;
    }

private:
    int _iz;//添加自己新的成员
};

class Point4D
: public Point3D
{
public:
    void show() const
    {
        //cout << _ix << endl;//error
    }
};

void test0()
{
    Point3D pt3d(5, 6, 7);
    //当采用private继承时，基类的非私有成员在派生类内部都是private型
    //pt3d.print();//error  
}


int main()
{
    test0();
    return 0;
}

