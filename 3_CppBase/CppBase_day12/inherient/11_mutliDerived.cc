#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
    A() {   cout << "A()" << endl;  }
    void print() const 
    {   cout << "A::print() _a:" << _a << endl; }

private:
    long _a;
};

class B
: virtual public A
{
public:
    B() {   cout << "B()" << endl;  }

private:
    long _b;
};

class C
: virtual public A
{
public:
    C() {   cout << "C()" << endl;  }

private:
    long _c;
};

class D
: public B
, public C
{
public:
    D()
    {   cout << "D()" << endl;  }
private:
    long _d;
};

void test0()
{
    D d;
    //问题2：存储二义性问题
    //解决方案：通过虚拟继承来解决
    d.print();//error
    //d.B::print();//ok
    //d.C::print();//ok
    cout << "sizeof(d):" << sizeof(d) << endl;
}


int main()
{
    test0();
    return 0;
}

