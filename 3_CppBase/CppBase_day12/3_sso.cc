#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::string;

void test0()
{
    int * pint = new int(1);
    string s1("helloworldaaaaa");//字符串内容与对象都在栈上
    string s2("helloworldaaaaab");
    string s3("helloworldaaaaabccccccccccccccccccccc");

    cout << "s1:" << s1 << endl;
    cout << "s2:" << s2 << endl;
    printf("s1's content address: %p\n", s1.c_str());
    printf("s2's content address: %p\n", s2.c_str());
    printf("pint: %p\n", pint);
    cout << "&s1:" << &s1 << endl;
    cout << "&s2:" << &s2 << endl;
    cout << "sizeof(s1):" << sizeof(s1) << endl;
    cout << "sizeof(s2):" << sizeof(s2) << endl;
    cout << "sizeof(s3):" << sizeof(s3) << endl;

    string s4 = s1;//在栈上开空间速度要比在堆上快
    cout << "s4:" << s4 << endl;
    printf("s4's content address: %p\n", s4.c_str());

    //并不能保证是多线程安全的
    string s5 = s2;//在堆上，采用的是深拷贝
    cout << "s5:" << s5 << endl;
    printf("s5's content address: %p\n", s5.c_str());
}


int main()
{
    test0();
    return 0;
}

