//#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::string;

void test0()
{
    //调用不同的构造函数创建c++风格字符串
    string s1;
    string s2(3, 'a');
    string s3("hello");

    cout << "s1:" << s1 << endl;
    cout << "s2:" << s2 << endl;
    cout << "s3:" << s3 << endl;
    //获取字符串的长度
    cout << "s2.size(): " << s2.size() << endl;
    cout << "s3.length():" << s3.length() << endl;

    //遍历每一个字符
    for(size_t idx = 0; idx < s3.size(); ++idx) {
        cout << s3[idx] << endl;
    }
    cout << endl;

    //增强for循环
    // (元素 ： 容器)
    //
    // auto的含义: 自动推导
    // & 在这里表示引用符号
    // 当去掉引用符号时，已经对元素本身进行了复制
    for(auto & ch : s3){
        //ch = ch + 1;
        cout << ch << " ";
    }
    cout << endl;
    cout << "s3:" << s3 << endl;

    //比较的用法   == !=  <  > 
    if(s3 != "Hello") {
        cout << " s3 不等于  hello" << endl;
    }
}

void test1()
{
    //string = const char *
    string s1 = "hello";
    string s2("world");

    
    //把一个c++风格的字符串转换成了C风格的字符串
    const char * pstr = s2.c_str();
    cout << "pstr:" << pstr << endl;

    //目前来看，迭代器就是指针, 指向的是容器中某一个元素的位置
    auto it = s2.begin();
    cout << "*it:" << *it << endl;
    cout << "s2:" << s2 << endl;

    //迭代器的遍历方式
    for(it; it != s2.end(); ++it) {
        cout << *it << endl;
    }
    cout << endl;
}

void test2()
{
    string s1("hello");
    string s2("world");

    //拼接操作
    string s3 = s1 + s2 + "aaa";
    cout << "s3:" << s3 << endl;

    s1 += s2;
    cout << "s1:" << s1 << endl;
    //append
    s1.append(s2);
    cout << "s1:" << s1 << endl;
    s1.append("bbb");
    cout << "s1:" << s1 << endl;

    //截取子串
    string s4 = s1.substr(2,3);
    cout << "s4:" << s4 << endl;

    //查找字符串
    //string::size_type pos = s3.find("world");
    size_t pos = s3.find("world");
    cout << "pos:" << pos << endl;

    //insert添加操作
    s4.insert(1, 5, 'c');
    cout << "s4:" << s4 << endl;

    //删除字符
    s4.erase(1, 5);
    cout << "s4:" << s4 << endl;
}



int main()
{
    /* test0(); */
    /* test1(); */
    test2();
    return 0;
}

