#include <iostream>
using std::cout;
using std::endl;

void test0(){
    int * p = new int();
    cout << *p << endl;
    delete p;
    p = nullptr;

    int * p1 = new int(3);
    cout << *p1 << endl;
    delete p1;
    p1 = nullptr;

    cout << "*************" << endl;
    int * p2 = new int[5]();
    for(int i = 0;i<5;++i){
        cout << p2[i] << endl;
    }
    delete [] p2;
    p2 = nullptr;
}

int main(void){
    test0();
    return 0;
}
