#include <iostream>
using std::cout;
using std::endl;

void swap(int & a,int & b){ //int & a = x
    int tmp = a;
    a = b;
    b= tmp;
}

void test0(){
    int number = 4;
    int & ref = number;

    cout << &number << endl;
    cout << &ref << endl;

    int x = 300,y = 500;
    cout << x <<"," << y << endl;
    swap(x,y);
    cout << x <<"," << y << endl;
    


}
int x = 300;
int y = 600;

int main(void){
    int & ref1 = x;
    int & ref2 = y;
    swap(ref1,ref2);
    cout << x << "," << y << endl;
    //test0();
    return 0;
}
