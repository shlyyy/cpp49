#include <iostream>
using std::cout;
using std::endl;

namespace wd
{
int number = 1;

void print(){
    cout << "hello" << endl;
}
}//end of namespace wd

namespace cpp
{
int number = 1;

void print(int x){
    cout << "hello" << endl;
}

}//end of namespace cpp

using wd::number;
using wd::print;

using cpp::print;
void test0(){
    print(4);


}

int main(void){
    test0();
    return 0;
}
