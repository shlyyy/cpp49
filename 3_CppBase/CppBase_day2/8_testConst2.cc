#include <iostream>
using std::cout;
using std::endl;

void test0(){
    int number = 1,number2 = 2;
    //const 在*左边，代表常量指针，指向一个常量的指针，不能够修改其值
    const int * p1 = &number;
    //*p1 = 100;
    p1 = &number2;

    int const * p2 = &number;
    //*p2 = 100;
    p2 = &number2;

    //const在*右边时，代表指针常量，const point,它的const属性体现在指针的属性上
    //不能改变指向，但可以改变所指的值
    int * const p3 = &number;
    *p3 = 100;
    //p3 = &number2;

}

int main(void){
    test0();
    return 0;
}
