#include <iostream>
using std::cout;
using std::endl;

int add(int x,int y,int z=7){
    return x + y + z;
}

int add(int x,int y){
    return x + y;
}


void test0(){
    int a = 2,b = 4,c = 8;
    cout << add(a,b,c) << endl;
}

int main(void){
    test0();
    return 0;
}
