#include <iostream>
using namespace std;

namespace wd
{
int number = 1;
 
void display(){
    cout << "hello" << endl;
}

namespace cpp
{
int number = 2;

void display(){
    cout << "world" << endl;
}

}//end od namespace cpp
}//end of namespace wd


/* using wd::number; */
/* using wd::display; */
void test0(){
    cout << wd::number << endl;
    cout << wd::cpp::number << endl;
}

/* using cpp::number; */
/* void test1(){ */
/*     cout << number << endl; */
/* } */

int main(void){
    test0();
    return 0;
}
