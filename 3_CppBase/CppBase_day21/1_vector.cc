#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(in = 0, int = 0)" << endl;
    }

    ~Point()
    {}

private:
    int _ix;
    int _iy;
};

void test0()
{
    vector<Point> number;
    number.push_back(Point(1, 2));
    number.emplace_back(3, 4);
}

void test()
{
    cout << "sizeof(vector<int>) = " << sizeof(vector<int>) << endl;
    cout << "sizeof(vector<long>) = " << sizeof(vector<long>) << endl;

    cout << endl;
    vector<int> number = {1, 3, 5, 7, 9, 6, 8, 4};
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector尾部进行插入与删除" << endl;
    number.push_back(12);
    number.push_back(13);
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;
    number.pop_back();
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    //Q:为什么没有提供在头部进行插入与删除的方法？
    //A:vector中的元素是连续的，如果在头部插入一个元素或者删除
    //一个元素，会导致后面所有的元素都需要挪动位置，那么时间
    //复杂度O(N)
    //
    //Q:获取vector中第一个元素的地址
    &number;//error
    number[0];//ok
    &*number.begin();//ok
    int *pdata = number.data();//ok

    cout << endl << "在vector任意位置进行插入与删除" << endl;
    vector<int>::iterator it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    it = number.begin();
    ++it;
    ++it;
    number.insert(it, 111);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    number.insert(it, 5, 33);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl;
    //迭代器已经失效了（因为底层发生了扩容，it指向的是老的空间）
    vector<int> vec = {12, 34, 56, 78};
    it = number.begin();
    ++it;
    ++it;
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

#if 1
    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    /* it = it + 2; */
    cout << "*it = " << *it << endl;
    number.insert(it, {111, 222, 333, 444});
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;
#endif

    cout << endl << "vector清空元素" << endl;
    number.clear();//将元素清空
    number.shrink_to_fit();//回收多余的空间
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

