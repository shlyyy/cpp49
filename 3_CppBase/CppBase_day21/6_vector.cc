#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    vector<int> number = {1, 3, 5, 7, 9, 6, 8, 4};
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector尾部进行插入与删除" << endl;
    number.push_back(12);
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector任意位置进行插入与删除" << endl;
    vector<int>::iterator it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 111);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    //因为push_back每次插入元素的个数是固定的，所以按照2倍进行
    //扩容是没有问题的，但是insert操作，每次插入的元素的个数
    //是不一定,所以不能有统一的扩容方法
    //size() = m = 10, capacity() = n = 16, 插入的元素的个数t
    //1、t < n - m时,剩余的容量比要插入的元素大，就不会扩容
    //2、n - m < t < m时,会按照2 * m进行扩容
    //3、n - m < t, m < t < n时,会按照 t + m进行扩容
    //4、n - m < t, n < t时,会按照 t + m进行扩容
    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    number.insert(it, 200, 33);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl;
    //迭代器已经失效了（因为底层发生了扩容，it指向的是老的空间）
    vector<int> vec = {12, 34, 56, 78};
    it = number.begin();
    ++it;
    ++it;
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

