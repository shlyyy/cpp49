#include <iostream>
#include <list>

using std::cout;
using std::endl;
using std::list;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

template <typename T>
struct CompareList
{
    bool operator()(const T &lhs, const T &rhs) const
    {
        cout << "struct CompareList" << endl;
        return lhs < rhs;
    }
};

template <>
struct CompareList<int>
{
    bool operator()(const int &lhs, const int &rhs) const
    {
        cout << "struct CompareList<int>" << endl;
        return lhs < rhs;
    }
};


void test()
{
    list<int> number = {1, 3, 5, 7, 9, 3, 3, 6, 8, 4};
    display(number);


    cout << endl << "测试list的reverse函数"<< endl;
    number.reverse();
    display(number);

    cout << endl << "测试list的sort函数" << endl;
    /* number.sort(); */
    /* std::less<int> les; */
    /* number.sort(les); */
    /* number.sort(std::less<int>()); */
    /* number.sort(std::greater<int>()); */
    number.sort(CompareList<int>());
    display(number);

    cout << endl << "测试list的unique函数"<< endl;
    number.unique();
    display(number);

    cout << endl << "测试list的merge函数" << endl;
    /* number.sort(std::greater<int>()); */
    list<int> number2 = {2, 77, 99, 88};
    number2.sort();//将number2先排序
    /* number2.sort(std::greater<int>());//将number2先排序 */
    number.merge(number2);
    display(number);
    display(number2);

    cout << endl << "测试list的splice函数" << endl;
    list<int> number3 = {111, 333, 777, 999};
    auto it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    auto it3 = number3.end();
    --it3;
    cout << "*it3 = " << *it3 << endl;
    /* number.splice(it, number3); */
    /* display(number); */
    /* display(number2); */
    number.splice(it, number3, it3);
    display(number);
    display(number3);

    cout << endl;
    auto it2 = number3.begin();
    cout << "*it2 = " << *it2 << endl;
    it3 = number3.begin();
    ++it3;
    ++it3;
    cout << "*it3 = " << *it3 << endl;
    number.splice(it, number3, it2, it3);
    display(number);
    display(number3);

    cout << endl;
    auto it4 = number.begin();
    cout << "*it4 = " << *it4 << endl;
    auto it5 = number.end();
    --it5;
    cout << "*it5 = " << *it5 << endl;
    number.splice(it4, number, it5);
    display(number);
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

