#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    vector<int> number = {1, 3, 3, 3, 3, 5, 6, 8, 2};
    /* vector<int> number = {1, 3, 2, 3, 4, 3, 6, 3, 5, 6, 8, 2}; */
    display(number);

    for(auto it = number.begin(); it != number.end(); ++it)
    {
        //不能将所有满足条件的3删除掉(不能删除连续的3)
        if(3 == *it)
        {
            number.erase(it);
        }
    }
    display(number);
}

void test2()
{
    vector<int> number = {1, 3, 3, 3, 3, 5, 6, 8, 2};
    /* vector<int> number = {1, 3, 2, 3, 4, 3, 6, 3, 5, 6, 8, 2}; */
    display(number);

    for(auto it = number.begin(); it != number.end(); )
    {
        //不能将所有满足条件的3删除掉(不能删除连续的3)
        if(3 == *it)
        {
            number.erase(it);
        }
        else
        {
            ++it;
        }
    }
    display(number);
}
int main(int argc, char **argv)
{
    test2();
    return 0;
}

