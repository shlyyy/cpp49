#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(in = 0, int = 0)" << endl;
    }

    ~Point()
    {}

private:
    int _ix;
    int _iy;

};
int main(int argc, char **argv)
{
    return 0;
}

