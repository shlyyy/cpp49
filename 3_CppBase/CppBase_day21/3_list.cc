#include <iostream>
#include <list>
#include <vector>

using std::cout;
using std::endl;
using std::list;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    list<int> number = {1, 3, 5, 7, 9, 6, 8, 4};
    display(number);

    cout << endl << "在list尾部进行插入与删除" << endl;
    number.push_back(12);
    number.push_back(13);
    display(number);
    number.pop_back();
    display(number);

    cout << endl << "在list头部进行插入与删除" << endl;
    number.push_front(100);
    number.push_front(200);
    display(number);
    number.pop_front();
    display(number);

    cout << endl << "在list任意位置进行插入与删除" << endl;
    list<int>::iterator it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 111);
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    number.insert(it, 5, 33);
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    vector<int> vec = {12, 34, 56, 78};
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    number.insert(it, {111, 222, 333, 444});
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl << "list清空元素" << endl;
    number.clear();//将元素清空
    cout << "number.size() = " << number.size() << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

