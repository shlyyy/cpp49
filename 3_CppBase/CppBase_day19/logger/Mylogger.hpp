#ifndef __WD_Mylogger_HPP__ 
#define __WD_Mylogger_HPP__
//预处理操作是针对于一个*.cc文件的，
//当头文件在一个*.cc文件中出现多次时
//只会有一份Mylogger的源码出现

//keep your code clean
#include <log4cpp/Category.hh>

namespace wd
{

class Mylogger
{
public:
    static Mylogger * getInstance();
    static void destroy();

    void error(const char * msg); 

    template <class... Args>
    void error(const char * msg, Args... args)
    {
        //Category对象本身就是支持可变参数的
        _mycat.error(msg, args...);
    }

    void warn(const char * msg);

    template <class... Args>
    void warn(const char * msg, Args... args)
    {
        _mycat.warn(msg, args...);
    }

    void info(const char * msg);

    template <class... Args>
    void info(const char * msg, Args... args)
    {
        _mycat.info(msg, args...);
    }

    void debug(const char * msg);
    template <class... Args>
    void debug(const char * msg, Args... args)
    {
        _mycat.debug(msg, args...);
    }

private:
    Mylogger();
    ~Mylogger();

private:
    log4cpp::Category & _mycat;//引用成员
    //log4cpp::OstreamAppender * _pos;
    static Mylogger * _pInstance;
};

#define LogError(msg, ...) wd::Mylogger::getInstance()->error(addprefix(msg), ##__VA_ARGS__)
#define LogWarn(msg, ...) wd::Mylogger::getInstance()->warn(addprefix(msg), ##__VA_ARGS__)
#define LogInfo(msg, ...) wd::Mylogger::getInstance()->info(addprefix(msg), ##__VA_ARGS__)
#define LogDebug(msg, ...) wd::Mylogger::getInstance()->debug(addprefix(msg), ##__VA_ARGS__)

#define addprefix(msg) string("[")\
    .append(__FILE__).append(":")\
    .append(__FUNCTION__).append(":")\
    .append(std::to_string(__LINE__)).append("] ")\
    .append(msg).c_str()



}//end of namespace wd
#endif

