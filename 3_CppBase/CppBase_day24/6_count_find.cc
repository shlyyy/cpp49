#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std::endl;
using std::count;
using std::find;
using std::vector;
using std::for_each;

void test()
{
    vector<int> number = {1, 3, 6, 3, 9, 8, 3, 5, 3};
    for_each(number.begin(), number.end(), 
             [](int value){ 
             cout << value << "  ";
             });
    cout << endl;

    size_t cnt = count(number.begin(), number.end(), 3);
    cout << "cnt = " << cnt << endl;

    /* auto it = find(number.begin(), number.end(), 9); */
    vector<int>::iterator it = find(number.begin(), number.end(), 9);
    if(it != number.end())
    {
        cout << "该元素存在vector中 " << *it << endl;
    }
    else
    {
        cout << "该元素不在vector中" << endl;
    }
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

