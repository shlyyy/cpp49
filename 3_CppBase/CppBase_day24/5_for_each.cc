#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

using std::cout;
using std::endl;
using std::vector;
using std::for_each;
using std::ostream_iterator;
using std::copy;

void func(int &value)
{
    ++value;
    cout << value << "  ";
}

void test()
{
    int a = 10;
    vector<int> number = {1, 3, 9, 7, 6};
    /* for_each(number.begin(), number.end(), func); */
    //lambda表达式,匿名函数
    //[]，捕获列表
    /* for_each(number.begin(), number.end(), [&a](int &value){ */
    /* for_each(number.begin(), number.end(), [](int &value) mutable ->void{ */
    for_each(number.begin(), number.end(), [](int &value) {
             /* cout << a << endl; */
             /* ++a; */
             ++value;
             cout << value << "  ";
             });
    cout << endl;
    copy(number.begin(), number.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;
    cout << a << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}








