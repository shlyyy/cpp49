#include <iostream>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::vector;
using std::reverse_iterator;

void test()
{
    vector<int> number = {1, 3, 5, 7, 9, 2, 4, 6, 8};
    vector<int>::iterator it;
    for(it = number.begin(); it != number.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;
}

void test2()
{
    vector<int> number = {1, 3, 5, 7, 9, 2, 4, 6, 8};
    vector<int>::reverse_iterator rit;
    for(rit = number.rbegin(); rit != number.rend(); ++rit)
    {
        cout << *rit << "  ";
    }
    cout << endl;
}
int main(int argc, char **argv)
{
    test2();
    return 0;
}

