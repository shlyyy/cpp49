#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void test()
{
    vector<int> number;
    number.push_back(1);

    bool flag = true;
    for(auto it = number.begin(); it != number.end(); ++it)
    {
        cout << *it << "  ";
        if(flag)
        {
            //底层已经发生了扩容，导致迭代器it失效了
            number.push_back(2);
            it = number.begin();//将迭代器重新置位即可
            flag = false;
        }
    }
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

