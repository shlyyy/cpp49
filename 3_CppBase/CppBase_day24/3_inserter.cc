#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <set>
#include <algorithm>

using std::cout;
using std::endl;
using std::back_inserter;
using std::back_insert_iterator;
using std::front_inserter;
using std::front_insert_iterator;
using std::inserter;
using std::insert_iterator;
using std::ostream_iterator;
using std::vector;
using std::list;
using std::set;
using std::copy;

void test()
{
    vector<int> vecNumber = {1, 3, 7, 9};
    list<int> listNumber = {2, 4, 6, 8};

    //将list中的元素插入到vector尾部
    /* copy(listNumber.begin(), listNumber.end(), */ 
    /*      back_inserter(vecNumber)); */
    copy(listNumber.begin(), listNumber.end(), 
         back_insert_iterator<vector<int>>(vecNumber));
    copy(vecNumber.begin(), vecNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    //将vector中的元素插入到list头部
    //std::front_inserter以及front_insert_iterator底层会
    //调用push_front
    /* copy(vecNumber.begin(), vecNumber.end(), */ 
    /*      front_inserter(listNumber)); */
    copy(vecNumber.begin(), vecNumber.end(), 
         front_insert_iterator<list<int>>(listNumber));
    copy(listNumber.begin(), listNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << endl << endl;
    //inserter与insert_iterator底层使用的是insert函数
    set<int> setNumber = {11, 4, 6, 9, 22, 55, 6};
    auto it = setNumber.begin();
    /* copy(vecNumber.begin(), vecNumber.end(), */ 
    /*      inserter(setNumber, it)); */
    copy(vecNumber.begin(), vecNumber.end(), 
         insert_iterator<set<int>>(setNumber, it));
    copy(setNumber.begin(), setNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

