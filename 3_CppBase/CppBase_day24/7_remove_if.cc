#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::vector;
using std::ostream_iterator;
using std::copy;

bool func(int value)
{
    return value > 6;
}

void test()
{
    vector<int> number = {2, 4, 6, 9, 8, 5, 3};
    copy(number.begin(), number.end(),
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    /* auto it = remove_if(number.begin(), number.end(), */ 
    /*                     bind1st(std::greater<int>(), 6));//error */

    auto it = remove_if(number.begin(), number.end(), 
                        bind2nd(std::greater<int>(), 6));

    /* auto it = remove_if(number.begin(), number.end(), */ 
    /*                     bind1st(std::less<int>(), 6)); */

    /* auto it = remove_if(number.begin(), number.end(), [](int value){ */
    /*                     return value > 6; */
    /*                     }); */

    //将所有大于6的元素删除
    //remove_if可以配合erase进行使用，达到删除满足条件的元素
    /* auto it = remove_if(number.begin(), number.end(), func); */
    number.erase(it, number.end());
    copy(number.begin(), number.end(),
         ostream_iterator<int>(cout, "  "));
    cout << endl;
}


int main(int argc, char **argv)
{
    test();
    return 0;
}

