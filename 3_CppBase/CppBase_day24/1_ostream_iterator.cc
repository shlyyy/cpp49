#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::copy;

//cout << 1 << endl;
//operator<<
void test()
{
    vector<int> vec = {1, 4, 7, 9, 3};
    ostream_iterator<int> osi(cout, "\n");
    copy(vec.begin(), vec.end(), osi);
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

