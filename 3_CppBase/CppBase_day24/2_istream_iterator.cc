#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>


using std::cout;
using std::endl;
using std::istream_iterator;
using std::ostream_iterator;
using std::vector;
using std::copy;

void test()
{
    vector<int> vec;
    /* vec.reserve(10); */
    cout << "111" << endl;
    istream_iterator<int> isi(std::cin);
    cout << "222" << endl;
    /* copy(isi, istream_iterator<int>(), vec.begin()); */
    //std::back_inserter函数的底层最终会调用到push_back
    /* copy(isi, istream_iterator<int>(), std::back_inserter(vec)); */
    copy(isi, istream_iterator<int>(), 
         std::back_insert_iterator<vector<int>>(vec));
    cout << "333" << endl;
    copy(vec.begin(), vec.end(), 
         ostream_iterator<int>(cout, "  "));
    /* cout << "444" << endl; */
    cout << endl;

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

