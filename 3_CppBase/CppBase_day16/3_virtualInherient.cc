#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
    A(): _la(0){ cout << "A()" << endl;  }

    A(long a)
    : _la(a)
    {   cout << "A(long)" << endl;  }

    void printA() const {   cout << "A::_la:" << _la << endl; }

private:
    long _la = 0;
};

class B 
: virtual 
public A
{
public:
    B(long a, long b)
    : A(a)
    , _lb(b)
    {   cout << "B(long,long)" << endl; }

    void printB() const
    {   cout << "B::_lb:" << _lb << endl;}

private:
    long _lb;
};

class C 
: virtual 
public A
{
public:
    C(long a, long c)
    : A(a)
    , _lc(c)
    {   cout << "C(long,long)" << endl; } 

    void printC() const
    {   cout << "C::_lc:" << _lc << endl;}
private:
    long _lc;
};

class D
: public B 
, public C
{
public:
    //当采用了虚拟继承时，最下层的派生类D的构造函数
    //初始化表达式中要显式调用顶层虚基类A的构造函数；
    //如果不显式给出来，就会自动虚基类A的默认构造函数
    D(long a, long b, long c, long d)
    : A()
    , B(a, b)
    , C(a, c)
    , _ld(d)
    {   cout << "D(long,long,long,long)" << endl;   }

    void printD() const
    {   cout << "D::_ld:" << _ld << endl;}

private:
    long _ld;
};

void test0()
{
    D d(1, 2, 3, 4);
    d.printA();//虚基类A只有一份
    //d.B::printA();
    //d.C::printA();
    d.printB();
    d.printC();
    d.printD();
    cout << endl;

    C c(10, 11);
    c.printA();
    c.printC();


}


int main()
{
    test0();
    return 0;
}

