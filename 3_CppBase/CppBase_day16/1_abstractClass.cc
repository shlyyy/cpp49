#include <iostream>
using std::cout;
using std::endl;

//由于A无法实例化对象，但其派生类可以
//A也称为是抽象类
class A
{
protected:
    A(long a): _la(a) {cout << "A()" << endl; }

    void print() const {    cout << "A::_la:" << _la << endl;   }

    ~A() {  cout << "~A()" << endl; }

    long _la;
};

class B
: public A
{
public:
    B(long a, long b)
    : A(a)
    //, _la(a) //基类成员只能在基类构造函数中进行初始化
    , _lb(b)
    {   cout << "B(long,long)" << endl; }

    long _lb;
};

void test0()
{
    //A a(1);//A无法实例化对象
    //A * pa = new A(1);
    //pa->print();

    B b(1, 2);//B可以
}


int main()
{
    test0();
    return 0;
}

