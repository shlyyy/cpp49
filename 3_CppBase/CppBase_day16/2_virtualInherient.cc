#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
    A(long a)
    : _la(a)
    {   cout << "A(long)" << endl;  }

    void printA() const {   cout << "A::_la:" << _la << endl; }
private:
    long _la;
};

class B 
: //virtual 
public A
{
public:
    B(long a, long b)
    : A(a)
    , _lb(b)
    {   cout << "B(long,long)" << endl; }

    void printB() const
    {   cout << "B::_lb:" << _lb << endl;}

private:
    long _lb;
};

class C 
: //virtual 
public A
{
public:
    C(long a, long c)
    : A(a)
    , _lc(c)
    {   cout << "C(long,long)" << endl; } 

    void printC() const
    {   cout << "C::_lc:" << _lc << endl;}
private:
    long _lc;
};

class D
: public B 
, public C
{
public:
    D(long a, long b, long c, long d)
    : B(a, b)
    , C(a, c)
    , _ld(d)
    {   cout << "D(long,long,long,long)" << endl;   }

    void printD() const
    {   cout << "D::_ld:" << _ld << endl;}

private:
    long _ld;
};

void test0()
{
    D d(1, 2, 3, 4);

    //d.printA();//error
    d.B::printA();
    d.C::printA();
}


int main()
{
    test0();
    return 0;
}

