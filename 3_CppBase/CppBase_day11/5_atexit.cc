#include <stdlib.h>
#include <iostream>
using std::cout;
using std::endl;

void display()
{
    cout << "display()" << endl;
}

void print()
{
    cout << "print()" << endl;
}

void test0()
{//atexit的作用：在程序退出时，所注册的函数会被调用
 //注册的函数遵循栈结构的, 后注册的先调用
    atexit(display);
    atexit(display);
    atexit(print);
}


int main()
{
    cout << "enter main()" << endl;
    test0();
    cout << "exit main()" << endl;
    return 0;
}

