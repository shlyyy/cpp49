#include <string.h>
#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

class CowString
{
public:
    CowString()
    : _pstr(new char[5]() + 4)
    {   
        cout << "CowString()" << endl;  
        //初始化引用计数
        initRefcount();
    }

    CowString(const char * pstr)
    : _pstr(new char[strlen(pstr) + 5]() + 4)
    {
        cout << "CowString(const char*)" << endl;
        strcpy(_pstr, pstr);
        initRefcount();
    }

    ~CowString()
    {
        release();
    }


    //时间复杂度为O(1)
    CowString(const CowString & rhs)
    : _pstr(rhs._pstr)//浅拷贝
    {
        cout << "CowString(const CowString&)" << endl;
        increaseRefcount();
    }

    CowString & operator=(const CowString & rhs)
    {
        cout << "CowString & operator=(const CowString&)" << endl;
        if(this != &rhs) {
            release();//回收左操作数空间
            //浅拷贝, 引用计数加1
            _pstr = rhs._pstr;
            increaseRefcount();
        }
        return *this;
    }

    char & operator[](size_t idx);

    const char * c_str() const {    return _pstr;   }

    int size() const {  return strlen(_pstr);   }
    
    int getRefcount() const { return *(int*)(_pstr - 4);     }

    friend ostream & operator<<(ostream & os, const CowString & rhs);
private:
    void initRefcount() {
        *(int*)(_pstr - 4) = 1;
    }

    void increaseRefcount() {
       ++*(int*)(_pstr - 4);
    }

    void decreaseRefcount() {
       --*(int*)(_pstr - 4);
    }

    void release()
    {
        decreaseRefcount();
        if(getRefcount() == 0) {
            delete [] (_pstr - 4);
            cout << ">> free heap space" << endl;
        }
    }

private:
    char * _pstr;
};

ostream & operator<<(ostream & os, const CowString & rhs)
{
    os << rhs._pstr;
    return os;
}
    
char & CowString::operator[](size_t idx)
{
    if(idx < size()) {
        //判断是否为共享字符串
        if(getRefcount() > 1){
            decreaseRefcount();
            char * ptmp = new char[size() + 5]() + 4;
            strcpy(ptmp, _pstr);
            _pstr = ptmp;
            initRefcount();
        }
        return _pstr[idx];
    }else {
        static char nullchar = '\0';
        return nullchar;
    }
}

void test0()
{
    CowString s0;
    cout << "s0:" << s0 << endl;
    CowString s1("hello");
    cout << "s1:" << s1 << endl;

    CowString s2 = s1;
    cout << "s2:" << s2 << endl;
    printf("s1's content address: %p\n", s1.c_str());
    printf("s2's content address: %p\n", s2.c_str());
    cout << "s1' refcount:" << s1.getRefcount() << endl;
    cout << "s2' refcount:" << s2.getRefcount() << endl;


    CowString s3("world");
    cout << "s3:" << s3 << endl;
    cout << "s3' refcount:" << s3.getRefcount() << endl;

    cout << "\n赋值运算符 s3 = s1:" << endl;
    s3 = s1;
    cout << "s3:" << s3 << endl;
    printf("s1's content address: %p\n", s1.c_str());
    printf("s2's content address: %p\n", s2.c_str());
    printf("s3's content address: %p\n", s3.c_str());
    cout << "s1' refcount:" << s1.getRefcount() << endl;
    cout << "s2' refcount:" << s2.getRefcount() << endl;
    cout << "s3' refcount:" << s3.getRefcount() << endl;


    cout << "\n执行写操作s3[0] = 'H' :" << endl;
    s3[0] = 'H';
    printf("&s1: %p\n", &s1);
    printf("&s2: %p\n", &s2);
    printf("&s3: %p\n", &s3);
    printf("s1's content address: %p\n", s1.c_str());
    printf("s2's content address: %p\n", s2.c_str());
    printf("s3's content address: %p\n", s3.c_str());
    cout << "s1' refcount:" << s1.getRefcount() << endl;
    cout << "s2' refcount:" << s2.getRefcount() << endl;
    cout << "s3' refcount:" << s3.getRefcount() << endl;
    cout << "s3:" << s3 << endl;

    cout << "\n执行读操作s1[0]:" << endl;
    cout << "s1[0]:" << s1[0] << endl;
    printf("s1's content address: %p\n", s1.c_str());
    printf("s2's content address: %p\n", s2.c_str());
    printf("s3's content address: %p\n", s3.c_str());
    cout << "s1' refcount:" << s1.getRefcount() << endl;
    cout << "s2' refcount:" << s2.getRefcount() << endl;
    cout << "s3' refcount:" << s3.getRefcount() << endl;
}


int main()
{
    test0();
    return 0;
}

