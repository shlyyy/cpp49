#include <iostream>

using std::cout;
using std::endl;


int main(int argc, char **argv)
{
#ifndef __HELLO__ //ifndef = if not define
    cout << "hello" << endl;
#else
    cout << "else" << endl;
#endif
    return 0;
}

