#include <iostream>
using std::cout;
using std::endl;

class Point{
public:
    //手动写出构造函数之后，系统不再给我们提供默认的无参构造
    //如果仍然想使用无参构造，就自己写出来
    /* Point(){ */
    /*     cout << "Point()" << endl; */
    /* } */

    //初始化列表写在构造函数的头和体之间
    //第一行用：
    //后面几行用逗号，逗号放在最前面;
    //每个数据成员的初始化都占一行
    /* Point(int ix,int iy) */
    /* { */
    /*     cout << "Point(int,int)" << endl; */
    /*     _ix = ix; */
    /*     _iy = iy; */
    /* } */

    void print(){
        cout << "ix:" << _ix
            << " , iy:" << _iy << endl;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _iy;
    int _ix;
};


void test0(){
    Point  *point = new Point();
    point->print();

    /* Point pt(3,4); */
    /* pt.print(); */
}

int main(void){
    test0();
    return 0;
}
