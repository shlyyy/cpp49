#include <iostream>
using std::cout;
using std::endl;

#define MAX(X,Y) X>Y?X:Y
#define MULTIPLY(X,Y) ((X)*(Y))
const int multiply(int a,int b){
    return a * b;
}

const int number = 2;
//number = 3;//error


void test0(){
    int a = 1,b =2,c = 3;
    cout << "MULTIPLY(a + b,b + c):" << multiply(a + b,b + c) << endl; 
}

//1+2*2+3

int main(void){
    test0();
    return 0;
}
