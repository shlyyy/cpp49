#include <iostream>
using std::cout;
using std::endl;

//常用工具，监测内存使用情况 valgrind

void test0(){
    int * pa = new int(1);
    cout << "*pa:" << *pa << endl;
    delete pa;

    int * pb = new int();
    cout << "*pb:" << *pb << endl;
    delete pb;
}

int main(void){
    test0();
    return 0;
}
