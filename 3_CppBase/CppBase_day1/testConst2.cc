#include <iostream>
using std::cout;
using std::endl;

void test0(){
    int number = 1,number2 = 2;
    int * p = &number;
    *p = 10;
    p = &number2;

    //const关键字在*号左边
    const int * p1 = &number; //能够改变指针的指向，不能改变其内容
    //*p1 = 100;
    p1 = &number2;

    int const * p2 = &number; //能够改变指针的指向，不能改变其内容
    //*p2 = 100;
    p2 = &number2;


    //const关键字在*号右边
    int * const p3 = &number; //不能改变指针的指向，可以改变其内容
    *p3 = 100;
    //p3 = &number2;
}

int main(void){
    test0();
    return 0;
}
