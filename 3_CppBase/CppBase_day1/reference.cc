#include <iostream>
using std::cout;
using std::endl;

void test0(){
    //int & ref2; //引用不能单独存在

    int number = 1;
    int & ref = number;
    cout << "&number:" << &number << endl;
    cout << "&ref:" << &ref << endl;
    cout << "ref:" << ref << endl;

    int number2 = 2;
    ref = number2;//只是修改了值，并不是改变ref的指向，并且也修改了number的值
    cout << "ref:" << ref << endl;
    cout << "number:" << number << endl;
    cout << "&number:" << &number << endl;
    cout << "&ref:" << &ref << endl;

}

int main(void){
    test0();
    return 0;
}
