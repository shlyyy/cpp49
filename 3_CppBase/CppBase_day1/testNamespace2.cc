#include <iostream>
using std::cout;
using std::endl;
namespace wd
{
int number = 1;
void display(){
    cout << "wd::hello" << endl;
    cout << "wd::number:" << number << endl;
}
}//end of namespace wd

namespace wd
{
int number2 = 2;
}

namespace wd
{
int number3 = 3;
}

int number2 = 100;//全局位置不要有和匿名空间一样的实体名

namespace //匿名空间的定义
{
int number = 10;
void display(){
    cout << "::hello" << endl;
    cout << "::number:" << number << endl;
}
}//end of namespace wd

static void print(){
    cout << "CPP49" << endl;
}
