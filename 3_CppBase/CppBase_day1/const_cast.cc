#include <iostream>
using std::cout;
using std::endl;

void func(int * px){
    *px =100;
    cout << "*px:" << *px << endl;
    cout << "px:" << px << endl;
}

void test0(){
    const int number = 10;
    func(const_cast<int*>(&number));
    cout << number << endl;
    cout << &number << endl;
}

int main(void){
    test0();
    return 0;
}
