#include <iostream>
using std::cout;
using std::endl;
namespace wd
{
int number = 1;
void display(){
    cout << "wd::hello" << endl;
    cout << "wd::number:" << number << endl;
}

namespace CPPteam
{
int number = 2;
void display(){
    cout << "wd::CPPteam::hello" << endl;
    cout << "wd::CPPteam::number:" << number << endl;
}
}//end of namespace CPPteam

}//end of namespace wd


using wd::number;
using wd::display;


void test0(){
    //C的相关库函数位于匿名空间
    //C++为了兼容C语言做的工作
    ::printf("hello,wuhan\n");
    wd::display();
    wd::CPPteam::display();
}

int main(void){
    test0();
    return 0;
}
