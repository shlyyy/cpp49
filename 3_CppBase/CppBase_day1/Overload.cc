#include <iostream>
using std::cout;
using std::endl;

int add(long a,int b){
    return a + b;
}

int add(int a,long b){
    return a + b;
}


/* int add(int a,int b){ */
/*     return a + b; */
/* } */

//给形参赋默认值从右到左
int add(int a ,int b,int c = 3){
    return a + b + c;
}

void test0(){
    int x = 10,y = 12,z = 14;
    //C++进行了名字改编：当函数名称相同时，会根据参数的类型、顺序、个数进行改编
    //cout << add() << endl;
    //cout << add(x) << endl;
    cout << add(x,y) << endl;
    cout << add(x,y,z) << endl;



    long uu = 18;
    int yy = 19;
    add(uu,yy);
    add(yy,uu);
}

int main(void){
    test0();
    return 0;
}
