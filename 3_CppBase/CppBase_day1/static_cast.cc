#include <iostream>
using std::cout;
using std::endl;

void test0(){
    //C语言的强制转换是一个万能的写法
    int * p1 = (int*)malloc(sizeof(int));
    *p1 = 1;
    free(p1);

    //C++对强制转换做了一些区分
    int * p2 = static_cast<int*>(malloc(sizeof(int)));
    *p2 = 10;
    free(p2);


    //能对强制转换进行区分
    //方便查找
    const char * pstr = "hello";
    int * pint = (int*)pstr;
    //int * pint2 = static_cast<int*>(pstr);


}

int main(void){
    test0();
    return 0;
}
