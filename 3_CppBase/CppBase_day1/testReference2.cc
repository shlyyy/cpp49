#include <iostream>
using std::cout;
using std::endl;
int g_number = 100;

int & func1(){
    cout << "g_number:" << g_number << endl;
    return g_number;// int & ref = g_number;
}


int func2(){
    cout << "g_number:" << g_number << endl;
    return g_number;// int & ref = g_number;
}

int & func3(){
    int local_number = 10;
    return local_number;
}

int & func4(){
    int * heap_number = new int(1);
    cout << *heap_number << endl;
    return *heap_number;
}

void test0(){
    cout << &g_number << endl;
    cout << &func1() << endl; 

    int & ref = func4();
    delete & ref;

    int a = 1, b =2;
    int c = a + func4() + b;
    int d = a + func4() + b;
    int e = a + func4() + b;
    int r = a + func4() + b;
    int y = a + func4() + b;
    
    /* int & ref = func3(); */
    /* cout << ref << endl; */
    //&func2();
}

int main(void){
    test0();
    return 0;
}
