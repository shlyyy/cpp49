#include <iostream>
using std::cout;
using std::endl;
/* void swap(int x,int y){ //值传递 */
/*     int tmp = x; */
/*     x = y; */
/*     y = tmp; */
/* } */

void swap(int * px,int * py){//指针传递（地址传递）
    int tmp = *px;
    *px = *py;
    *py = tmp;
}

void swap(int & x, int & y){//引用传递
    int tmp = x;
    x = y;
    y = tmp;
}


void test0(){
    int a = 1,b = 2;
    cout << "a:" << a << " ,b:" << b << endl;
    swap(a,b);
    cout << "a:" << a << " ,b:" << b << endl;

    const int & ref = a;
    //ref = 100;

}

int main(void){
    test0();
    return 0;
}
