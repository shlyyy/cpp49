#include <iostream>
using std::cout;
using std::endl;

#define MULTIPLY(X,Y) ((X)*(Y))


//inline必须加在函数的实现前
inline
int multiply(int x,int y){
    return x * y;
}

void test0();

void test0(){
    int a = 2,b =3;
    multiply(a,b);
}

int main(void){
    test0();
    return 0;
}
