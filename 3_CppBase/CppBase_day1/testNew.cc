#include <iostream>
using std::cout;
using std::endl;



void test0(){




    //开辟数组空间时，一定不要忘了最后的小括号，小括号里也不要传参
    int * p = new int[10]();//开辟数组时，要记得采用[]
    for(int idx = 0; idx != 10; ++idx)
    {
        p[idx] = idx;
        cout << p[idx] << endl;
    }
    delete []p;//回收时，也要采用[]
}

int main(void){
    test0();
    return 0;
}
