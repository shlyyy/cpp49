#include <limits>
#include <iostream>
using std::cout;
using std::ostream;
using std::istream;
using std::endl;



class Line
{
//public:
    //嵌套类Point专为Line类服务
    class Point
    {
    public:
        Point() = default;//默认构造函数

        Point(int ix, int iy = 0)
        : _ix(ix)
        , _iy(iy)
        {   cout << "Point(int,int)" << endl;   }


        void print() const
        {
            cout << "(" << _ix
                 << "," << _iy
                 << ")";
        }
    private:
        int _ix = 0;
        int _iy = 0;
    };
public:
    Line(int x1, int y1, int x2, int y2)
    : _pt1(x1, y1)
    , _pt2(x2, y2)
    {   cout << "Line(int,int,int,int)" << endl;    }

    void printLine() const
    {
        _pt1.print();
        cout << " ---> ";
        _pt2.print();
        cout << endl;
    }

private:
    Point _pt1, _pt2;
};


void test0()
{
    //嵌套类被private关键字修饰以后，就不能直接访问了
    //Line::Point pt(11, 12);
    //cout << "pt:";
    //pt.print();
    //cout << endl;

    Line line(1, 2, 3, 4);
    line.printLine();
}


int main()
{
    test0();
    return 0;
}

