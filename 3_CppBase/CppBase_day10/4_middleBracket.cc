#include <string.h>
#include <vector>
#include <iostream>
using std::cout;
using std::endl;

class CharArray
{
public:
    CharArray(int size)
    : _size(size)
    , _data(new char[_size]())
    {}

    char & operator[](int idx)
    {
        cout << "char & operator[](int)" << endl;
        if(idx >= 0 && idx < _size)
        {
            return _data[idx];
        } else {
            static char nullchar = '\0';
            return nullchar;
        }
    }

    ~CharArray()
    {
        if(_data) {
            delete [] _data;
            _data = nullptr;
        }
    }

    int size() const {  return _size;   }

private:
    int _size;
    char * _data;
};

void test1()
{
    const char * pstr = "hello,world";
    CharArray ca(strlen(pstr) + 1);
    for(int i = 0; i < ca.size(); ++i) {
        ca[i] = pstr[i];
    }

    for(int i = 0; i < ca.size(); ++i){
        cout << ca[i] << endl;
    }

    std::string s("hello");
    std::vector<int> numbers{1, 2, 3, 4, 5};
    for(size_t i = 0; i < numbers.size(); ++i){
        cout << numbers[i] << " ";
    }
    cout << endl;
}

void test0()
{
    int arr[5] = {1, 2, 3, 4, 5};
    cout << arr[0] << endl;
    //arr.operator[](0)
}


int main()
{
    /* test0(); */
    test1();
    return 0;
}

