#include <limits>
#include <iostream>
using std::cout;
using std::ostream;
using std::istream;
using std::endl;

class Point
{
public:
    Point() = default;//默认构造函数
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    void print() const
    {
        cout << "(" << _ix
             << "," << _iy
             << ")";
    }

    //如果以成员函数形式重载,左操作数成了this指针代表的当前对象
    //因此输出流运算符一般情况下，不能以成员函数形式重载
    /* ostream & operator<<(ostream & os) */
    /* { */
    /*     os << "(" << _ix */
    /*          << "," << _iy */
    /*          << ")"; */
    /*     return os; */
    /* } */

    friend ostream & operator<<(ostream & os, const Point & rhs);
    friend istream & operator>>(istream & os, Point & rhs);

private:
    int _ix = 0;
    int _iy = 0;
};

ostream & operator<<(ostream & os, const Point & rhs)
{
     os << "(" << rhs._ix
          << "," << rhs._iy
          << ")";
     return os;
}

void readInteger(istream & is, int & x)
{
    cout << "pls input a valid integer:" << endl;
    while(is >> x, !is.eof()) {
        if(is.bad()) {
            cout << "istream has broken" << endl;
            return;
        } else if(is.fail()) {
            is.clear();
            is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cout << "pls input a valid integer:" << endl;
        } else {
            return;
        }
    }
}

istream & operator>>(istream &is, Point & rhs)
{   //对于输入流来说，需要考虑每一次使用之后，流是否还有效
    readInteger(is, rhs._ix);
    readInteger(is, rhs._iy);
    return is;
}

void test0()
{
    Point pt(1, 2);
    //左操作数是流对象，右操作数是Point对象
    cout << "pt:" << pt << endl;
    //与常规思维是相悖的
    //pt << cout << endl;
    
    std::cin >> pt;
    cout << "pt:" << pt << endl;
}


int main()
{
    test0();
    return 0;
}

