#ifndef __WD_Line_HPP__ 
#define __WD_Line_HPP__ 
class Line
{
public:
    Line(int, int, int,int);
    ~Line();

    void printLine() const;

private:
    class LineImpl;//嵌套类的前向声明
private:
    LineImpl * _pimpl;
};

#endif

