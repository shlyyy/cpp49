#include "Line.hpp"
#include <limits>
#include <iostream>
using std::cout;
using std::ostream;
using std::istream;
using std::endl;


//设计模式：PIMPL
//将其打包成头文件和动态库的形式，交给第三方去使用
//好处：
//1. 实现了信息隐藏
//2. 可以实现库的平滑升级 (实现文件有变动，只需要替换动态库即可)
//3. 编译防火墙(只要头文件不变，第三方产品不需要重新进行编译的)


//LineImpl本身就是一个嵌套类
class Line::LineImpl
{
//public:
    //嵌套类Point专为Line类服务
    class Point
    {
    public:
        Point() = default;//默认构造函数

        Point(int ix, int iy = 0)
        : _ix(ix)
        , _iy(iy)
        {   cout << "Point(int,int)" << endl;   }


        void print() const
        {
            cout << "(" << _ix
                 << "," << _iy
                 << ")";
        }
    private:
        int _ix = 0;
        int _iy = 0;
    };
public:
    LineImpl(int x1, int y1, int x2, int y2)
    : _pt1(x1, y1)
    , _pt2(x2, y2)
    {   cout << "LineImpl(int,int,int,int)" << endl;    }

    ~LineImpl() {   cout << "~LineImpl()" << endl;  }

    void printLine() const
    {
        _pt1.print();
        cout << " ---> ";
        _pt2.print();
        cout << endl;
    }

private:
    Point _pt1, _pt2;
};

Line::Line(int x1, int y1, int x2, int y2)
: _pimpl(new LineImpl(x1, y1, x2, y2))
{
    cout << "Line(int,int,int,int)" << endl;
}

Line::~Line()
{
    if(_pimpl) {
        delete _pimpl;
        _pimpl = nullptr;
    }
    cout << "~Line()" << endl;
}

void Line::printLine() const
{
    _pimpl->printLine();
}
