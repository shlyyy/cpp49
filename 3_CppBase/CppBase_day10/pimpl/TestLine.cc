#include "Line.hpp"
#include <iostream>
using std::cout;
using std::endl;

void test0()
{
    Line line(1, 2, 3, 4);
    line.printLine();
}


int main()
{
    test0();
    return 0;
}

