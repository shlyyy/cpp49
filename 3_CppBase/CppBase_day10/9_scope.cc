#include <iostream>
using std::cout;
using std::endl;

namespace wd
{

int number = 1;

class Example
{
public:
    //就近原则
    void display(int number)
    {
        //直接使用number会屏蔽其他作用域中的相同变量
        cout << "局部变量number:" << number << endl;
        cout << "this->number:" << this->number << endl;
        cout << "Example::number:" << Example::number << endl;
        cout << "wd::number:" << wd::number << endl;
    }

private:
    int number = 100;
};


}//end of namespace wd

void test0()
{
    wd::Example e;
    e.display(10);
}


int main()
{
    test0();
    return 0;
}

