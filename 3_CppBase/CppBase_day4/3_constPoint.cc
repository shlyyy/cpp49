#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {
        //_ix = ix;//赋值语句
        //_iy = iy;
    }

    void print()
    {
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }


private:
    const int _ix;
    const int _iy;
};

void test0()
{

}


int main()
{
    test0();
    return 0;
}

