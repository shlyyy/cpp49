#include <iostream>
using std::cout;
using std::endl;


//需求：一个类只能创建一个对象,且是唯一的对象
//
//应用场景:
//1. 全局唯一的数据，都可以考虑使用单例模式来设计
//2. 配置文件
//3. 网页库、词典都可以设计为单例对象
class Singleton
{
public:
    //作用：用来创建对象
    //2. 定义了一个静态的成员函数
    static Singleton * getInstance()
    {   //只有第一次才去执行new表达式创建对象
        //后续再调用该静态成员函数时，直接返回
        //第一次创建的对象即可
        if(nullptr == _pInstance) {
            _pInstance  = new Singleton();
        }
        return _pInstance;
    }

    static void destroy()
    {
        if(_pInstance) {
            delete _pInstance;
            _pInstance = nullptr;
        }
    }

    void setXY(int x, int y)
    {
        _ix = x;
        _iy = y;
    }


    ~Singleton() {  cout << "~Singleton()" << endl; }
private:
    //1. 将构造函数私有化
    Singleton(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Singleton(int,int)" << endl;
    }

    //禁止复制
    Singleton(const Singleton &);
    Singleton & operator=(const Singleton&);

public:
    void print() const
    {
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
    //3. 定义一个静态的能够存储本类型对象的指针
    static Singleton * _pInstance;
};

//Singleton * Singleton::_pInstance = nullptr;
//因为有了Singleton的作用域，可以认为是在类内部
Singleton * Singleton::_pInstance = getInstance();


void test0()
{
    //Singleton pt1(1, 2);//不能让该语句编译通过
    //Singleton pt2(11, 12);//error

    Singleton * p1 = Singleton::getInstance();
    Singleton * p2 = Singleton::getInstance();
    p1->setXY(101, 11);
    p1->print();
    p2->print();
    cout << "p1:" << p1 << endl;
    cout << "p2:" << p2 << endl;

    //Singleton s = *p1;//error 调用的是拷贝构造函数
    
    //delete p1;//error 形式不够优雅, 不希望该语句编译通过
    Singleton::destroy();
}


int main()
{
    test0();
    return 0;
}

