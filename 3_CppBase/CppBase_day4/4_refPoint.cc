#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    , _ref(_ix)
    {
        //_ref = _ix;
    }

    void print()
    {
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
    int & _ref;//引用还是占据一个指针的大小,引用的底层实现还是指针
};

void test0()
{
    cout << "sizeof(Point):" << sizeof(Point) << endl;
    int a = 1;
    int & ref1 = a;//在语法层面无法获取引用的真实地址
    cout << "&ref1: " << &ref1 << endl;
    cout << "&a:" << &a << endl;
}


int main()
{
    test0();
    return 0;
}

