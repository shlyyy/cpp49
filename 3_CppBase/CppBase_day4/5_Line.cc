#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int =0 ,int =0)" << endl;
    }


    void print()
    {
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }


private:
    int _ix;
    int _iy;
};

class Line
{
public:
    //当类中有对象成员时，默认情况下，会自动调用相应类型的
    //默认构造函数，完成自己的初始化
    //
    //当不希望调用类对象成员的默认构造函数时，需要
    //手动在初始化表达式中显式调用相应类型的构造函数
    Line(int x1, int y1, int x2, int y2)
    //: _p1()
    //, _p2()
    : _p1(x1, y1)
    , _p2(x2, y2)
    {
        cout << "Line(int,int,int,int)" << endl;
    }

    void printLine()
    {
        _p1.print();
        cout << " ---> ";
        _p2.print();
    }

private:
    Point _p1;
    Point _p2;
};

void test0()
{
    Line line(1, 2, 3, 4);
    line.printLine();


}


int main()
{
    test0();
    return 0;
}

