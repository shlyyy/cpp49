#include <iostream>
using std::cout;
using std::endl;


class Point
{
public:
    Point(int x = 0, int y = 0)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int x,int y)" << endl;
    }

    //具有复制控制语义的函数
    //1. 拷贝构造函数
    //2. 赋值运算符函数
    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
        cout << this->_ix << endl;
    }
    
    //当类中没有显式提供赋值运算符函数时，系统会自动提供一个
    //Point operator=(const Point & rhs)
    //void operator=(const Point & rhs)
    Point & operator=(const Point & rhs)
    {
        this->_ix = rhs._ix;
        this->_iy = rhs._iy;
        cout << "Point & operator=(const Point&)" << endl;
        return *this;
    }

    void print(/*  Point * const this    */);

    ~Point()
    {   cout << "~Point()" << endl; }

private:
    int _ix;
    int _iy;
};


void Point::print(/*  Point * const this    */)
{
    //this = 0x1000;//error
    cout << "(" << this->_ix
         << "," << this->_iy
         << ")" << endl;
}

void test0()
{
    int a = 1, b = 2, c = 3;
    cout << (a = b) << endl;//赋值操作
    &(a = b);
    cout << "&a:" << &a << endl;
    cout << "&(a = b): " << &(a = b) << endl;
    a = b = c;


    Point p1(1, 2);
    Point p2(3, 4);
    cout << "&p1:" << &p1 << endl;
    cout << "&p2:" << &p2 << endl;
    cout << "p1:";
    p1.print();//=> Point::print(&p1);
    cout << "p2:";//经过编译器处理之后，会将对象的地址传递给this指针
    p2.print();//=> Point::print(&p2);


    p2 = p1;//赋值操作
    //p2.operator=(p1);//等价于上面这一句
    cout << "\n执行赋值操作: \n";
    cout << "p1:";
    p1.print();
    cout << "p2:";
    p2.print();

    Point * p = new Point(11, 12);
    p->print();// => Point::print(p);

    delete p;
}



int main()
{
    test0();
    return 0;
}

