#include <iostream>
using std::cout;
using std::endl;


//需求：一个类只能创建一个对象,且是唯一的对象

class Point
{
public:
    //作用：用来创建对象
    static Point & getInstance()
    {//在其中调用构造函数创建对象
        static Point pt;
        return pt;
    }

private:
    //1. 将构造函数私有化
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int,int)" << endl;
    }
public:
    void print() const
    {
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
};

//Point pt3(1, 1);//error

void test0()
{
    //Point pt1(1, 2);//不能让该语句编译通过
    //Point pt2(11, 12);//error

    Point & ref1 = Point::getInstance();
    Point & ref2 = Point::getInstance();
    ref1.print();
    ref2.print();
    cout << "&ref1:" << &ref1 << endl;
    cout << "&ref2:" << &ref2 << endl;
    

}


int main()
{
    test0();
    return 0;
}

