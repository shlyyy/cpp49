#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point() 
    : _ix(0)
    , _iy(0)
    {   cout << "Point()" << endl;  }

    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {   cout << "Point(const Point &)" << endl; }

    void print() const
    {
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }

    void show() const
    {   cout << "show()" << endl;   }

    void show2(int i)
    {   cout << "show2() i:" << i << endl;  }

    ~Point() {  cout << "~Point()" << endl; }


private:
    int _ix;
    int _iy;
};

void test0()
{
    int arr[5] = {1, 2, 3, 4, 5};
    cout << "arr[0]:" << arr[0] << endl;

    Point points[5] = {
        Point(1, 2),
        Point(3, 4),
    };
}

void test1()
{
    Point * pt = nullptr;
    pt->show();//ok
    pt->show2(1);//ok
    pt->print();// => Point::print(nullptr);
}


int main()
{
    test0();
    /* test1(); */
    return 0;
}

