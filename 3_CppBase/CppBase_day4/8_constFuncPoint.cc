#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {
        //_ix = ix;//赋值语句
        //_iy = iy;
    }

#if 0
    void print() 
    {
        cout << "void print()" << endl;
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }
#endif
    void print() const 
    {
        cout << "void print() const" << endl;
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
};


void func(const Point & p)
{
    p.print();
}

void test0()
{
    Point pt(1, 2);
    cout << "pt:";
    pt.print();//pt是一个非const对象，可以调用const版本的成员函数

    const Point pt2(11, 12);
    cout << "\npt2:";
    pt2.print();//const对象只能调用const版本的成员函数

    //在传递参数时用的最多
    cout << endl;
    func(pt);
}


int main()
{
    test0();
    return 0;
}

