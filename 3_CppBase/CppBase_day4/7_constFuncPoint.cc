#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {
        //_ix = ix;//赋值语句
        //_iy = iy;
    }

    //当函数名称相同时，只是函数参数类型、顺序、个数不同时
    //会构成函数重载

    void print(/*   Point * const this  */) 
    {
        cout << "void print()" << endl;
        //_ix = 100;//ok
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }

    //const关键字修饰成员函数以后，修改了this的形式
    //this构成了双重限定
    void print(/*   Point const * const this   */) const 
    {
        cout << "void print() const" << endl;
        //this->_ix = 100;//error
        cout << "(" << _ix
             << "," << _iy 
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
};


void func(const Point & p)
{
    p.print();
}

void test0()
{
    Point pt(1, 2);
    cout << "pt:";
    pt.print();//pt是一个非const对象，只能调用非const版本

    const Point pt2(11, 12);
    cout << "\npt2:";
    pt2.print();//const对象只能调用const版本的成员函数

    //在传递参数时用的最多
    cout << endl;
    func(pt);
}


int main()
{
    test0();
    return 0;
}

