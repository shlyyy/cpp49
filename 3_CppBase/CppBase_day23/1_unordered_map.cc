#include <math.h>
#include <iostream>
#include <unordered_map>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::unordered_map;
using std::string;
using std::pair;
using std::make_pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first 
             << "  " 
             << elem.second << endl;
    }
}

void test()
{
    //unordered_map的特征
    //1、存放是key-value类型，key值是唯一的，不能重复
    //但是value值既可以重复也可以不重复
    //2、key值是没有顺序的
    //3、底层使用的还是哈希
    unordered_map<int, string> number = {
        pair<int, string>(1, "beijing"),
        pair<int, string>(2, "beijing"),
        {5, "nanjing"},
        {7, "wuhan"},
        make_pair(3, "wangdao"),
        make_pair(3, "wangdao"),
    };
    display(number);

    cout << endl << "下标操作" << endl;
    cout << "number[3] = " << number[3] << endl;//查找
    cout << "number[6] = " << number[6] << endl;//插入
    display(number);

    cout << endl <<endl;
    number[6] = "point";//修改
    display(number);
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
    }

    double getDistance() const
    {
        return hypot(_ix, _iy);
    }

    int getX() const
    {
        return _ix;
    }

    int getY() const
    {
        return _iy;
    }

    ~Point()
    {

    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
        << ", " << rhs._iy
        << ")";

    return os;
}

void test2()
{
    unordered_map<string, Point> number = {
        pair<string, Point>("1", Point(1, 2)),
        pair<string, Point>("hello", Point(1, 2)),
        {"3", Point(4, 5)},
        {"9", Point(4, 5)},
        make_pair("wangdao", Point(-1, -2)),
        make_pair("wangdao", Point(-1, -2)),
    };
    display(number);
}

int main(int argc, char **argv)
{
    test2();
    return 0;
}

