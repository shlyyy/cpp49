#include <iostream>
#include <queue>
#include <vector>

using std::cout;
using std::endl;
using std::priority_queue;
using std::vector;

void test()
{
    //优先级队列的底层使用的是堆排序（默认是大根堆）
    //当有元素插入进来的时候，会与堆顶进行比较，如果堆顶比
    //新插入的元素小的话，那么就满足std::less,就会将新插入
    //的元素作为新的堆顶；如果新插入的元素与堆顶进行比较，
    //堆顶比新插入的元素大，也就不满足std::less，那么老的
    //堆顶依旧还是在堆顶的位置，不会发生置换
    vector<int> vec = {1, 4, 7, 9, 5, 6, 8};
    /* priority_queue<int> pque(vec.begin(), vec.end()); */
    priority_queue<int> pque;

    for(size_t idx = 0; idx != vec.size(); ++idx)
    {
        pque.push(vec[idx]);
        cout << "优先级最高的元素是 : " << pque.top() << endl;
    }

    while(!pque.empty())
    {
        cout << pque.top() << "  ";
        pque.pop();
    }
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

