#include <iostream>

#include <log4cpp/Category.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/RollingFileAppender.hh>
#include <log4cpp/Priority.hh>
#include <log4cpp/PatternLayout.hh>

using std::cout;
using std::endl;

using namespace log4cpp;

void test0()
{
    PatternLayout * ptnLayout = new PatternLayout();
    ptnLayout->setConversionPattern("%d %c [%p] %m%n");

    PatternLayout * ptnLayout2 = new PatternLayout();
    ptnLayout2->setConversionPattern("%d %c [%p] %m%n");

    PatternLayout * ptnLayout3 = new PatternLayout();
    ptnLayout3->setConversionPattern("%d %c [%p] %m%n");

    OstreamAppender * pOsApp = new OstreamAppender("console", &cout);
    pOsApp->setLayout(ptnLayout);

    FileAppender * pFileApp = new FileAppender("fileApp", "wd.log");
    pFileApp->setLayout(ptnLayout2);

    auto pRollingFileApp = new RollingFileAppender("rollingfileApp"
                , "rollingwd.log"
                , 5 * 1024
                , 3);
    pRollingFileApp->setLayout(ptnLayout3);

    Category & mycat = Category::getRoot().getInstance("salesDepart");
    mycat.setPriority(Priority::DEBUG);
    mycat.addAppender(pOsApp);
    mycat.addAppender(pFileApp);
    mycat.addAppender(pRollingFileApp);

    for(int i = 0; i < 100; ++i) {
        mycat.emerg("this is an emerg message");
        mycat.fatal("this is a fatal message");
        mycat.alert("this is an alert message");
        mycat.crit("this is a crit message");
        mycat.error("this is an error message");
        mycat.warn("this is a warn message");
        mycat.info("this is an info message");
        mycat.notice("this is a notice message");
        mycat.debug("this is a debug message");
    }

    Category::shutdown();
}


int main()
{
    test0();
    return 0;
}

