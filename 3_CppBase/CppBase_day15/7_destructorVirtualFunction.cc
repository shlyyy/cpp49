#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
    A()
    : _pa(new int(10))
    {   cout << "A()" << endl;  }

    void print() const
    {   cout << "*_pa:" << *_pa << endl;    }

    //当基类析构函数设置为虚函数之后，
    //派生类析构函数会自动成为虚函数
    //
    //析构函数只有一个, 是一个特殊的存在
    virtual
    ~A()
    {
        if(_pa) {
            delete _pa;
            _pa = nullptr;
        }
        cout << "~A()" << endl;
    }

private:
    int * _pa;
};

class B
: public A
{
public:
    B()
    : A()
    , _pb(new int(100))
    {   cout << "B()" << endl;  }

    void display() const
    {   cout << "*_pb" << *_pb << endl;}

    ~B() {
        if(_pb) {
            delete _pb;
            _pb = nullptr;
        }
        cout << "~B()" << endl;
    }

private:
    int * _pb;
};

void test0()
{
    A * pa = new B();
    pa->print();//ok
    //pa->display();//error

    //需求：希望通过基类A的指针回收派生类B的对象
    delete pa;
}


int main()
{
    test0();
    return 0;
}

