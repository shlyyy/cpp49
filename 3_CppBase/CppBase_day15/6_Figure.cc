#include <math.h>
#include <iostream>
using std::cout;
using std::endl;
using std::string;

//定义一个图形类，想要获取
//图形的名字和面积大小
//
//满足了面向对象的设计原则之一：
//对扩展开放，对修改关闭
class Figure
{
public:
    virtual string name() const = 0;
    virtual double area() const = 0;
};

class Rectangle
: public Figure
{
public:
    Rectangle(double length, double width)
    : _length(length)
    , _width(width)
    {   cout << "Rectangle()" << endl;  }

    string name() const override
    {   return string("rectangle"); }

    double area() const override
    {   return _length * _width;    }

private:
    double _length;
    double _width;
};

class Circle
: public Figure
{
public:
    Circle(double radius)
    : _radius(radius)
    {   cout << "Cirlce()" << endl; }

    string name() const override
    {   return string("circle"); }

    double area() const override
    {
        return PI * _radius * _radius;
    }

private:
    //constexpr表达的是编译时常量
    constexpr static double PI = 3.14159;
    //const static int max = 1024;
    double _radius;
};

class Triangle
: public Figure
{
public:
    Triangle(double a, double b, double c)
    : _a(a), _b(b), _c(c)
    {   cout << "Triangle()" << endl;   }

    string name() const override
    {   return string("triangle"); }

    double area() const override
    {
        //海伦公式
        double p = (_a + _b + _c) / 2;
        return sqrt(p * (p - _a) * (p - _b) * (p - _c));
    }

private:
    double _a;
    double _b;
    double _c;
};

void display(Figure & fig)
{
    cout << fig.name() << "'s area is: " 
         << fig.area() << endl;
}

void test0()
{
    Rectangle rectangle(10, 20);
    Circle circle(30);
    Triangle triangle(3, 4, 5);
    display(rectangle);
    display(circle);
    display(triangle);
}


int main()
{
    test0();
    return 0;
}

