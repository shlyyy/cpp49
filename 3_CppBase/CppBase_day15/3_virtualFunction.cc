#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
    virtual 
    void func(int val = 1)
    {   cout << "A->" << val << endl;   }
};

class B
: public A
{
public:
    virtual 
    void func(int val = 10)
    {   cout << "B->" << val << endl;   }
};

class C
: public B
{
public:
    virtual 
    void func(int val = 100)
    {   cout << "C->" << val << endl;   }
};


void test0()
{
    B b;
    A * pa = &b;
    //默认参数发生的时机是在编译时
    //编译时取决于指针的类型
    pa->func();//pa->func(1);

    B *pb = &b;
    pb->func();//pb->func(10);

    C c;
    pb = &c;
    pb->func();
}


int main()
{
    test0();
    return 0;
}

