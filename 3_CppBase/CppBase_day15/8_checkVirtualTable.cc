#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    virtual void func1()
    {   cout << "Base::func1()" << endl;}
    virtual void func2()
    {   cout << "Base::func2()" << endl;}
    virtual void func3()
    {   cout << "Base::func3()" << endl;}

private:
    long _ibase = 10;
};

class Derived
: public Base
{
public:
    virtual void func1()
    {   cout << "Derived::func1()" << endl;}
    virtual void func2()
    {   cout << "Derived::func2()" << endl;}
    virtual void func3()
    {   cout << "Derived::func3()" << endl;}
private:
    long _derived = 100;
};

void test0()
{
    Derived derived;

    long * plong = reinterpret_cast<long*>(&derived);
    cout << plong[0] << endl;
    cout << plong[1] << endl;
    cout << plong[2] << endl;

    long * plong2 = reinterpret_cast<long*>(plong[0]);
    cout << plong2[0] << endl;
    cout << plong2[1] << endl;
    cout << plong2[2] << endl;
    typedef void(*Function)();
    Function f = reinterpret_cast<Function>(plong2[0]);
    f();
    f = reinterpret_cast<Function>(plong2[1]);
    f();
    f = reinterpret_cast<Function>(plong2[2]);
    f();

    //long ** pd = (long**)&derived;
    //pd[0][0]
}


int main()
{
    test0();
    return 0;
}

