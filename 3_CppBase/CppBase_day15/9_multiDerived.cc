#include <vector>
#include <iostream>
using std::cout;
using std::endl;


class A {
public:
    virtual void a() {  cout << "A::a()" << endl;   }
    virtual void b() {  cout << "A::b()" << endl;   }
    virtual void c() {  cout << "A::c()" << endl;   }
};

class B {
public:
    virtual void a() {  cout << "B::a()" << endl;   }
    virtual void b() {  cout << "B::b()" << endl;   }
    void c() {  cout << "B::c()" << endl;   }
    void d() {  cout << "B::d()" << endl;   }
};

class C
: public A
, public B
{
public:
    virtual void a() {  cout << "C::a()" << endl;   }
    void c() {  cout << "C::c()" << endl;   }
    void d() {  cout << "C::d()" << endl;   }
};

class D
: public C
{
public:
    void c() {  cout << "D::c()" << endl;}
};

void test0()
{
    C c;
    c.a();
    //c.b();//error 成员名冲突的二义性
    c.c();
    c.d();
    cout << endl;

    A * pa = &c;
    pa->a();//C::a()
    pa->b();//A::b()
    pa->c();//C::c()   
    cout << endl;

    B * pb = &c;
    pb->a();//C::a()
    pb->b();//B::b()
    pb->c();//B::c()
    pb->d();//B::d()
    cout << endl;

    C * pc = &c;
    pc->a();//C::a()
    //pc->b();//error
    pc->c();//C::c()
    pc->d();//C::d()
    cout << endl;

    D d;
    pc = &d;
    pc->c();
}


int main()
{
    test0();
    return 0;
}

