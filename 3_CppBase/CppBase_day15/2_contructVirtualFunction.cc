#include <iostream>
using std::cout;
using std::endl;

class Grandpa
{
public:
    Grandpa() { cout << "Grandpa()" << endl;    }
    ~Grandpa() { cout << "~Grandpa()" << endl;    }

    virtual 
    void func1() {  cout << "Grandpa::func1()" << endl; }

    virtual 
    void func2() {  cout << "Grandpa::func2()" << endl; }
};

class Parent
: public Grandpa
{
public:
    Parent() { 
        cout << "Parent()" << endl;    
        //对象没有创建完毕,将其当成非虚函数来处理
        this->func1();//体现的是静态多态
    }

    ~Parent() { 
        cout << "~Parent()" << endl;    
        func2();
    }

#if 1
    //virtual 
    void func1() {  cout << "Parent::func1()" << endl; }

    //virtual 
    void func2() {  cout << "Parent::func2()" << endl; }
#endif
};

class Son
: public Parent
{
public:
    Son(): Parent(){  cout << "Son()" << endl;    }
    ~Son(){  cout << "~Son()" << endl;    }

    //virtual 
    void func1() {  cout << "Son::func1()" << endl; }

    //virtual 
    void func2() {  cout << "Son::func2()" << endl; }

};

void test0()
{
    //Parent p1;//当Parent构造函数执行时，并不知道还有Son的存在
    //即看不到Son的存在, 就无法执行Son的虚函数了
    
    //根据：虚函数（动态多态）被激活的条件
    Son son;//在Son对象构造的过程中，调用了Parent的构造函数
    //此时Son对象还没有构造完毕，因此不能走虚函数的机制
    
    cout << endl;


}


int main()
{
    test0();
    return 0;
}

