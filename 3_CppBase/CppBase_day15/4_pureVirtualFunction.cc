#include <iostream>
using std::cout;
using std::endl;


//定义了纯虚函数的类，称为抽象类
//
//抽象类无法实例化，但可以通过其
//派生类完成纯虚函数的实现，之后
//创建派生类的对象
//
//
class A
{//A类型纯粹是作为接口存在的
public:
    virtual void print() = 0;
    virtual void display() = 0;
};

class B
: public A
{
public:
    void print() override
    {
        cout << "B::print()" << endl;
    }
};

class C
: public B
{
public:
    //override关键字可以在虚函数的
    //函数名写错时，显式告诉我们错误
    void display() override
    {   cout << "C::display()" << endl; }
};


void test0()
{
    //A a;//error  A无法实例化
    //B b;//error B也是抽象类
    C c;
    A * pa = &c;
    pa->print();
    pa->display();
}


int main()
{
    test0();
    return 0;
}

