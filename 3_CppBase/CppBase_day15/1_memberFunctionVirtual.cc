#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    Base(long base)
    : _base(base)
    {   cout << "Base(long)" << endl;   }


    void func1()
    {   
        //动态多态机制
        this->print();    
    }

    void func2()
    {   
        //有了类名作用域，采用的是静态多态
        this->Base::print();  
    }

//private:
    virtual 
    void print() const
    {   cout << "Base::_base :" << _base << endl;   }

private:
    long _base;
};

class Derived
: public Base
{
public:
    Derived(long base, long derived)
    : Base(base)
    , _derived(derived)
    {   cout << "Derived(long,long)" << endl;   }


private:
    virtual 
    void print() const
    {   cout << "Derived::_derived:" << _derived << endl;   }
private:
    long _derived;
};

void test0()
{
    Base base(1);
    base.func1();

    Derived derived(10, 100);
    //Base * const this = &derived;
    derived.func1();//derived.func1(&derived);

    cout << endl;
    derived.func2();

    //静态多态吗？不能这样认为的,还是动态多态
    Base * pbase = &base;
    pbase->print();//调用的是Base::print()方法
    //pbase->Base::print();

    //动态多态
    Base * pbase2 = &derived;
    pbase2->print();//调用的是Derived::print()方法
}


int main()
{
    test0();
    return 0;
}

