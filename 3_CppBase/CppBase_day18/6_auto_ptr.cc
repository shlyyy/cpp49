
#include <memory>
#include <iostream>
using std::cout;
using std::endl;
using std::auto_ptr;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;    }

    ~Point()
    {   cout << "~Point()" << endl; }  

    void print() const 
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
};

void test0()
{
    Point * pt = new Point(1, 2);
    auto_ptr<Point> ap(pt);
    cout << "ap'get():" << ap.get() << endl;
    cout << "pt:" << pt << endl;
    ap->print();
    (*ap).print();

    cout << endl;

    //在语法形式上，是一个拷贝构造的写法；
    //但在底层已经完成了所有权的转移,表达的是移动语义
    //因此存在矛盾, auto_ptr的实现存在缺陷
    auto_ptr<Point> ap2 = ap;
    ap->print();
    (*ap).print();
    ap2->print();


    cout << "111" << endl;
}


int main()
{
    test0();
    return 0;
}

