
#include <vector>
#include <memory>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;
using std::shared_ptr;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;    }

    ~Point()
    {   cout << "~Point()" << endl; }  

    void print() const 
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
};

//当局部对象sp2即将被销毁时，调用的是
//shared_ptr的移动构造函数，没有调用拷贝构造函数
shared_ptr<Point> getValue()
{
    shared_ptr<Point> sp2(new Point(10, 11));
    sp2->print();
    return sp2;
}

void test0()
{
    Point * pt = new Point(1, 2);
    shared_ptr<Point> sp(pt);
    cout << "sp'get():" << sp.get() << endl;
    cout << "sp'use_count():" << sp.use_count() << endl;
    cout << "pt:" << pt << endl;
    sp->print();
    (*sp).print();

    cout << endl;

    //shared_ptr可以进行复制或者赋值
    //引用计数会加1
    shared_ptr<Point> sp2 = sp; //ok
    cout << "sp'use_count():" << sp.use_count() << endl;
    cout << "sp2'use_count():" << sp2.use_count() << endl;
    cout << "sp2'get():" << sp2.get() << endl << endl;

    //可以表达移动语义
    shared_ptr<Point> sp3 = getValue();
    cout << "*sp3.print():" ;
    (*sp3).print();
    cout << "sp3'use_count():" << sp3.use_count() << endl;


    //shared_ptr可以作为容器的元素
    vector<shared_ptr<Point>> numbers;
    //sp2是一个左值,这里要调用拷贝构造函数
    cout << "将sp2放入容器中";
    //numbers.push_back(sp2);//ok 引用计数加1
    //将sp2转移到容器中，sp2不再托管原来的对象
    numbers.push_back(std::move(sp2));//ok
    cout << "sp1'use_count():" << sp.use_count() << endl;
    cout << "numbers[0]'s use_count():" << numbers[0].use_count() << endl;
    cout << "sp2'use_count():" << sp2.use_count() << endl;
    
    cout << "111" << endl;
    //如果重新托管一个新对象，就可以正常使用了
    sp2.reset(new Point(33, 44));
    sp2->print();

    //sp2 = std::move(sp);
}

void test1()
{
    Point * pt = new Point(1, 2);
    shared_ptr<Point> sp(pt);
    cout << "sp->print():";
    sp->print();

    shared_ptr<Point> sp2 = sp;
    cout << "sp's use_count: " << sp.use_count() << endl;

    //当执行reset方法时，会先将引用计数减1，
    //如果引用计数为0， 才回收原来所托管的对象
    //如果引用计数不为0， 直接托管新的对象
    cout << ">> 执行reset操作:" << endl;
    sp.reset(new Point(11, 12));
    cout << "111" << endl;
    cout << "sp->print():";
    sp->print();

}

void test2()
{
    //没有托管任何对象的,空指针
    shared_ptr<Point> sp;
    cout << "sp's get():" << sp.get() << endl;
}

int main()
{
    /* test0(); */
    /* test1(); */
    test2();
    return 0;
}

