#include <iostream>
using std::cout;
using std::endl;


//自定义的智能指针 
template <class Type>
class RAII
{
public:
    RAII(Type * p)
    : _p(p)
    {}

    ~RAII() {
        if(_p) {
            delete _p;
            _p = nullptr;
        }
    }

    Type * operator->()
    {   return _p;  }

    Type & operator*()
    {   return *_p; }

    Type * get() const {    return _p;  }

    //从类中删除该函数, 就不能执行赋值或者复制
    RAII(const RAII &) = delete;
    RAII & operator=(const RAII&) = delete;

private:
    Type * _p;//托管的是堆空间的资源
};

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;    }

    ~Point()
    {   cout << "~Point()" << endl; }  

    void print() const 
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
};

void test0()
{
    //智能指针的用法
    RAII<Point> raii(new Point(1, 2));
    raii->print();
    (*raii).print();

}


int main()
{
    test0();
    return 0;
}

