#include <iostream>
using std::cout;
using std::endl;

void test0()
{
    int a = 1;
    int & ref = a;
    cout << "ref:" << ref << endl;

    int & ref3 = 1;

    //右值引用只能绑定到右值
    int && ref2 = 1;
    cout << "ref2:" << ref2 << endl;

    //右值引用无法绑定到左值
    //int && ref3 = a;//error
    const int && ref4 = 1;//没有存在的意义
}

int main()
{
    test0();
    return 0;
}

