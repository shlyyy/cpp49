#include <memory>
#include <iostream>
using std::cout;
using std::endl;
using std::weak_ptr;
using std::shared_ptr;

class Child;

class Parent
{
public:
    Parent() {  cout << "Parent()" << endl; }
    ~Parent() {  cout << "~Parent()" << endl; }

    weak_ptr<Child> _wpChild;
};

class Child
{
public:
    Child() {  cout << "Child()" << endl; }
    ~Child() {  cout << "~Child()" << endl; }

    shared_ptr<Parent> _spParent;
};

void test0()
{
    shared_ptr<Parent> spParent(new Parent);
    shared_ptr<Child> spChild(new Child);
    cout << "spParent' use_count:" << spParent.use_count() << endl;
    cout << "spChild' use_count:" << spChild.use_count() << endl;

    //weak_ptr = shared_ptr
    spParent->_wpChild = spChild;//不会导致spChild的引用计数加1
    spChild->_spParent = spParent;//引用计数加1
    cout << "spParent' use_count:" << spParent.use_count() << endl;
    cout << "spChild' use_count:" << spChild.use_count() << endl;
}


int main()
{
    test0();
    return 0;
}

