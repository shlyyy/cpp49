#include <iostream>
#include <set>
#include <vector>
#include <utility>

using std::cout;
using std::endl;
using std::multiset;
using std::pair;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    //multiset的特征
    //1、key值是不唯一的，可以重复
    //2、默认情况下，会按照key值升序排列
    //3、multiset的底层实现是红黑树结构
    multiset<int> number = {1, 2, 5, 9, 8, 6, 4, 2, 2, 6};
    /* multiset<int, std::greater<int>> number = {1, 2, 5, 9, 8, 6, 4, 2, 2, 6}; */
    display(number);

    cout << endl << "multiset的查找方法" << endl;
    size_t cnt1 = number.count(2);
    size_t cnt2 = number.count(10);
    cout << "cnt1 = " << cnt1 << endl;
    cout << "cnt2 = " << cnt2 << endl;

    cout << endl;
    multiset<int>::iterator it = number.find(6);
    /* auto it = number.find(6); */
    if(it == number.end())
    {
        cout << "该元素是不存在multiset中" << endl;
    }
    else
    {
        cout <<"该元素存在multiset中 " << *it << endl;
    }

    cout << endl << "set的bound函数" << endl;
    auto it3 = number.lower_bound(2);
    cout << "*it3 = " << *it3 << endl;

    auto it4 = number.upper_bound(2);
    cout << "*it4 = " << *it4 << endl;
    while(it3 != it4)
    {
        cout << *it3 << "  ";
        ++it3;
    }

    cout << endl;
    pair<multiset<int>::iterator, multiset<int>::iterator> 
        ret = number.equal_range(2);
    while(ret.first != ret.second)
    {
        cout << *ret.first << "  ";
        ++ret.first;
    }
    cout << endl;

    cout << endl << "multiset的插入操作" << endl;
    number.insert(7);
    display(number);

    cout << endl;
    vector<int> vec = {3, 11, 77, 22};
    number.insert(vec.begin(), vec.end());
    display(number);

    number.insert({88, 77, 10, 23});
    display(number);

    /* cout << endl << "multiset的下标操作" << endl; */
    /* cout << "number[1] = " << number[1] << endl;//error */
    auto it2 = number.begin();
    cout << "*it2 = " << *it2 << endl;
    /* *it2 = 100;//error,不支持修改 */
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

