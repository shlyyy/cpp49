#include <iostream>
#include <set>
#include <vector>
#include <utility>

using std::cout;
using std::endl;
using std::set;
using std::pair;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    //set的特征
    //1、key值是唯一的，不能重复
    //2、默认情况下，会按照key值升序排列
    //3、set的底层实现是红黑树结构
    set<int> number = {1, 2, 5, 9, 8, 6, 4, 2, 2, 6};
    display(number);

    cout << endl << "set的查找方法" << endl;
    size_t cnt1 = number.count(2);
    size_t cnt2 = number.count(10);
    cout << "cnt1 = " << cnt1 << endl;
    cout << "cnt2 = " << cnt2 << endl;

    cout << endl;
    set<int>::iterator it = number.find(6);
    /* auto it = number.find(6); */
    if(it == number.end())
    {
        cout << "该元素是不存在set中" << endl;
    }
    else
    {
        cout <<"该元素存在set中 " << *it << endl;
    }

    cout << endl << "set的插入操作" << endl;
    pair<set<int>::iterator, bool> ret = number.insert(7);
    if(ret.second)
    {
        cout << "该元素插入成功 " << *ret.first << endl;
    }
    else
    {
        cout << "该元素已经存在set中，插入失败" << endl;
    }
    display(number);

    cout << endl;
    vector<int> vec = {3, 11, 77, 22};
    number.insert(vec.begin(), vec.end());
    display(number);

    number.insert({88, 77, 10, 23});
    display(number);

    /* cout << endl << "set的下标操作" << endl; */
    /* cout << "number[1] = " << number[1] << endl;//error */
    auto it2 = number.begin();
    cout << "*it2 = " << *it2 << endl;
    /* *it2 = 100;//error,不支持修改 */
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

