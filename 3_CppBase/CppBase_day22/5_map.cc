#include <iostream>
#include <map>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::pair;
using std::make_pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first 
             << "  " 
             << elem.second << endl;
    }
}

void test()
{
    //map的特征
    //1、存放的是键值对形式key-value，key值是唯一的
    //不能重复，但是value值是可以重复的
    //2、默认情况下，会按照key值升序排列
    //3、底层使用的是红黑树
    map<int, string> number = {
        pair<int, string>(2, "beijing"),
        pair<int, string>(7, "nanjing"),
        {3, "wuhan"},
        {3, "wuhan"},
        make_pair(1, "wangdao"),
        make_pair(4, "wangdao"),
    };
    display(number);

    cout << endl << "map的查找操作" << endl;
    size_t cnt1 = number.count(3);
    cout << "cnt1 = " << cnt1 << endl;

    cout << endl;
    map<int, string>::iterator it = number.find(10);
    if(it != number.end())
    {
        cout << "该元素存在map中 " 
             << it->first << "  "
             << it->second << endl;
    }
    else
    {
        cout << "该元素不存在map中" << endl;
    }

    cout << endl << "map的insert操作" << endl;
    pair<map<int, string>::iterator, bool> 
        /* auto */
        /* ret = number.insert(pair<int, string>(5, "hubei")); */
        /* ret = number.insert({5, "hubei"}); */
        ret = number.insert(make_pair(5, "hubei"));
    if(ret.second)
    {
        cout << "插入成功 " 
             << ret.first->first << "  "
             << ret.first->second << endl;
    }
    else
    {
        cout << "插入失败，该元素存在map中" << endl;
    }
    display(number);

    cout << endl << endl;
    number.insert({{1, "2wangdao"}, {8, "dongjing"}, {10, "huangshi"}});
    display(number);

    cout << endl << "map的下标" << endl;
    //下标中传递的是key值，如果key存在，会返回value值，如果key
    //不存在，会返回空
    cout << "number[1] = " << number[1] << endl;//查找
    cout << "number[6] = " << number[6] << endl;//插入
    display(number);

    cout << endl << endl;
    //T &operator[](const Key &key)
    number[6] = "wuhan";//修改
    number[5] = "wuhan";
    number.operator[](6).operator=("wuhan");
    display(number);

    /* map<int, string> mm = {1, "wangdao"); */
    /* const map<int, string> mm = { */
    /*     pair<int, string>(2, "beijing"), */
    /* }; */
    /* /1* mm[2];//error *1/ */
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

