#include <math.h>
#include <iostream>
#include <map>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::pair;
using std::make_pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first 
             << "  " 
             << elem.second << endl;
    }
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {}

    double getDistance() const
    {
        return hypot(_ix, _iy);
    }

    int getX() const
    {
        return _ix;
    }

    int getY() const
    {
        return _iy;
    }

    ~Point()
    {}

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
        << ", " << rhs._iy
        << ")";

    return os;
}

void test()
{
    map<string, Point> number = {
        pair<string, Point>("1", Point(1, 2)),
        pair<string, Point>("hello", Point(1, 2)),
        {"wangdao", Point(3, 4)},
        {"hello", Point(3, 4)},
        make_pair("2", Point(4, 5)),
        make_pair("5", Point(4, 5)),
    };
    display(number);
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

