#include <iostream>
#include <list>

using std::cout;
using std::endl;
using std::list;

void test()
{
    //初始化
    //1、无参对象
    /* list<int> number; */
    /* list<int> number2();//number2是函数名 */
    //2、count个value
    /* list<int> number(10, 1); */
    //3、迭代器范围的形式
    /* int arr[10] = {1, 3, 5, 7, 9, 8, 6, 4, 2, 0}; */
    /* list<int> number(arr, arr + 10);//[arr,arr + 10)左闭右开 */
    //4、拷贝构造或者移动构造
    /* list<int> number2(number); */
    //5、大括号
    list<int> number = {1, 3, 5, 7, 9, 2, 4, 6, 8};

    //遍历
    //1、使用下标
#if 0
    for(size_t idx = 0; idx != number.size(); ++idx)
    {
        cout << number[idx] << "  ";
    }
    cout << endl;
#endif

    //2、迭代器的形式
    //迭代器可以看成是指针，但是不完全等同于指针。泛型指针
    list<int>::iterator it;
    for(it = number.begin(); it != number.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;

#if 0
    list<int>::iterator it2;
    for(it2 = number2.begin(); it2 != number2.end(); ++it2)
    {
        cout << *it2 << "  ";
    }
    cout << endl;
#endif
    //3、初始化迭代器
    list<int>::iterator it3 = number.begin();
    for(; it3 != number.end(); ++it3)
    {
        cout << *it3 << "  ";
    }
    cout << endl;

    //4、for与auto结合
    for(auto &elem : number)
    {
        cout << elem << "  ";
    }
    cout << endl;


}

int main(int argc, char **argv)
{
    test();
    return 0;
}

