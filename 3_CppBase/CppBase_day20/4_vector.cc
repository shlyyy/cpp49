#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    vector<int> number = {1, 3, 5, 7, 9, 6, 8, 4};
    display(number);

    cout << endl << "在vector尾部进行插入与删除" << endl;
    number.push_back(12);
    number.push_back(13);
    display(number);
    number.pop_back();
    display(number);
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

