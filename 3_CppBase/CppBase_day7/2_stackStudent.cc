#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Student
{
public:
    Student(int id, const char * name)
    : _id(id)
    , _name(new char[strlen(name) + 1]())
    {
        strcpy(_name, name);
        cout << "Student(int,const char*)" << endl;
    }


    ~Student() {
        if(_name) {
            delete [] _name;
            _name = nullptr;
            cout << "~Student()" << endl;
        }
    }

public:
    void print() const {
        cout << "id:" << _id << endl
             << "name:" << _name << endl;
    }

private://因为并不会调用以下函数，只需要申明为private即可
    static void * operator new(size_t sz);
    static void operator delete(void * p);

private:
    int _id;
    char * _name;
};

void test0()
{
    //Student * pstu = new Student(101, "Jackie"); //error
    //pstu->print();
    //delete pstu;

    Student s2(102, "Rose");
    s2.print();
}


int main()
{
    test0();
    return 0;
}

