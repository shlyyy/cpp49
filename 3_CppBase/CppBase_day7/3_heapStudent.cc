#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Student
{
public:
    Student(int id, const char * name)
    : _id(id)
    , _name(new char[strlen(name) + 1]())
    {
        strcpy(_name, name);
        cout << "Student(int,const char*)" << endl;
    }

    void destroy() 
    {
        //this->~Student();
        delete this;
    }


private:
    //不能生栈对象
    ~Student() {
        if(_name) {
            delete [] _name;
            _name = nullptr;
            cout << "~Student()" << endl;
        }
    }

public:
    void print() const {
        cout << "id:" << _id << endl
             << "name:" << _name << endl;
    }

    //显式定义出来，就是为了覆盖系统的同名函数
    void * operator new(size_t sz)
    {
        cout << "void * operator new(size_t)" << endl;
        return malloc(sz);
    }

    void operator delete(void * p)
    {
        cout << "void operator delete(void*)" << endl;
        free(p);
    }

private:
    int _id;
    char * _name;
};

void test0()
{
    Student * pstu = new Student(101, "Jackie");
    pstu->print();
    //delete pstu;//error
    pstu->destroy();

    //Student s2(102, "Rose");//error
    //s2.print();
}


int main()
{
    test0();
    return 0;
}

