#include <limits>
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::istream;//通用输入流


void printStreamStatus(istream & is)
{
    cout << "is's goodbit:" << is.good() << endl;
    cout << "is's badbit:" << is.bad() << endl;
    cout << "is's eofbit:" << is.eof() << endl;
    cout << "is's failbit:" << is.fail() << endl;
}

void test0()
{
    int number = 0;
    printStreamStatus(cin);
    cin >> number;
    cout << "number:" << number << endl;
    printStreamStatus(cin);

    cin.clear();//恢复流的状态
    //清空缓冲区
    cin.ignore(1024, '\n');
    printStreamStatus(cin);

    string line;
    cin >> line;

    cout << "line:" << line << endl;

}
//number是一个传入传出参数
void readInteger(istream & is, int & number)
{
    cout << "pls input a valid integer:" << endl;
    //逗号表达式的返回值只与最后一个表达式的值有关
    while(is >> number, !is.eof()) {
        if(cin.bad()) {
            cout << "istream has broken!" << endl;
            return;
        } else if(is.fail()) {
            is.clear();
            is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            cout << "pls input a valid integer:" << endl;
        } else {
            //有效状态
            cout << "number:" << number << endl;
            break;
        }
    }
}

void test1()
{
    int num = 0;
    readInteger(cin, num);
}


int main()
{
    /* test0(); */
    test1();
    return 0;
}

