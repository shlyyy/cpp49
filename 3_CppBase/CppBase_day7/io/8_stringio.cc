#include <sstream>
#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::istringstream;
using std::ostringstream;

void test0()
{
    //将其他类型转换为字符串类型
    int num1 = 512;
    int num2 = 1024;
    ostringstream oss;
    oss << "num1= " << num1 << " num2= " << num2;
    string str = oss.str();
    cout << str << endl;

    //将字符串类型转换成其他类型
    string key;
    int number;
    istringstream iss(str);
    //输入流运算符是以空格 \t  \n 作为分隔符
    while(iss >> key >> number) {
        cout << key << " " << number << endl;
    }
}

//将整型数据转换成字符串
string int2str(int number)
{   //sprintf
    ostringstream oss;
    oss << number;
    return oss.str();
}

//读取配置文件
void readConfiguration(const string & filename)
{
    ifstream ifs(filename) ;
    if(!ifs) {
        cout << "ifstream open file " << filename << " error\n";
        return;
    }
    string line;
    string key, value;
    while(std::getline(ifs,line)) {
        istringstream iss(line);
        iss >> key >> value;
        cout << key << " --> " << value << endl; 
    }
}

void test1()
{
    readConfiguration("myserver.conf");
}


int main()
{
    /* test0(); */
    test1();
    return 0;
}

