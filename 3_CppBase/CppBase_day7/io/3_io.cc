#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

void test0()
{
    cout << "hello,cout" << endl;
    cerr << "hello,cerr" << endl;
}


int main()
{
    test0();
    return 0;
}

