#include <fstream>
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
using std::istream;
using std::fstream;

void printStreamStatus(istream & is)
{
    cout << "is's goodbit:" << is.good() << endl;
    cout << "is's badbit:" << is.bad() << endl;
    cout << "is's eofbit:" << is.eof() << endl;
    cout << "is's failbit:" << is.fail() << endl;
}
void test0()

{
    //文件输入输出流要求文件必须存在
    fstream fs("mytest2.txt");
    if(!fs) {
        cout << "fstream open file error\n";
        return;
    }

    int number = 0;
    for(int i = 0; i < 5; ++i) {
        cin >> number;
        fs << number << " ";
    }

    //printStreamStatus(fs);
    //cout << endl;
    fs.seekg(0);//偏移到文件流的开始位置

    for(int i = 0; i < 5; ++i) {
        fs >> number;
        //printStreamStatus(fs);
        //cout << endl;

        cout << number << " ";
    }
    cout << endl;

}


int main()
{
    test0();
    return 0;
}

