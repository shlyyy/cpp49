#include <vector>
#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::ifstream;

void test0()
{
    //文件输入流要求文件必须存在,否则会出错
    ifstream ifs;
    //ifs.open("mytest.txt");
    ifs.open("1_io.cc");
    if(!ifs.good()) {
        cout << "ifstream open file error!" << endl;
        return;
    }

    string word;
    //>> 输入流运算符以空格 \t  \n 作为分隔符
    while(ifs >> word){
        cout << word << endl;
    }

    ifs.close();//切记关闭流对象
}

void test1()
{
    //文件输入流要求文件必须存在,否则会出错
    ifstream ifs;
    //ifs.open("mytest.txt");
    ifs.open("1_io.cc");
    if(!ifs) {
        cout << "ifstream open file error!" << endl;
        return;
    }

    //1. 每次读取一行数据
    char buff[1024] = {0};
    while(ifs.getline(buff, 1024)){
        cout << buff << endl;
    }

    ifs.close();//切记关闭流对象
}

void test2()
{
    //文件输入流要求文件必须存在,否则会出错
    ifstream ifs;
    //ifs.open("mytest.txt");
    ifs.open("1_io.cc");
    if(!ifs) {
        cout << "ifstream open file error!" << endl;
        return;
    }

    vector<string> file;
    file.reserve(100);
    //2.每次读取一行数据 (后续用得更多一些)
    string line;//会自己解决空间的问题
    while(std::getline(ifs, line)){
        //cout << line << endl;
        file.push_back(line);
    }

    for(auto & line : file) {
        cout << line << endl;
    }

    ifs.close();//切记关闭流对象
}
int main()
{
    /* test0(); */
    /* test1(); */
    test2();
    return 0;
}

