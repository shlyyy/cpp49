#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::ifstream;
using std::string;

void test0()
{
    ifstream ifs("1_io.cc");
    if(!ifs) {
        cout << "ifstream open file error\n";
        return;
    }

    //先将文件游标偏移到文件末尾
    ifs.seekg(0, std::ios::end);//
    long length = ifs.tellg();
    cout << "length:" << length << endl;

    //读取整个文件的内容
    ifs.seekg(0);
    char * pbuff = new char[length + 1]();
    ifs.read(pbuff, length);

    string file(pbuff);
    cout << "file:" << file << endl;

    ifs.close();
    delete [] pbuff;//记得回收
}


int main()
{
    test0();
    return 0;
}

