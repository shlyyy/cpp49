#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;
using std::function;

int add(int x, int y, int z)
{
    cout << "int add(int, int, int)" << endl;
    return x + y + z;
}

class Test
{
public:
    int add(int x, int y)
    {
        cout << "int Test::add(int, int)" << endl;
        return x + y;
    }

    int data = 100;//C++11可以直接初始化数据成员
};

void test()
{
    //函数的形态（函数的类型）：函数的返回类型 + 函数的参数列表
    //函数的参数列表：函数的参数的个数、参数的类型、参数的顺序
    //bind可以改变函数的形态(只能改变函数的参数个数)
    //f的类型：int()
    //function可以存函数的类型，所以将function称为函数的容器
    function<int()> f = bind(&add, 1, 2, 3);
    cout << "f() = " << f() << endl;

    Test tst;
    //f2的类型:int()
    function<int()> f2 = bind(&Test::add, &tst, 1, 2);
    cout << "f2() = " << f2() << endl;

    function<int()> f3 = bind(&Test::data, &tst);
    cout << "f3() = " << f3() << endl;

    //占位符
    using namespace std::placeholders;
    //add ===>int(int, int, int)
    //f4的类型:int(int)
    function<int(int)> f4 = bind(&add, 1, _1, 3);
    cout <<"f4(10) = " << f4(10) << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

