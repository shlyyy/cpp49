#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using std::cout;
using std::endl;
using std::vector;
using std::for_each;
using std::mem_fn;
using std::remove_if;

class Number
{
public:
    Number(size_t data = 0)
    : _data(data)
    {

    }

    void print() const
    {
        cout << _data << "  ";
    }

    bool isEven() const
    {
        return (0 == (_data%2));
    }

    bool isPrime() const
    {
        if(1 == _data)
        {
            return false;
        }

        for(size_t idx = 2; idx <= _data/2; ++idx)
        {
            if(0 == _data % idx)
            {
                return false;
            }
        }

        return true;
    }

    ~Number()
    {

    }
private:
    size_t _data;
};

void test()
{
    vector<Number> vec;
    for(size_t idx = 1; idx != 30; ++idx)
    {
        vec.push_back(Number(idx));
        /* vec.emplace_back(idx); */
    }

    /* for_each(vec.begin(), vec.end(), mem_fn(&Number::print)); */
    Number number;
    using namespace std::placeholders;
    /* std::function<void()> f = std::bind(&Number::print, &number); */
    /* for_each(vec.begin(), vec.end(), f);//error */
    for_each(vec.begin(), vec.end(), 
    /*          std::bind(&Number::print, &number)); */
             std::bind(&Number::print, _1));
    cout << endl;

    //想将所有的偶数去掉
    /* vec.erase(remove_if(vec.begin(), vec.end(), */ 
    /*                     mem_fn(&Number::isEven)), vec.end()); */
    cout << endl;
    auto it = remove_if(vec.begin(), vec.end(), 
                        mem_fn(&Number::isEven));
    vec.erase(it, vec.end());
    for_each(vec.begin(), vec.end(), mem_fn(&Number::print));
    cout << endl;

    cout << endl;
    it = remove_if(vec.begin(), vec.end(), 
                        mem_fn(&Number::isPrime));
    vec.erase(it, vec.end());
    for_each(vec.begin(), vec.end(), mem_fn(&Number::print));
    cout << endl;
    
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

