#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

#if 0
int add(int x, int y)
{
    cout << "int add(int, int)" << endl;
    return x + y;
}
#endif
int add(int x, int y, int z)
{
    cout << "int add(int, int, int)" << endl;
    return x + y + z;
}

class Test
{
public:
    int add(int x, int y)
    {
        cout << "int Test::add(int, int)" << endl;
        return x + y;
    }
};

void test()
{
    //函数的形态（函数的类型）：函数的返回类型 + 函数的参数列表
    //函数的参数列表：函数的参数的个数、参数的类型、参数的顺序
    //bind可以改变函数的形态(只能改变函数的参数个数)
    auto f = bind(&add, 1, 2, 3);
    cout << "f() = " << f() << endl;

    Test tst;
    auto f2 = bind(&Test::add, &tst, 1, 2);
    cout << "f2() = " << f2() << endl;

    auto f3 = bind(&Test::add, tst, 10, 20);
    cout << "f3() = " << f3() << endl;
}

int func1()
{
    return 1;
}

int func2()
{
    return 10;
}

int func3(int number)
{
    return number;
}

void test2()
{
    /* func1(); */
    //延迟调用
    //函数指针
    typedef int (*pFunc)();
    pFunc f1 = &func1;//注册回调函数
    //.....
    //...
    cout << "f1() = " << f1() << endl;//执行回调函数

    f1 = func2;
    cout << "f1() = " << f1() << endl;

    /* f1 = func3;//int(int),error */

}

int main(int argc, char **argv)
{
    test2();
    return 0;
}

