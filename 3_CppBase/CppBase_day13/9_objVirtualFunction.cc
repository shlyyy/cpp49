#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    Base(long base)
    : _base(base)
    {   cout << "Base(long)" << endl;   }
    
    //当在基类中定义了虚函数以后，
    //在派生类中同名的函数，只要其形式与基类完全一致，
    //它会自动成为虚函数, 即使不加virtual关键字
    virtual
    void print() const
    {   cout << "Base::_base:" << _base << endl;    }

    virtual 
    void display() const
    {   cout << "Base::display()" << endl;  }

private:
    long _base;
};

class Derived
: public Base
{
public:
    Derived(long base, long derived)
    : Base(base)
    , _derived(derived)
    {   cout << "Derived(long,long)" << endl;   }

//private:
    //派生类覆盖基类的同名虚函数
    //virtual
    void print() const
    {   cout << "Derived::_derived:" << _derived << endl;    }

private:
    long _derived;
};

class Derived2
: public Base
{
public:
    Derived2(long base, long derived2)
    : Base(base)
    , _derived2(derived2)
    {   cout << "Derived2(long,long)" << endl;   }

//private:
    //virtual
    void print() const
    {   cout << "Derived2::_derived:" << _derived2 << endl;    }

private:
    long _derived2;
};

void display(Base base)
{//在编译时，pbase指针只能访问基类的print方法
    base.print();//同一种指令，不同类型的对象执行不同的行为
}

//动态多态被激活的条件：
//1. 基类定义虚函数
//2. 派生类重定义虚函数
//3. 创建派生类对象
//4. 基类指针指向派生类对象
//5. 基类指针调用虚函数

void test0()
{
    cout << "sizeof(Base):" << sizeof(Base) << endl;
    cout << "sizeof(Derived):" << sizeof(Derived) << endl;
    Base base(1);

    Derived derived(10, 11);
    Derived2 derived2(100, 101);
    display(base);
    display(derived);
    display(derived2);
}


int main()
{
    test0();
    return 0;
}

