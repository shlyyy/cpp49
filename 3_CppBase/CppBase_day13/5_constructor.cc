#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    virtual Base(long base)
    : _base(base)
    {   cout << "Base(long)" << endl;   }
private:
    long _base;
};

void test0()
{

}


int main()
{
    test0();
    return 0;
}

