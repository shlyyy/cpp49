#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    Base(long base)
    : _data(base)
    {   cout << "Base(long)" << endl;   }
    
    void print() const
    {   cout << "Base::_data:" << _data << endl;    }

    void print(int x) const
    {
        cout << "Base::print(int) x:" << x << endl;
    }

    void display() const
    {   cout << "Base::display()" << endl;  }

protected:
    long _data;
};

class Derived
: public Base
{
public:
    Derived(long base, long derived)
    : Base(base)
    , _data(derived)
    {   cout << "Derived(long,long)" << endl;   }

    void print() const
    {   
        cout << "Base::_data:" << Base::_data << endl;    
        cout << "Derived::_data:" << _data << endl;    
    }

private:
    long _data;
};


void test0()
{
    cout << "sizeof(Base):" << sizeof(Base) << endl;
    cout << "sizeof(Derived):" << sizeof(Derived) << endl;
    Derived derived(10, 11);

    //发生了隐藏, 当基类定义了同名函数时，
    //如果使用对象调用同名函数，只能调用到派生类的同名函数
    derived.print();//表现的是静态联编, 在编译时就可以确定
    //derived.print(100);//error, 隐藏, 对于基类的不同参数的版本，也是无法直接看到的
    derived.Base::print(100);//加上基类的作用域限定符之后，是可以直接去程序代码区取用
    derived.display();
}


int main()
{
    test0();
    return 0;
}

