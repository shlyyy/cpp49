#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    Base(long base)
    : _base(base)
    {   cout << "Base(long)" << endl;   }
    
    virtual
    void print() const
    {   cout << "Base::_base:" << _base << endl;    }

    virtual 
    void display() const
    {   cout << "Base::display()" << endl;  }

private:
    long _base;
};

class Derived
: public Base
{
public:
    Derived(long base, long derived)
    : Base(base)
    , _derived(derived)
    {   cout << "Derived(long,long)" << endl;   }

    void print() const
    {   cout << "Derived::_derived:" << _derived << endl;    }

private:
    long _derived;
};


void test0()
{
    cout << "sizeof(Base):" << sizeof(Base) << endl;
    cout << "sizeof(Derived):" << sizeof(Derived) << endl;
    Base base(1);
    Derived derived(10, 11);

    //发生了隐藏, 当基类定义了同名函数时，
    //如果使用对象调用同名函数，只能调用到派生类的同名函数
    derived.print();//表现的是静态联编, 在编译时就可以确定
}


int main()
{
    test0();
    return 0;
}

