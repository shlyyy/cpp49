#include <iostream>
using std::cout;
using std::endl;

class C;

class A
{
public:
    void setC(C & c, int ic);
};

class B
{
    friend class A;
};


class C
{
    friend class B;

private:
    int _ic;
};

void A::setC(C & c, int ic)
{
    c._ic = ic;
}

void test0()
{

}


int main()
{
    test0();
    return 0;
}

