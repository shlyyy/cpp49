#include <iostream>
using std::cout;
using std::endl;




//重载了函数调用运算符的类创建的对象, 称为函数对象
struct Foo
{
    Foo(int x = 0, int y = 0)
    : _ix(x)
    , _iy(y)
    {   cout << "Foo(int,int)" << endl; }

    //函数调用运算符
    int operator()(int x, int y)
    {
        ++_count;
        cout << "int operator()(int,int)" << endl;
        return x + y;   
    }

    int operator()(int x, int y, int z)
    {   
        ++_count;
        return x + y + z;   
    }

    //对象的状态
    int _count = 0;
    int _ix = 0;
    int _iy = 0;
};

struct Bar
{
    Bar(int x, int y)
    : _foo(x, y)
    {   cout << "Bar(int,int)" << endl; }

    Foo _foo;
};


int add(int x, int y)
{   //只有一个
    static int count = 0;
    ++count;
    return x + y;  
}

void test0()
{
    int a = 3, b = 4;
    cout << add(a, b) << endl;
    //add.operator()(a, b);

    //add.opeartor+(num);
}

void test1()
{
    int a = 3, b = 4, c = 5;
    Foo foo0(a, b);//调用构造函数
    cout << "foo0(a, b):" << foo0(a, b) << endl;

    //foo称为函数对象, 携带了状态
    Foo foo;//对象已经创建完毕了
    cout << foo.operator()(a, b) << endl;
    cout << "foo(a, b):" << foo(a, b) << endl;
    cout << "foo(a, b, c):" << foo(a, b, c) << endl;
    cout << "foo._count:" << foo._count << endl;
}


int main()
{
    //test0();//函数调用
    //test0.operator()();
    test1();
    return 0;
}

