#include <math.h>
#include <iostream>
using std::cout;
using std::endl;

class Point;//类的前向声明, 告诉编译器是一个类，但还不知道具体内容

class Line
{
public:
    Line() {    cout << "Line()" << endl;   }
    ~Line() {    cout << "~Line()" << endl;   }
    //函数声明
    double getDistance(const Point & lhs, const Point & rhs);
};

class Point
{
public:
    Point(int ix = 0, int iy =0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    void print() const 
    {
        cout << "(" << _ix
             << "," << _iy
             <<")";
    }
    
    //友元之成员函数
    friend double Line::getDistance(const Point & lhs, const Point & rhs);

private:
    int _ix;
    int _iy;
};

double Line::getDistance(const Point & lhs, const Point & rhs)
{
    return sqrt((lhs._ix - rhs._ix) * (lhs._ix - rhs._ix) + 
                (lhs._iy - rhs._iy) * (lhs._iy - rhs._iy));
}

void test0()
{
    Point pt1(1, 2), pt2(3, 4);
    pt1.print();
    cout << " --> ";
    pt2.print();
    //Line line;
    //Line();//临时对象, 匿名对象，生命周期只在这一行

    //cout << "的距离是:" << line.getDistance(pt1, pt2) << endl;
    cout << "的距离是:" << Line().getDistance(pt1, pt2) << endl;
}



int main()
{
    test0();
    return 0;
}

