#include <iostream>
using std::cout;
using std::endl;

//复数分为实部和虚部
//负数 -1 是否能够开方
class Complex
{
public:
    Complex(double dreal = 0, double dimage = 0)
    : _dreal(dreal)
    , _dimage(dimage)
    {   cout << "Complex(double,double)" << endl;   }

    void print() const 
    {
        cout << _dreal;
        if(_dimage > 0)
            cout << " + " << _dimage << "i";
        else if(_dimage < 0)
            cout <<  " - " << (-1) * _dimage << "i";
        cout << endl;
    }

    //当左操作数本身发生了变化时，建议是以成员函数形式重载
    Complex & operator+=(const Complex & rhs)
    {
        cout << "Comple & operator +=(const Complex&)" << endl;
        _dreal += rhs._dreal;
        _dimage += rhs._dimage;
        return *this;
    }

    friend Complex operator+(const Complex & lhs, const Complex & rhs);
private:
    double _dreal;
    double _dimage;
};


//友元普通函数形式重载, 不需要提供getter函数
Complex operator+(const Complex & lhs, const Complex & rhs)
{
    cout << "Complex operator+(lhs, rhs)" << endl;
    return Complex(lhs._dreal + rhs._dreal, lhs._dimage+ rhs._dimage);
}

void test0()
{
    Complex c1(1, 2), c2(3, 4);
    //Complex c3 = c1 + c2;// operator+(c1, c2)
    Complex c3 = operator+(c1, c2);
    cout << "c3:";
    c3.print();

    c1 += c2;
    cout << "c1:";
    c1.print();

    cout << "\nc1 = c1 + c2;\n";
    c1 = c1 + c2;
    cout << "c1:";
    c1.print();


}

void test1()
{
    //内置类型
    int a = 3, b = 4;
    int c = a + b;
    a = a + b;

    a += b;//表达式执行完毕之后，a已经发生了变化

    //&(a + b);//error
}


int main()
{
    test0();
    return 0;
}

