#include <iostream>
using std::cout;
using std::endl;

//复数分为实部和虚部
//负数 -1 是否能够开方
class Complex
{
public:
    Complex(double dreal = 0, double dimage = 0)
    : _dreal(dreal)
    , _dimage(dimage)
    {   cout << "Complex(double,double)" << endl;   }

    void print() const 
    {
        cout << _dreal;
        if(_dimage > 0)
            cout << " + " << _dimage << "i";
        else if(_dimage < 0)
            cout <<  " - " << (-1) * _dimage << "i";
        cout << endl;
    }

    //当左操作数本身发生了变化时，建议是以成员函数形式重载
    Complex & operator+=(const Complex & rhs)
    {
        cout << "Comple & operator +=(const Complex&)" << endl;
        _dreal += rhs._dreal;
        _dimage += rhs._dimage;
        return *this;
    }

    //当自增运算符执行结束之后，当前对象都发生了变化
    //适合以成员函数形式重载
    //前置++
    Complex & operator++()
    {
        ++_dreal;
        ++_dimage;
        return *this;
    }
    //编译器为了区分前置和后置形式，特别规定
    //在后置形式的形参列表之中多加一个int类型
    //int类型只是为了区分前置和后置形式，不需要传参

    //后置++
    Complex operator++(int c)
    {
        cout << "c:" << c << endl;
        Complex tmp(*this);//保存一份变化之前的值
        ++_dreal;
        ++_dimage;
        return tmp;
    }

    friend Complex operator+(const Complex & lhs, const Complex & rhs);
private:
    double _dreal;
    double _dimage;
};


//友元普通函数形式重载, 不需要提供getter函数
Complex operator+(const Complex & lhs, const Complex & rhs)
{
    cout << "Complex operator+(lhs, rhs)" << endl;
    return Complex(lhs._dreal + rhs._dreal, lhs._dimage+ rhs._dimage);
}

void test0()
{
    Complex c1(1, 2), c2(3, 4);
    //Complex c3 = c1 + c2;// operator+(c1, c2)
    Complex c3 = operator+(c1, c2);
    cout << "c3:";
    c3.print();

    c1 += c2;
    cout << "c1:";
    c1.print();

    cout << "\nc1 = c1 + c2;\n";
    c1 = c1 + c2;
    cout << "c1:";
    c1.print();
}

void test1()
{
    cout << "test1()" << endl;
    //内置类型
    int a = 3;

    //&(++a);//左值，返回的就是a本身
    cout << "++a : " << ++a << endl;
    cout << "a:" << a << endl;

    //&(a++);//右值, 产生了一个临时变量,保存就是a变化之前的值
    cout << "a++ : " << a++ << endl;
    cout << "a:" << a << endl;
}

void test2()
{
    Complex c1(1, 1);
    cout << "c1:";
    c1.print();

    cout << "++c1: ";
    (++c1).print();
    cout << "c1:";
    c1.print();
    cout << endl;

    //显式调用前置++
    c1.operator++();
    //显式调用后置++
    c1.operator++(1);
    cout << endl;

    cout << "c1++ :";
    (c1++).print();
    cout << "c1:";
    c1.print();

}



int main()
{
    /* test0(); */
    /* test1(); */
    test2();
    return 0;
}

