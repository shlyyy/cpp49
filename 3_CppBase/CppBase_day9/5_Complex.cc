#include <iostream>
using std::cout;
using std::endl;

//复数分为实部和虚部
//负数 -1 是否能够开方
class Complex
{
public:
    Complex(double dreal = 0, double dimage = 0)
    : _dreal(dreal)
    , _dimage(dimage)
    {   cout << "Complex(double,double)" << endl;   }

    void print() const 
    {
        cout << _dreal;
        if(_dimage > 0)
            cout << " + " << _dimage << "i";
        else if(_dimage < 0)
            cout <<  " - " << (-1) * _dimage << "i";
        cout << endl;
    }

    double real() const {   return _dreal;  }
    double image() const {   return _dimage;  }

private:
    double _dreal;
    double _dimage;
};


//普通函数形式重载
Complex operator+(const Complex & lhs, const Complex & rhs)
{
    return Complex(lhs.real() + rhs.real(), lhs.image() + rhs.image());
}

//内置类型无法重载
/* int operator+(int x, int y) */
/* { */
/*     return x - y; */
/* } */

void test0()
{
    Complex c1(1, 2), c2(3, 4);
    //Complex c3 = c1 + c2;// operator+(c1, c2)
    Complex c3 = operator+(c1, c2);
    cout << "c3:";
    c3.print();

}

void test1()
{
    //内置类型
    int a = 3, b = 4;
    int c = a + b;

    //&(a + b);//error
}


int main()
{
    test0();
    return 0;
}

