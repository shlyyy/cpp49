#include <math.h>
#include <iostream>
using std::cout;
using std::endl;

class Point;//类的前向声明, 告诉编译器是一个类，但还不知道具体内容

class Line
{
public:
    Line() {    cout << "Line()" << endl;   }
    ~Line() {    cout << "~Line()" << endl;   }
    //函数声明
    double getDistance(const Point & lhs, const Point & rhs);
    void setPoint(Point & pt, int ix, int iy);

private:
    int _iz;
};

class Point
{
public:
    Point(int ix = 0, int iy =0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    void print() const 
    {
        cout << "(" << _ix
             << "," << _iy
             <<")";
    }
    
    //友元之友元类
    friend class Line;

    //友元是单向的: Line是Point的友元，但反过来，Point不是Line的友元
    void setZ(Line & line, int iz)
    {
        line._iz = iz;
    }

private:
    int _ix;
    int _iy;
};

double Line::getDistance(const Point & lhs, const Point & rhs)
{
    return sqrt((lhs._ix - rhs._ix) * (lhs._ix - rhs._ix) + 
                (lhs._iy - rhs._iy) * (lhs._iy - rhs._iy));
}
    
void Line::setPoint(Point & pt, int ix, int iy)
{
    pt._ix = ix;
    pt._iy = iy;
}

void test0()
{
    Line line;
    Point pt1(1, 2), pt2(3, 4);
    pt1.print();
    cout << " --> ";
    pt2.print();

    cout << "的距离是:" << line.getDistance(pt1, pt2) << endl;

    line.setPoint(pt2, 10, 11);
    cout << "pt2:";
    pt2.print();
    cout << endl;


}



int main()
{
    test0();
    return 0;
}

