#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

template <class T>
T add(T x, T y);//函数模板的声明

int add(int x,int y)
{   
    cout << "int add(int,int)" << endl;
    return x + y;   
}


//模板的全特化
template <>
const char * add<const char *>(const char * pstr1, const char * pstr2)
{
    //strlen传递的参数是空指针时，会导致段错误
    char * ptmp = new char[strlen(pstr1) + strlen(pstr2) + 1]();
    strcpy(ptmp, pstr1);
    strcat(ptmp, pstr2);
    return ptmp;
}


#if 0
float add(float x, float y)
{   return x + y;   }

double add(double x, double y)
{   return x + y;   }
#endif

void test0()
{
    int i1 = 2, i2 = 3;
    float f1 = 1.1, f2 = 2.2;
    const char * p1 = "hello";
    const char * p2 = "world";
    //cout << "add(i1,i2):" << add(i1, i2) << endl;
    //cout << "add(f1,f2):" << add(f1, f2) << endl;
    cout << "add(p1,p2):" << add(p1, p2) << endl;
}


int main()
{
    test0();
    return 0;
}

#if 1
//函数模板的定义
template <class T>
T add(T x, T y)
{   
    cout << "T add(T,T)" << endl;   
    return x + y;   
}
#endif
