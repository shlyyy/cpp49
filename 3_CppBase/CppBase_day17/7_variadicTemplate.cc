#include <iostream>
using std::cout;
using std::endl;

template <class... Args>
void print(Args... args)
{
    cout << "sizeof...(Args):" << sizeof...(Args) << endl;
    cout << "sizeof...(args):" << sizeof...(args) << endl;
}

void test0()
{
    print(1, 2.2, 'a', "hello");
}


int main()
{
    test0();
    return 0;
}

