#include <iostream>
using std::cout;
using std::endl;

template <class Type>
int print(Type type)
{
    cout << type << endl;
    return 0;
}


//比较取巧的写法
template <class... Args>
void printAll(Args... args)
{
    int arr[] = {(print(args), 0)..., };
    //int arr[] = {(print(2), 0), (print(2.2), 0), (print('a'), 0), (print("hello"), 0)};
}

template <class... Args>
void printAargs(Args... args)
{
    int arr[] = {print(args)... };
}

void test0()
{
    printAll(2, 2.2, 'a', "hello");
    cout << endl;
    printAargs(2, 2.2, 'a', "hello");
}


int main()
{
    test0();
    return 0;
}

