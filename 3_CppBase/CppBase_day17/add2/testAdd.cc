#include "add"
#include <iostream>
using std::cout;
using std::endl;

void test0()
{
    int a = 2, b = 3;
    cout << add(a, b) << endl;
}


int main()
{
    test0();
    return 0;
}

