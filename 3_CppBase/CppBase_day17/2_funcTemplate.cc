#include <iostream>
using std::cout;
using std::endl;

template <class T>
T add(T x, T y);//函数模板的声明

#if 1
template <class T1, class T2>
T1 add(T1 x, T2 y)
{   
    cout << "T1 add(T1, T2)" << endl;
    return x + y;   
}
#endif

int add(int x,int y)
{   
    cout << "int add(int,int)" << endl;
    return x + y;   
}

#if 0
float add(float x, float y)
{   return x + y;   }

double add(double x, double y)
{   return x + y;   }
#endif

void test0()
{
    int i1 = 2, i2 = 3;
    float f1 = 1.1, f2 = 2.2;
    double d1 = 3.333, d2 = 4.444;
    cout << "add(i1,i2):" << add(i1, i2) << endl;
    //隐式实例化
    cout << "add(f1,f2):" << add(f1, f2) << endl;
    //显式实例化
    cout << "add(d1,d2):" << add<float>(d1, d2) << endl;
    //cout << "add(d1,d2):" << add(d1, d2) << endl;
    cout << "add(i1,f2):" << add(i1, f2) << endl;
}


int main()
{
    test0();
    return 0;
}

//函数模板的定义
#if 1
template <class T>
T add(T x, T y)
{   
    cout << "T add(T,T)" << endl;   
    return x + y;   
}
#endif
