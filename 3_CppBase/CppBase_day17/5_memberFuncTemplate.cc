#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(double x, double y)
    : _dx(x)
    , _dy(y)
    {   cout << "Point(double,double)" << endl; }

    //成员函数模板
    template <class T>
    T convert()
    {   return (T)_dx;  }

private:
    double _dx;
    double _dy;
};

void test0()
{
    Point pt(11.11, 22.22);

    cout << pt.convert<int>() << endl;

}


int main()
{
    test0();
    return 0;
}

