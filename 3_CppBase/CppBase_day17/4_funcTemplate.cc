#include <iostream>
using std::cout;
using std::endl;

//模板参数之非类型参数
//
//非类型参数提供了一种新的方式来定义常量
//而且可以根据实际情况做出调整
//相比宏定义和const关键字更有优势一些
//
//模板参数可以设置默认值
template <class T, int kBase = 10>
T multiply(T x, T y)
{
    return x * y * kBase;
}

void test0()
{
    int a = 3, b = 4;
    double d1 = 1.1, d2 = 2.2;
    cout << multiply<double, 8>(a, b) << endl;
    cout << multiply(a, b) << endl;
    cout << multiply(3.14159, d2) << endl;
    //multiply({a, 10}, {b, 10});
}


int main()
{
    test0();
    return 0;
}

