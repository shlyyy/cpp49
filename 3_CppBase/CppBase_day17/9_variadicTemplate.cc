#include <iostream>
using std::cout;
using std::endl;

//print函数递归的出口条件
#if 1
template <class Type>
void print(Type t)
{   cout << t << endl;  }
#endif



template <class T, class... Args>
void print(T t, Args... args) //1 和 n - 1 件
{
    cout << t << " ";
    //调用的过程，那就需要进行参数传递
    print(args...);//...出现在函数参数包的右边时，就是解包的过程
}


void test0()
{
    print(1, 2.2, 'a', "hello");
    //cout << 1 << endl;
    //  print(2.2, 'a', "hello");
    //      cout << 2.2 << endl;
    //          print('a', "hello");
    //              cout << 'a' << endl;
    //                  print("hello");
    
    //int number = 0;
    //LogError("hello %d", number);
}


int main()
{
    test0();
    return 0;
}

