#include <iostream>
using std::cout;
using std::endl;

//函数模板, 以1当3
template <class T>
T add(T x, T y)
{   return x + y;   }

template <class T1, class T2>
T1 add(T1 x, T2 y)
{   return x + y;   }


//模板的实现原理: 模板参数推导 (是在编译时发生)
//
//       实例化
//函数模板 --> 模板函数
//
//模板实质上就是代码生成器

//在实际的机器码中，是以下三个函数，
//而不是上面的函数模板
//
//这三个函数称为模板函数, 即由模板生成的函数
#if 0
int add(int x,int y)
{   return x + y;   }

float add(float x, float y)
{   return x + y;   }

double add(double x, double y)
{   return x + y;   }
#endif

void test0()
{
    int i1 = 2, i2 = 3;
    float f1 = 1.1, f2 = 2.2;
    double d1 = 3.333, d2 = 4.444;
    cout << "add(i1,i2):" << add(i1, i2) << endl;
    //隐式实例化
    cout << "add(f1,f2):" << add(f1, f2) << endl;
    //显式实例化
    cout << "add(d1,d2):" << add<float>(d1, d2) << endl;
    cout << "add(i1,f2):" << add(i1, f2) << endl;
}


int main()
{
    test0();
    return 0;
}

