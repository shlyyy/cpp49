#include <iostream>
using std::cout;
using std::endl;


class Point
{
public:
    //成员函数不占据类型的存储空间, 他们都存放在程序代码区
    explicit //禁止隐式转换
    Point(int x = 0, int y = 0)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int x,int y)" << endl;
    }

    //系统自动提供的拷贝构造函数
    //explicit 不需要它
    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

    //当在类中没有显式定义析构函数时，
    //系统会自动提供一个析构函数
    //~Point() {}
    ~Point()
    {   cout << "~Point()" << endl; }

private:
    int _ix;
    int _iy;
};

void test1()
{
    //Point = int
    //Point pt0 = 10;//这里发生了隐式转换

    Point pt00(10);//显式调用有参构造函数

    Point pt1(1, 2);
    //用一个已经存在的对象初始化另一个新对象
    Point pt2 = pt1;//调用拷贝构造函数
    //Point pt2(pt1);//上面这一行等价于这一行
    cout << "pt1:";
    pt1.print();
    cout << "pt2:";
    pt2.print();


}

void test0()
{
    int a(1);
    int b = a;
    cout << "a:" << a << endl
         << "b:" << b << endl;

    int c = 1, d = 2;
    c = d;
    cout << "c:" << c << endl
         << "d:" << d << endl;
}




int main()
{
    /* test0(); */
    test1();
    return 0;
}

