#include <iostream>
using std::cout;
using std::endl;


class Point
{
public:
    //当没有显式定义构造函数时，
    //系统会自动提供一个默认构造函数
    //或者无参构造函数
    //
    //构造函数可以构成函数重载
#if 0
    Point()
    {   
        cout << "Point()" << endl;
        _ix = 0;
        _iy = 0;
    }

    //有参构造函数
    Point(int x, int y)
    {
        cout << "Point(int x,int y)" << endl;
        _ix = x;
        _iy = y;
    }
#endif


    void print()
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
};

void test0()
{
    //
    Point pt;//已经调用了构造函数
    pt.print();

    /* Point pt2(1, 2); */
    /* cout << "pt2:"; */
    /* pt2.print(); */
}


int main()
{
    test0();
    return 0;
}

