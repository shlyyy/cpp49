
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;


struct Computer {
//默认访问权限是public的
//public:
    //在类内部只有函数声明
    void setBrand(const char * brand);
    void setPrice(float price);
    void print();

private:
    char * _brand;
    float _price;
};


//类名是具有作用域的, 表示一个范围
void Computer::setBrand(const char * brand)
{
    _brand = new char[strlen(brand) + 1]();
    strcpy(_brand, brand);
}

void Computer::setPrice(float price)
{   _price = price; }

void Computer::print()
{
    cout << "brand:" << _brand << endl;
    cout << "price:" << _price << endl;
}


void test0()
{
    Computer pc;//对象
    pc.setBrand("Huawei matebook");
    pc.setPrice(7777);
    pc.print();

}


int main()
{
    test0();
    return 0;
}

