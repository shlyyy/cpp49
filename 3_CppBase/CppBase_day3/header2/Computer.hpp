#ifndef _WD_COMPUTER_H__
#define _WD_COMPUTER_H__


#include <string.h>
#include <iostream>
using std::cout;
using std::endl;


class Computer {
public:
    //在类内部只有函数声明
    void setBrand(const char * brand);
    void setPrice(float price);
    void print();

private:
    char * _brand;
    float _price;
};


#endif

