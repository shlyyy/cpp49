#include "Computer.hpp"

//解决方案二: 将三个函数放到*.cc去实现
void Computer::setBrand(const char * brand)
{
    _brand = new char[strlen(brand) + 1]();
    strcpy(_brand, brand);
}

void Computer::setPrice(float price)
{   _price = price; }

void Computer::print()
{
    cout << "brand:" << _brand << endl;
    cout << "price:" << _price << endl;
}
