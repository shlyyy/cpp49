

#ifndef _WD_COMPUTER_H__
#define _WD_COMPUTER_H__


#include <string.h>
#include <iostream>
using std::cout;
using std::endl;


class Computer {
public:
    //在类内部只有函数声明
    void setBrand(const char * brand);
    void setPrice(float price);
    void print();

private:
    char * _brand;
    float _price;
};


//类名是具有作用域的, 表示一个范围
//解决方案一: 将三个函数设置为inline函数
inline
void Computer::setBrand(const char * brand)
{
    _brand = new char[strlen(brand) + 1]();
    strcpy(_brand, brand);
}

inline
void Computer::setPrice(float price)
{   _price = price; }

inline
void Computer::print()
{
    cout << "brand:" << _brand << endl;
    cout << "price:" << _price << endl;
}
#endif

