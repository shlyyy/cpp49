#include <iostream>
using std::cout;
using std::endl;


class Point
{
public:
    //以一当三
    Point(int x = 0, int y = 0)
    //1. 初始化阶段
    : _ix(x)
    , _iy(y)
    {
        //2. 计算阶段
        cout << "Point(int x,int y)" << endl;
        //_ix = x;//赋值操作
        //_iy = y;
    }


    void print()
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

private:
    int _ix;
    int _iy;
};

void test0()
{
    //
    int a = 1;
    a = 2;//赋值语句
    Point pt;//已经调用了构造函数
    pt.print();

    Point pt2(1, 2);
    cout << "pt2:";
    pt2.print();

    Point pt3(11);// ==> Point pt3(11, 0);
    cout << "pt3:";
    pt3.print();
}


int main()
{
    test0();
    return 0;
}

