
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;


class Computer {
public:
    Computer()
    : _brand(nullptr)
    , _price(0)
    { cout << "Computer()" << endl; }

    Computer(const char * brand, double price)
    : _brand(new char[strlen(brand) + 1]())
    , _price(price)
    {
        cout << "Computer(const char*, double)" << endl;
        strcpy(_brand, brand);
    }

    //系统提供的拷贝构造函数不再满足需求
    /* Computer(const Computer & rhs) */
    /* : _brand(rhs._brand) */
    /* , _price(rhs._price) */
    /* {   cout << "Computer(const Computer&)" << endl;    } */
    
    //必须手动提供
    Computer(const Computer & rhs)
    : _brand(new char[strlen(rhs._brand) + 1]())
    , _price(rhs._price)
    {   
        cout << "Computer(const Computer&)" << endl;    
        strcpy(_brand, rhs._brand);//深拷贝
    }

    void setBrand(const char * brand)
    {
        _brand = new char[strlen(brand) + 1]();
        strcpy(_brand, brand);
    }

    void setPrice(double price)
    {   _price = price; }

    void print() 
    {
        if(_brand) {
            printf("brand's address: %p\n", _brand);
            cout << "brand:" << _brand << endl;
            cout << "price:" << _price << endl;
        }
    }

    ~Computer()
    {
        cout << "~Computer()" << endl;
        if(_brand) {
            delete [] _brand;
            _brand = nullptr;
        }
    }

    

private:
    //当数据成员中出现了指针，该指针指向的是堆空间的资源
    //都需要小心小心再小心
    char * _brand;
    double _price;
};

void test0()
{
    Computer pc1("Huawei matebook", 7777);  
    cout << "pc1:";
    pc1.print();
    Computer pc2 = pc1;
    cout << "pc2:";
    pc2.print();
}


int main()
{
    test0();
    return 0;
}

