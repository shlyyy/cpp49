#include <iostream>
using std::cout;
using std::endl;


class Point
{
public:
    //成员函数不占据类型的存储空间, 他们都存放在程序代码区
    Point(int x = 0, int y = 0)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int x,int y)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

    //当在类中没有显式定义析构函数时，
    //系统会自动提供一个析构函数
    //~Point() {}
    ~Point()
    {   cout << "~Point()" << endl; }

private:
    int _ix;
    int _iy;
};

void test0()
{
    cout << "sizeof(Point):" << sizeof(Point) << endl;
    //局部变量(对象)
    Point pt;//位于test0函数的栈区
    pt.print();

    Point pt2(1, 2);
    cout << "pt2:";
    pt2.print();

    Point pt3(11);
    cout << "pt3:";
    pt3.print();
}


int main()
{
    test0();
    return 0;
}

