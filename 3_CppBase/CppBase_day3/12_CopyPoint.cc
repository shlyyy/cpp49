#include <iostream>
using std::cout;
using std::endl;


class Point
{
public:
    Point(int x = 0, int y = 0)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int x,int y)" << endl;
    }

    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

    ~Point()
    {   cout << "~Point()" << endl; }

private:
    int _ix;
    int _iy;
};

Point func2()
{
    Point pt(21, 22);
    cout << "pt:";
    pt.print();
    return pt;
}

void test2()
{
    Point pt2 = func2();
    //&func2();//error 右值, 临时对象,以过渡的形式出现
    //临时对象用完之后，马上就会被销毁
    Point(11, 12).print();//error 右值, 其生命周期只在这一行
    cout << "1111" << endl;

    //非const的左值引用无法绑定到右值
    //Point & ref = Point(11, 12);//error
    
    //const左值引用可以绑定到右值
    const Point & ref = Point(11, 12);

    Point pt0(31, 22);
    Point pt3 = Point(21, 22);
    //const左值引用还可以绑定到左值
    const Point  &ref4 = pt0;
}

void test0()
{
    int a(1);
    int b = a;
    cout << "a:" << a << endl
         << "b:" << b << endl;

    int c = 1, d = 2;
    c = d;
    cout << "c:" << c << endl
         << "d:" << d << endl;

    &a;
    &b;
    //&(a + b);//error 右值
    //int & ref = a + b;//error
    //非const左值引用只能绑定到左值，无法绑定到右值
    int & ref2 = a;//ok 
    &ref2;//引用本身可以取地址，它是一个左值
    const int & ref3 = a + b;
}




int main()
{
    /* test0(); */
    /* test1(); */
    test2();
    return 0;
}

