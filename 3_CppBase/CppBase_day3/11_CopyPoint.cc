#include <iostream>
using std::cout;
using std::endl;


class Point
{
public:
    Point(int x = 0, int y = 0)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int x,int y)" << endl;
    }

    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

    ~Point()
    {   cout << "~Point()" << endl; }

private:
    int _ix;
    int _iy;
};

void func(Point p)
{
    cout << "p:";
    p.print();
}

void test1()
{
    Point pt(10, 11);
    func(pt);// Point p = pt;
}

Point func2()
{
    Point pt(21, 22);
    cout << "pt:";
    pt.print();
    return pt;
}

void test2()
{
    func2();
}

void test0()
{
    int a(1);
    int b = a;
    cout << "a:" << a << endl
         << "b:" << b << endl;

    int c = 1, d = 2;
    c = d;
    cout << "c:" << c << endl
         << "d:" << d << endl;
}




int main()
{
    /* test0(); */
    /* test1(); */
    test2();
    return 0;
}

