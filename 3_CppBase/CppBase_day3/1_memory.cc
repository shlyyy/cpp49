#include <iostream>
using std::cout;
using std::endl;

//全局静态区的数据未初始化时，会直接清零
int gLobal;//跨模块调用

static int sVariable = 2;//只能在本模块内部使用

static void display()
{   //该函数也只能本模块内部使用
}

//放在文字常量区
const int cVariable = 12;

void test1()
{
    int number;//未初始时,其值是不确定的
    //栈上
    const int localNumber = 100;

    const char * pstr = "hello,world";
    printf("pstr: %p\n", pstr);

    printf("&gLobal: %p\n", &gLobal);
    printf("&number: %p\n", &number);
    printf("&cVariable: %p\n", &cVariable);
    printf("&localNumber: %p\n", &localNumber);
}

void test0()
{
    //函数栈区
    int number;//未初始时,其值是不确定的
    int * p1 = new int(10);
    
    //pstr所指的区域称为文字常量区
    const char * pstr = "hello,world";
    //*pstr = 'H';//error 不能修改
    
    //与pstr所指的是同一个字符串
    printf("%p\n", &"hello,world");

    //字符数组存储在栈上
    char str[] = "hello,world";
    printf("str: %p\n\n", str);
    str[0] = 'H';
    cout << "str:" << str << endl;


    printf("&gLobal: %p\n", &gLobal);
    printf("&sVariable: %p\n", &sVariable);
    printf("&number: %p\n", &number);
    printf("p1: %p\n", p1);//p1所指的区域,堆区
    printf("&p1: %p\n", &p1);//本身存在栈上
    printf("pstr: %p\n", pstr);//pstr所指的区域,堆区
    cout << "pstr:" << pstr << endl;
    cout << "gLobal:" << gLobal << endl;
    cout << "number:" << number << endl;
    printf("&test0: %p\n", test0);


    delete p1;
}


int main()
{
    /* test0(); */
    test1();
    printf("&main: %p\n", main);
    return 0;
}

