
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;


//对于自定义类类型，类名都要首字母大写
//驼峰命名法
class Computer {
//放在类的开始位置的应该该类能够提供的接口、功能、服务
//权限访问修饰符直接顶格写,不用缩进
public:
    //const char* 代表的是C风格的字符串
    void setBrand(const char * brand)
    {
        //_brand = brand;//浅拷贝(只传递指针的地址)
        _brand = new char[strlen(brand) + 1]();
        strcpy(_brand, brand);
    }

    //在类内部直接实现的函数都是inline函数
    void setPrice(float price)
    {   _price = price; }

    void print() 
    {
        cout << "brand:" << _brand << endl;
        cout << "price:" << _price << endl;
    }
    

private://类的数据成员要放在类的靠后的位置，尤其是private成员
    //编程规范
    char * _brand;
    float _price;
};

void test0()
{
    int a;//内置类型

    Computer pc;//对象
    pc.setBrand("Huawei matebook");
    pc.setPrice(7777);
    pc.print();
    //pc._price = 1000;//error 在类之外不能直接访问

}


int main()
{
    test0();
    return 0;
}

