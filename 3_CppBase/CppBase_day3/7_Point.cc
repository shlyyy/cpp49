#include <iostream>
using std::cout;
using std::endl;


class Point
{
public:
    Point(int x)
    : _ix(x)//再初始化_ix的值
    , _iy(_ix)// 先初始化_iy的值, 由于_ix的值不确定的
    {
        cout << "Point(int x,int y)" << endl;
    }


    void print()
    {
        cout << "(" << _ix
             << "," << _iy
             << ")" << endl;
    }

private:
    //数据成员的初始化，与其初始化列表中的初始化的顺序无关，
    //只与其在类中被声明的顺序有关,会按其在类中被声明的顺序
    //进行初始化
    int _iy;
    int _ix;
};

void test0()
{
    //
    int a = 1;
    a = 2;//赋值语句

    Point pt(11);
    cout << "pt:";
    pt.print();
}


int main()
{
    test0();
    return 0;
}

