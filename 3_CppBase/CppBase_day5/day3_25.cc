#include <iostream>
#include <string.h>

using std::cout;
using std::endl;

class Student{
public:
    Student(int num, const char* name, int age)
    :_num(num)// 使用new表达式开辟数组空间时，需要在[]后加上()
    ,_name(new char[strlen(name)+1]())
    ,_age(age)
    {
        strcpy(_name,name);
    }
    void print()
    {
        cout << "My num = " << _num
            << ", My name is " << _name
            << ", My age is " << _age << endl;
    }

    ~Student() {
        if(_name) {
            delete [] _name;
            _name = nullptr;
        }
    }
private:
    int _num;
    char* _name;
    int _age;
};

int main(void)
{
    Student stu1(1001,"xiaoming",23);
    stu1.print();
    return 0;
}
