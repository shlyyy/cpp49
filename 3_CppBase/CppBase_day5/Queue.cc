#include <iostream>
using std::cout;
using std::endl;

class Queue
{
public:
    Queue(int size = 10)
    : _front(0)
    , _rear(0)
    , _size(size)
    , _data(new int[_size]())
    {   cout << "Queue(int)" << endl;   }

    bool empty() const
    {   return _front == _rear;     }
    bool full() const
    {   return _front == (_rear + 1) % _size;    } 

    void push(int num)
    {
        if(!full()) {
            _data[_rear++] = num; 
            _rear %= _size;
        } else {
            cout << "queue is full, cannot push any more data" << endl;
        }
    }

    void pop()
    {
        if(!empty()) {
            ++_front;
            _front %= _size;
        } else {
            cout << "queue is empty, no more data" << endl;
        }
    }

    int front() const { return _data[_front];   }
    int back() const {  return _data[(_rear - 1 + _size) % _size];  }

    int getFrontIdx() const {   return _front;  }
    int getRearIdx() const {   return _rear;  }

private:
    int _front;
    int _rear;
    int _size;
    int * _data;
};

void test0()
{
    Queue que;
    cout << "此时队列中是否为空？" << que.empty() << endl;
    que.push(1);
    cout << "此时队列中是否为空？" << que.empty() << endl;
    for(int i = 2; i < 12; ++i) {
        que.push(i);
    }
    cout << "此时队列中是否已满?" << que.full() << endl << endl;

    cout << "此时队列的队头元素为:" << que.front() << endl;
    cout << "此时队列的队尾元素为:" << que.back() << endl;
    cout << "front的下标:" << que.getFrontIdx() << endl;
    cout << "rear的下标:" << que.getRearIdx() << endl;
    
    que.pop();
    que.push(10);
    cout << "此时队列的队头元素为:" << que.front() << endl;
    cout << "此时队列的队尾元素为:" << que.back() << endl;
    cout << "front的下标:" << que.getFrontIdx() << endl;
    cout << "rear的下标:" << que.getRearIdx() << endl << endl;

    while(!que.empty()) {
        cout << que.front() << endl;
        que.pop();
    }
    cout << "此时队列中是否为空？" << que.empty() << endl;

}


int main()
{
    test0();
    return 0;
}

