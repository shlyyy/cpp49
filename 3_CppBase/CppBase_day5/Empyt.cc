#include <iostream>
using std::cout;
using std::endl;

class A
{};

class B
: public A
{

private:
    int _ix;
};

void test0()
{
    A a1;
    A a2;
    A a3 = a1;

    a2 = a1;
    cout << "sizeof(B): " << sizeof(B) << endl;
    cout << "&a1:" << &a1 << endl;
    cout << "&a2:" << &a2 << endl;
    cout << "sizeof(a1):" << sizeof(a1) << endl;
}


int main()
{
    test0();
    return 0;
}

