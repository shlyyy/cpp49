#include <iostream>
using std::cout;
using std::endl;

#define MAX_SIZE 100

class Stack
{
public:
    Stack(int size = 10)
    : _size(size)
    , _top(-1)
    , _data(new int[_size]())
    {   cout << "Stack(int)" << endl;   }

    bool empty() const;
    bool full() const;
    void push(int num);
    void pop();
    int top() const;

    ~Stack() {
        if(_data) {
            delete [] _data;
            _data = nullptr;
        }
    }


private:
    int _size;
    int _top;
    int * _data;
};

bool Stack::empty() const
{   return _top == -1;  }

bool Stack::full() const
{   return _top == _size - 1;   }

void Stack::push(int num)
{
    if(!full()) {
        _data[++_top] = num;
    } else {
        cout << "stack is full, cannot push any more data" << endl;
    }
}

void Stack::pop()
{
    if(!empty()) {
        --_top;
    } else {
        cout << "stack is empty, no more data" << endl;
    }
}

int Stack::top() const
{   return _data[_top]; }

void test0()
{
    Stack stack;
    cout << "此时栈中是否为空？" << stack.empty() << endl;
    stack.push(1);
    cout << "此时栈中是否为空？" << stack.empty() << endl;
    for(int i = 2; i < 12; ++i) {
        stack.push(i);
    }
    cout << "此时栈中是否已满?" << stack.full() << endl;

    while(!stack.empty()) {
        cout << stack.top() << endl;
        stack.pop();
    }
    cout << "此时栈中是否为空？" << stack.empty() << endl;
}


int main()
{
    test0();
    return 0;
}











