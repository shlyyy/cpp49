#include <49func.h>
int main(int argc, char *argv[])
{
    // ./5_aqiang_udp_client 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);//udp SOCK_DGRAM
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(atoi(argv[2]));
    serverAddr.sin_addr.s_addr = inet_addr(argv[1]);
    // 客户端先sendto
    sendto(sockfd,"zaima",5,0,
           (struct sockaddr *)&serverAddr,sizeof(serverAddr));
    char buf[4096] = {0};
    fd_set rdset;
    while(1){
        FD_ZERO(&rdset);
        FD_SET(STDIN_FILENO,&rdset);
        FD_SET(sockfd,&rdset);
        select(sockfd+1,&rdset,NULL,NULL,NULL);
        if(FD_ISSET(STDIN_FILENO,&rdset)){
            bzero(buf,sizeof(buf));
            ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
            if(sret == 0){//发送一个长度为0的数据报
                sendto(sockfd,buf,0,0,
                       (struct sockaddr *)&serverAddr,
                       sizeof(serverAddr));
                break;
            }
            sendto(sockfd,buf,strlen(buf),0,
                   (struct sockaddr *)&serverAddr,
                   sizeof(serverAddr));
        }
        if(FD_ISSET(sockfd,&rdset)){
            bzero(buf,sizeof(buf));
            ssize_t sret = recvfrom(sockfd,buf,sizeof(buf),0,NULL,NULL);
            if(sret == 0){
                break;
            }
            printf("buf = %s\n", buf);
        }
    }
    close(sockfd);
    return 0;
}

