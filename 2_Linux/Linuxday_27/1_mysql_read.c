#include <49func.h>
#include <mysql/mysql.h>
int main(){
    MYSQL * mysql = mysql_init(NULL);
    MYSQL * ret = mysql_real_connect(mysql,"localhost","root","123","49test",0,NULL,0);
    if(ret == NULL){
        fprintf(stderr,"error:%s\n", mysql_error(mysql));
        return -1;
    }
    char sql[] = "delete from star where name = '123';";
    int qret = mysql_query(mysql,sql);
    if(qret != 0){
        fprintf(stderr,"error 1:%s\n", mysql_error(mysql));
        return -1;
    }
    strcpy(sql,"select * from star;");
    qret = mysql_query(mysql,sql);
    if(qret != 0){
        fprintf(stderr,"error 2:%s\n", mysql_error(mysql));
        return -1;
    }
    MYSQL_RES *res = mysql_store_result(mysql);
    printf("total rows = %lu\n", mysql_num_rows(res));
    MYSQL_ROW row;
    while((row = mysql_fetch_row(res)) != NULL){
        // 取出了一行
        for(unsigned int i = 0; i < mysql_num_fields(res); ++i){
            printf("%s\t", row[i]);
        }
        printf("\n");
    }
    mysql_close(mysql);
}
