#include <49func.h>
int main(int argc, char *argv[])
{
    const char query[] = "GET / HTTP/1.0\r\n"
                         "Host: www.baidu.com\r\n"
                         "\r\n";
    const char hostname[] = "www.baidu.com";
    struct hostent *phost = gethostbyname(hostname);
    char ip[1024] = {0};
    inet_ntop(AF_INET,phost->h_addr_list[0],ip,sizeof(ip));

    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(80);
    //serverAddr.sin_addr.s_addr = inet_addr(ip);
    memcpy(&serverAddr.sin_addr,phost->h_addr_list[0],4);
    int ret = connect(sockfd,(struct sockaddr *)&serverAddr,sizeof(serverAddr));
    ERROR_CHECK(ret,-1,"connect");
    send(sockfd,query,strlen(query),0);
    char text[8192] = {0};
    while(1){
        bzero(text,sizeof(text));
        size_t sret = recv(sockfd,text,sizeof(text),0);
        printf("text = %s\n", text);
        if(sret == 0){
            break;
        }
    }
    close(sockfd);
    return 0;
}

