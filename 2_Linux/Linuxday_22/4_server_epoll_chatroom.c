#include <49func.h>
typedef struct conn_s {
    int netfd;
    int isalive;
    time_t lastactive;
} conn_t;
int main(int argc, char *argv[])
{
    // ./4_server_epoll_chatroom 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in addr;//服务端地址
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    int reuse = 1; // SO_REUSEADDR属性的参数
    int ret = setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&reuse,sizeof(int));
    ERROR_CHECK(ret,-1,"setsockopt"); 
    ret = bind(sockfd, (struct sockaddr *)&addr,sizeof(addr));
    ERROR_CHECK(ret,-1,"bind");
    listen(sockfd,10);

    conn_t clientArr[1024]; //保存所有连入客户端的信息
    int curidx = 0;//保存下一个连入客户端的下标

    char buf[4096];
    int epfd = epoll_create(1);
    struct epoll_event event;
    event.data.fd = sockfd;
    event.events = EPOLLIN; // sockfd accept 对应的也是读行为
    epoll_ctl(epfd,EPOLL_CTL_ADD,sockfd,&event);
    time_t curtime;
    while(1){
        struct epoll_event readySet[1024];
        int readyNum = epoll_wait(epfd,readySet,1024,1000); //等待至多1000ms
        for(int i = 0; i < readyNum; ++i){
            if(readySet[i].data.fd == sockfd){
                struct sockaddr_in clientAddr;
                socklen_t clientAddrSize = sizeof(clientAddr);//该变量必须初始化
                int netfd = accept(sockfd,(struct sockaddr *)&clientAddr,&clientAddrSize);
                printf("netfd = %d\n",netfd);
                printf("client ip = %s, port = %d\n", 
                       inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));
                clientArr[curidx].netfd = netfd;
                clientArr[curidx].isalive = 1; // 1 表示存活 0 表示已经断开
                clientArr[curidx].lastactive = time(NULL); // 初始化活跃时间
                printf("curidx = %d, lastactive = %lu\n",curidx, clientArr[curidx].lastactive);
                event.data.fd = netfd;
                event.events = EPOLLIN;
                epoll_ctl(epfd,EPOLL_CTL_ADD,netfd,&event);
                ++curidx;
            }
            else{
                // 从readySet[i].data.fd中去读取数据，转发给所有其他活跃的客户端
                bzero(buf,sizeof(buf));
                ssize_t sret = recv(readySet[i].data.fd,buf,sizeof(buf),0);
                if(sret == 0){
                    for(int j = 0; j < curidx; ++j){
                        if(clientArr[j].isalive == 1 && clientArr[j].netfd == readySet[i].data.fd){
                            clientArr[j].isalive = 0;
                            epoll_ctl(epfd,EPOLL_CTL_DEL,clientArr[j].netfd,NULL);//移除监听
                            close(clientArr[j].netfd);
                            break;
                        }
                    }
                }
                for(int j = 0; j < curidx; ++j){
                    if(clientArr[j].isalive == 0 || clientArr[j].netfd == readySet[i].data.fd){
                        continue;//如果是已经断开的 or 自己本身就跳过
                    }
                    send(clientArr[j].netfd,buf,strlen(buf),0);
                }
                for(int j = 0; j < curidx; ++j){
                    if(clientArr[j].isalive == 1 && clientArr[j].netfd == readySet[i].data.fd){
                        clientArr[j].lastactive = time(NULL);
                        printf("j = %d, lastactive = %lu\n",j, clientArr[j].lastactive);
                        break;
                    }
                }
            }
        }
        curtime = time(NULL);
        printf("curtime = %s\n",ctime(&curtime));
        for(int i = 0; i < curidx; ++i){
            if(clientArr[i].isalive == 1 && curtime - clientArr[i].lastactive > 5){
                printf("i = %d, lastactive = %lu\n",i, clientArr[i].lastactive);
                clientArr[i].isalive = 0;
                epoll_ctl(epfd,EPOLL_CTL_DEL,clientArr[i].netfd,NULL);
                close(clientArr[i].netfd);
            }
        }
    }
    return 0;
}


