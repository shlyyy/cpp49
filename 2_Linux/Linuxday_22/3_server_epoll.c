#include <49func.h>
int main(int argc, char *argv[])
{
    // ./3_server 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in addr;//服务端地址
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    int reuse = 1; // SO_REUSEADDR属性的参数
    int ret = setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&reuse,sizeof(int));
    ERROR_CHECK(ret,-1,"setsockopt"); 
    ret = bind(sockfd, (struct sockaddr *)&addr,sizeof(addr));
    ERROR_CHECK(ret,-1,"bind");
    listen(sockfd,10);
    struct sockaddr_in clientAddr;
    socklen_t clientAddrSize = sizeof(clientAddr);//该变量必须初始化
    //socklen_t clientAddrSize = 0;
    int netfd = accept(sockfd,(struct sockaddr *)&clientAddr,&clientAddrSize);
    printf("netfd = %d\n",netfd);
    printf("client ip = %s, port = %d\n", 
           inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));
    // 1 fd_set rdset;
    int epfd = epoll_create(1); //epoll_create 取代定义fd_set
    // 2 设置监听 取代FD_SET 
    // 如果使用epoll_ctl 可以在循环外面使用
    struct epoll_event event;
    event.data.fd = STDIN_FILENO;
    event.events = EPOLLIN;
    epoll_ctl(epfd,EPOLL_CTL_ADD,STDIN_FILENO,&event); // FD_SET
    event.data.fd = netfd;
    event.events = EPOLLIN;
    epoll_ctl(epfd,EPOLL_CTL_ADD,netfd,&event);
    char buf[4096];

    while(1){
        //FD_ZERO(&rdset);
        //FD_SET(STDIN_FILENO,&rdset);
        //FD_SET(netfd,&rdset);
        //3 select(netfd+1,&rdset,NULL,NULL,NULL);
        struct epoll_event readySet[2];
        int readyNum = epoll_wait(epfd,readySet,2,-1);
       // if(FD_ISSET(STDIN_FILENO,&rdset)){
       //     bzero(buf,sizeof(buf));
       //     ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
       //     if(sret == 0){
       //         send(netfd,"nishigehaoren",13,0);
       //         break;
       //     }
       //     send(netfd,buf,sret,0);
       // }
       // if(FD_ISSET(netfd,&rdset)){
       //     bzero(buf,sizeof(buf));
       //     ssize_t sret = recv(netfd,buf,sizeof(buf),0);
       //     if(sret == 0){
       //         printf("hehe\n");
       //         break;
       //     }
       //     printf("buf = %s\n", buf);
       // }
        for(int i = 0; i < readyNum; ++i){
            if(readySet[i].data.fd == STDIN_FILENO){
                bzero(buf,sizeof(buf));
                ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
                if(sret == 0){
                    send(netfd,"nishigehaoren",13,0);
                    goto end;
                }
                send(netfd,buf,sret,0);
            }
            else if(readySet[i].data.fd == netfd){
                bzero(buf,sizeof(buf));
                ssize_t sret = recv(netfd,buf,sizeof(buf),0);
                if(sret == 0){
                    printf("hehe\n");
                    goto end;
                }
                printf("buf = %s\n", buf);
            }
        }
    }
end:
    close(netfd);
    close(sockfd);
    return 0;

}

