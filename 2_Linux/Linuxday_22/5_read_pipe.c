#include <49func.h>
int setnonblock(int fd){
    int flag = fcntl(fd,F_GETFL);//获取已经打开的fd的属性
    flag = flag|O_NONBLOCK;//增加一个非阻塞属性
    int ret =fcntl(fd,F_SETFL,flag);//修改fd的属性
    ERROR_CHECK(ret,-1,"fcntl");
    return 0;
}
int main(int argc, char *argv[])
{
    int fd = open("1.pipe",O_RDONLY);
    setnonblock(fd);
    char buf[1024] = {0};
    while(1){
        bzero(buf,sizeof(buf));
        ssize_t sret = read(fd,buf,3);
        printf("sret = %ld, buf = %s\n", sret, buf);
        sleep(1);
    }
    return 0;
}

