#include <49func.h>
int main(int argc, char *argv[])
{
    int fd = open("file1",O_RDONLY);
    char buf[1024] = {0};
    while(1){
        bzero(buf,sizeof(buf));
        ssize_t sret = read(fd,buf,3);
        printf("sret = %ld, buf = %s\n", sret, buf);
        if(sret == 0){
            break;
        }
        sleep(1);
    }
    return 0;
}

