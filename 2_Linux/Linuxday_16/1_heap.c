#include <49func.h>
void *threadFunc(void *arg){
    // arg接收了传入的参数（pHeap）
    // void * 传入是什么类型，就恢复成什么类型
    int *pHeap = (int *)arg;
    printf("I am child, *pHeap = %d\n", *pHeap);
    ++*pHeap;
}
int main()
{
    int *pHeap = (int *)malloc(sizeof(int));
    *pHeap = 1;
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,pHeap);
    sleep(1);
    printf("I am main, *pHeap = %d\n", *pHeap);
    return 0;
}

