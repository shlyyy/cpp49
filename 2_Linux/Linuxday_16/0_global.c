#include <49func.h>
int global = 0;
//extern int global;
void *threadFunc(void *arg){
    printf("I am child thread, global = %d\n", global);
    ++global;
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    sleep(1);
    printf("I am main thread, global = %d\n", global);
    return 0;
}

