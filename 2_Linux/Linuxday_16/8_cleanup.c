#include <49func.h>
void cleanup1(void *arg){
    // arg 直接传入p1
    printf("cleanup1\n");
    // free p1
    free(arg);
}
void cleanup2(void *arg){
    // arg 直接传入p2
    printf("cleanup2\n");
    // free p2
    free(arg);
}
void cleanup3(void *arg){
    // arg 传入fd3的地址
    int *pFd3 = (int *)arg;
    printf("cleanup3\n");
    // close fd3
    close(*pFd3);
}
void *threadFunc(void *arg){
    //pthread_exit(NULL);
    void *p1 = malloc(4);//申请资源之后，马上压栈
    pthread_cleanup_push(cleanup1,p1);//cleanup_push不是一个取消点
    //pthread_exit(NULL);
    void *p2 = malloc(4);
    pthread_cleanup_push(cleanup2,p2);
    //pthread_exit(NULL);
    int fd3 = open("file1",O_RDWR);
    pthread_cleanup_push(cleanup3,&fd3);
    // ...
    //pthread_exit(NULL);
    //close(fd3);
    return NULL;
    pthread_cleanup_pop(1);//把原来释放资源的行为，替换成弹栈
    //free(p2);
    pthread_cleanup_pop(1);
    //free(p1);
    pthread_cleanup_pop(1);
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    void *ret;
    pthread_join(tid,&ret);
    printf("ret = %ld\n", (long )ret);
    return 0;
}

