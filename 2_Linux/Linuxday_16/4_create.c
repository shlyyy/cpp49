#include <49func.h>
void * threadFunc(void *arg){
    //long val = (long )arg;
    //printf("I am child, val = %ld\n", val);
    long *pVal = (long *)arg;
    printf("I am child, val = %ld\n", *pVal);
    ++*pVal;
}
int main()
{
    pthread_t tid1,tid2,tid3;
   // long val1 = 1001;
   // long val2 = 1002;
   // long val3 = 1003;
   // pthread_create(&tid1,NULL,threadFunc,(void *)val1);
   // pthread_create(&tid2,NULL,threadFunc,(void *)val2);
   // pthread_create(&tid3,NULL,threadFunc,(void *)val3);
    long val = 1001;
    pthread_create(&tid1,NULL,threadFunc,&val);
    //sleep(1);
    //++val;
    pthread_create(&tid2,NULL,threadFunc,&val);
    //++val;
    pthread_create(&tid3,NULL,threadFunc,&val);
    sleep(1);
    return 0;
}

