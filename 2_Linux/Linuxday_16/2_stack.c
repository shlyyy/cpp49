#include <49func.h>
void *threadFunc(void *arg){
    int * pStack = (int *)arg;
    printf("I am child, stack = %d\n", *pStack);
    ++*pStack;
}
void func(){
    int stack = 1;
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&stack);
}
int main()
{
    //int stack = 1;
    //pthread_t tid;
    //pthread_create(&tid,NULL,threadFunc,&stack);
    func();
    sleep(1);
    //printf("I am main, stack = %d\n", stack);
    return 0;
}

