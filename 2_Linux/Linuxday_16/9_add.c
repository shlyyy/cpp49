#include <49func.h>
#define NUM 10000000
typedef struct shareRes_s {
    int num;
    pthread_mutex_t mutex;
} shareRes_t;
void *threadFunc(void *arg){
    shareRes_t * pShareRes = (shareRes_t *)arg;
    for(int i = 0; i < NUM; ++i){
        pthread_mutex_lock(&pShareRes->mutex);
        ++pShareRes->num;
        pthread_mutex_unlock(&pShareRes->mutex);
    }
    pthread_exit(NULL);
}
int main()
{
    shareRes_t shareRes;
    shareRes.num = 0;
    pthread_mutex_init(&shareRes.mutex,NULL);//初始化一个锁资源，第二参数是NULL表示锁是默认属性
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&shareRes);
    for(int i = 0; i < NUM; ++i){
        pthread_mutex_lock(&shareRes.mutex);
        ++shareRes.num;
        pthread_mutex_unlock(&shareRes.mutex);
    }
    pthread_join(tid,NULL);
    printf("num = %d\n",shareRes.num);
    return 0;
}

