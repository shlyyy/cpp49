#include "head.h"
int makeWorker(int workerNum, workerdata_t * workerdataArr){
    pid_t pid;
    for(int i = 0; i < workerNum; ++i){
        pid = fork();
        if(pid == 0){
            //只有子进程能进入
            //保证 if结构 永远无法离开
            eventLoop();
        }
        //只有父进程可以执行
        workerdataArr[i].pid = pid;
        workerdataArr[i].status = FREE;
        printf("worker %d, pid = %d\n",i, pid);
    }
    return 0;
}
int eventLoop(){
    while(1){
        sleep(1);
    }
}
