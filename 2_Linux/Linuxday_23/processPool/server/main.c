#include "head.h"
int main(int argc, char *argv[])
{
    // ./server 0.0.0.0 1234 3
    ARGS_CHECK(argc,4);
    int workerNum = atoi(argv[3]);
    workerdata_t * workerdataArr = (workerdata_t *)calloc(workerNum,sizeof(workerdata_t));
    makeWorker(workerNum,workerdataArr);
    int sockfd;
    tcpInit(&sockfd,argv[1],argv[2]);
    pause();
    return 0;
}

