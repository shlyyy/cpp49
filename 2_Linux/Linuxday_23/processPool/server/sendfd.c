#include "head.h"
int sendfd(int sockfd ,int fdtosend){
    struct msghdr hdr;
    bzero(&hdr,sizeof(hdr));//不可省略
    // 消息的正文部分
    char buf[] = "hello";
    struct iovec iov[1];
    iov[0].iov_base = buf;
    iov[0].iov_len = 5;
    hdr.msg_iov = iov;
    hdr.msg_iovlen = 1;
    // sendmsg 发送正文
    sendmsg(sockfd,&hdr,0);
}
int recvfd(int sockfd,int *pfdtorecv){
    struct msghdr hdr;
    bzero(&hdr,sizeof(hdr));
    // 消息的正文部分
    char buf[6] = {0};
    struct iovec iov[1];
    iov[0].iov_base = buf;
    iov[0].iov_len = 5;
    hdr.msg_iov = iov;
    hdr.msg_iovlen = 1;
    // recvmsg 接收正文
    recvmsg(sockfd,&hdr,0);
    printf("buf = %s\n", buf);
}
