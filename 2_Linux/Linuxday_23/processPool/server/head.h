#include <49func.h>
enum {
    FREE,
    BUSY
};
typedef struct workerdata_s{//父进程使用，用来保存每个子进程的信息
    pid_t pid;
    int status;
}workerdata_t;
 int makeWorker(int workerNum, workerdata_t * workerdataArr); 
 int eventLoop();
 int tcpInit(int *psockfd, const char *ip, const char *port);
