#include <48func.h>
int main(int argc, char *argv[])
{
    // ./6_cp src dest
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    int fdw = open(argv[2],O_WRONLY|O_TRUNC|O_CREAT,0666);
    ERROR_CHECK(fdw,-1,"open fdw");
    //char buf[4096] = {0}; 
    char buf[4096000] = {0}; 
    // buf选择char数组，不是字符串的含义，而是因为char的字节是1
    while(1){
        memset(buf,0,sizeof(buf));
        ssize_t sret = read(fdr,buf,sizeof(buf));
        ERROR_CHECK(sret,-1,"read");
        // 读取磁盘文件，返回值为0，则读完
        if(sret == 0){
            break;
        }
        // 写入dest
        write(fdw,buf,sret);
    }
    close(fdr);
    close(fdw);
    return 0;
}

