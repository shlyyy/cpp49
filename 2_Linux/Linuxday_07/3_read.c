#include <48func.h>
int main(int argc, char *argv[])
{
    // ./3_read file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    char buf[6] = {0};
    ssize_t sret = read(fd,buf,5);
    ERROR_CHECK(sret,-1,"read");
    printf("buf = %s, sret = %ld\n",buf,sret);
    memset(buf,0,sizeof(buf));//每次read操作之前先清空
    sret = read(fd,buf,5);
    ERROR_CHECK(sret,-1,"read");
    printf("buf = %s, sret = %ld\n",buf,sret);
    close(fd);
    return 0;
}

