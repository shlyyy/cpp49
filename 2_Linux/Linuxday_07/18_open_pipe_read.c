#include <49func.h>
int main(int argc, char *argv[])
{
    // ./18_open_pipe_read 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open");
    printf("read side is opened!\n");
    close(fdr);
    return 0;
}

