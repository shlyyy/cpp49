#include <49func.h>
int main(int argc, char *argv[])
{
    // ./15_redirect file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_WRONLY);
    ERROR_CHECK(fd,-1,"open");
    printf("fd = %d\n", fd);
    printf("You can see me!\n");

    close(STDOUT_FILENO);
    dup(fd);//让1号文件描述符引用磁盘文件的文件对象
    printf("You can't see me!\n");
    close(fd);
    return 0;
}

