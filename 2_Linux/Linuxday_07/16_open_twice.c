#include <49func.h>
int main(int argc, char *argv[])
{
    // ./16_open_twice file1
    ARGS_CHECK(argc,2);
    int fd1 = open(argv[1],O_RDWR);
    ERROR_CHECK(fd1,-1,"open 1");
    int fd2 = open(argv[1],O_RDWR);
    ERROR_CHECK(fd2,-1,"open 2");
    write(fd1,"hello",5);
    write(fd2,"world",5);
    close(fd2);
    close(fd1);
    return 0;
}

