#include <49func.h>
int main(int argc, char *argv[])
{
    // ./18_open_pipe_write 1.pipe
    ARGS_CHECK(argc,2);
    int fdw = open(argv[1],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open");
    printf("write side is opened!\n");
    close(fdw);
    return 0;
}

