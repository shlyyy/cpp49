#include <49func.h>
int main(int argc, char *argv[])
{
    // ./11_fileno file1
    ARGS_CHECK(argc,2);
    FILE * fp = fopen(argv[1],"r+");
    ERROR_CHECK(fp,NULL,"fopen");
    //write(3,"hello",5);
    //write(fp->_fileno,"world",5);
    write(fileno(fp),"hello",5);
    fclose(fp);
    return 0;
}

