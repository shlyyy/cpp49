#include <49func.h>
int main(int argc, char *argv[])
{
    // ./14_dup file1
    ARGS_CHECK(argc,2);
    int oldfd = open(argv[1],O_RDWR);
    ERROR_CHECK(oldfd,-1,"open");
    printf("oldfd = %d\n",oldfd);
    int newfd = dup(oldfd);
    ERROR_CHECK(newfd,-1,"dup");
    printf("newfd = %d\n", newfd);
    // 新旧文件描述符数值不相同
    // 引用的文件对象相同，共享偏移量
    write(oldfd,"hello",5);
    write(newfd,"world",5);
    close(newfd);
    close(oldfd);
    return 0;
}

