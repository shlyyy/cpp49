#include <48func.h>
int main(int argc, char *argv[])
{
    // ./2_write file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    char buf[] = "how are you";
    ssize_t sret = write(fd,buf,strlen(buf));
    //ssize_t sret = write(fd,buf,sizeof(buf));
    //写入文本数据，应该使用strlen而不是sizeof
    ERROR_CHECK(sret,-1,"write");
    printf("sret = %ld\n", sret);
    close(fd);
    return 0;
}

