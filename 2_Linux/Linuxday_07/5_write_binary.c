#include <48func.h>
int main(int argc, char *argv[])
{
    // ./5_write_binary file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    int data = 100000;
    ssize_t sret = write(fd,&data,sizeof(int));
    ERROR_CHECK(sret,-1,"write");
    printf("sret = %ld\n", sret);
    close(fd);
    return 0;
}

