#include <49func.h>
int main(int argc, char *argv[])
{
    // ./8_mmap file1
    ARGS_CHECK(argc,2);
    // 先 open 文件
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    // 建立内存和磁盘之间的映射
    char *p = (char *)mmap(NULL,5,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    ERROR_CHECK(p,MAP_FAILED,"mmap");//mmap失败返回不是NULL
    for(int i = 0; i < 5; ++i){
        printf("%c", *(p+i));
    }
    printf("\n");
    *(p+4) = 'O';
    munmap(p,5);
    close(fd);
    return 0;
}

