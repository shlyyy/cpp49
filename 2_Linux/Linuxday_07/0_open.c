#include <48func.h>
int main(int argc, char *argv[])
{
    // ./0_open file1
    ARGS_CHECK(argc,2);
    // 使用要求，如果flags里面存在O_CREAT，必须选择3参数的open
    // 创建出来的文件权限会受到掩码的影响
    //int fd = open(argv[1],O_WRONLY|O_CREAT|O_EXCL,0666);
    int fd = open(argv[1],O_WRONLY|O_TRUNC);
    ERROR_CHECK(fd,-1,"open");
    printf("fd = %d\n", fd);
    close(fd);
    return 0;
}

