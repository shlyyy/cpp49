#include <49func.h>
int main(int argc, char *argv[])
{
    // ./13_redirect file1
    ARGS_CHECK(argc,2);
    // 在关闭1之前请先打印一个换行符
    printf("You can see me!\n");
    //close(1);
    close(STDOUT_FILENO);
    int fd = open(argv[1],O_WRONLY);
    ERROR_CHECK(fd,-1,"open");
    printf("fd = %d\n", fd);
    printf("You can't see me!\n");
    return 0;
}

