#include <49func.h>
#include <openssl/md5.h>
int main(int argc, char *argv[])
{
    int fd = open("file1",O_RDONLY);
    MD5_CTX ctx;
    MD5_Init(&ctx);
    char buf[4096];
    while(1){
        bzero(buf,sizeof(buf));
        ssize_t sret = read(fd,buf,sizeof(buf));
        if(sret == 0){
            break;
        }
        MD5_Update(&ctx,buf,sret);
    }
    unsigned char md[16];//将要保存md5码16个字节的二进制数据
    MD5_Final(md,&ctx);
    for(int i = 0; i < 16; ++i){
        printf("%02x",md[i]);
    }
    printf("\n");
    return 0;
}

