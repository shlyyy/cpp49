#include <49func.h>
int main(int argc, char *argv[])
{
    // ./0_homework_same file1 file2
    ARGS_CHECK(argc,3);
    int fd1 = open(argv[1],O_RDONLY);
    ERROR_CHECK(fd1,-1,"open fd1");//目前对大家的要求，必须写errorcheck
    int fd2 = open(argv[2],O_RDONLY);
    ERROR_CHECK(fd2,-1,"open fd2");//目前对大家的要求，必须写errorcheck
    //char ch1,ch2;
    //while(1){
    //    ssize_t sret1 = read(fd1,&ch1,1);
    //    ssize_t sret2 = read(fd2,&ch2,1);
    //    if(sret1 == 0 && sret2 == 0){
    //        break;
    //    }
    //    if(sret1 != sret2 || ch1 != ch2){
    //        printf("Not the same!\n");
    //        return 0;
    //    }
    //}
    //printf("The same!\n");
    char buf1[4096];
    char buf2[4096];
    while(1){
        ssize_t sret1 = read(fd1,buf1,4096);
        ssize_t sret2 = read(fd2,buf2,4096);
        if(sret1 == 0 && sret2 == 0){
            break;
        }
        if(sret1 != sret2){
            printf("Not the same!\n");
            return 0;
        }
        else {
            for(int i = 0; i < sret1; ++i){
                if(buf1[i] != buf2[i]){
                    printf("Not the same!\n");
                    return 0;
                }
            }
        }
    }
    printf("The same!\n");
    return 0;
}

