#include <49func.h>
typedef struct student_s {
    int num;
    //char * name; 指针成员一定注意野指针问题
    char name[30];
    float score;
} student_t;
int main(int argc, char *argv[])
{
    // ./2_homework_student file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    student_t s1[3] = {
        {1001,"Caixukun",88.8},
        {1002,"Wuyifan",59.9},
        {1003,"Liyifeng",60}
    };
    write(fd,s1,sizeof(s1));
    lseek(fd,0,SEEK_SET);
    student_t s2[3];
    read(fd,s2,sizeof(s2));
    for(int i = 0; i < 3; ++i){
        printf("num = %d, name = %s, score = %5.2f\n", s2[i].num, s2[i].name, s2[i].score);
    }
    close(fd);
    return 0;
}

