#include <49func.h>
int main(int argc, char *argv[])
{
    // ./7_read 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open");
    char buf[1024] = {0};
    while(1){
        sleep(3);
        read(fdr,buf,sizeof(buf));
    }
    close(fdr);
    return 0;
}

