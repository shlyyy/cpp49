#include <49func.h>
int main(int argc, char *argv[])
{
    // ./1_homework_million file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_WRONLY|O_CREAT|O_TRUNC,0666);
    ERROR_CHECK(fd,-1,"open");
    //for(int i = 0; i < 1000000; ++i){
    //    write(fd,"1",1);
    //}
    char buf[100000];
    for(int i = 0; i < 100000; ++i){
        buf[i] = '1';
    }
    for(int i = 0; i < 10; ++i){
        write(fd,buf,100000);
    }
    close(fd);
    return 0;
}

