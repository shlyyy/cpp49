#include <49func.h>
int main(int argc, char *argv[])
{
    // ./3_homework_mmap file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    struct stat statbuf;
    stat(argv[1],&statbuf);
    printf("size = %ld\n",statbuf.st_size);
    char *p = (char *)mmap(NULL,statbuf.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    ERROR_CHECK(p,MAP_FAILED,"mmap");
    for(int i = 0; i < statbuf.st_size; ++i){
        if(p[i] <= 'z' && p[i] >= 'a'){
            p[i] -= 32;
        }
        else if(p[i] == '"' || p[i] == ',' || p[i] == '.' || p[i] == ';'){
            p[i] = ' ';
        }
    }
    munmap(p,statbuf.st_size);
    close(fd);
    return 0;
}

