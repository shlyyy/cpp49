#include <49func.h>
int main(int argc, char *argv[])
{
    // ./5_azhen 1.pipe 2.pipe
    ARGS_CHECK(argc,3);
    //int fdr = open(argv[1],O_RDONLY);  存在死锁问题
    //ERROR_CHECK(fdr,-1,"open fdr");
    //int fdw = open(argv[2],O_WRONLY);
    //ERROR_CHECK(fdw,-1,"open fdw");
    int fdw = open(argv[1],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open fdw");
    int fdr = open(argv[2],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    printf("chat is established!\n");
    char buf[4096];
    while(1){
        // 读取对面发送的消息
        memset(buf,0,sizeof(buf));
        read(fdr,buf,sizeof(buf));
        printf("buf = %s\n", buf);
        // 阿珍发消息
        memset(buf,0,sizeof(buf));
        read(STDIN_FILENO,buf,sizeof(buf));
        write(fdw,buf,strlen(buf));
    }
    close(fdr);
    close(fdw);
    return 0;
}

