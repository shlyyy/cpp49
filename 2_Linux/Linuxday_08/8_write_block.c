#include <49func.h>
int main(int argc, char *argv[])
{
    // ./8_write_block 1.pipe
    ARGS_CHECK(argc,2);
    int fd1 = open(argv[1],O_RDWR);//fd1 读
    ERROR_CHECK(fd1,-1,"open fd1");
    int fd2 = open(argv[1],O_RDWR);//fd2 写
    ERROR_CHECK(fd2,-1,"open fd2");
    char buf[4096];
    fd_set rdset,wrset;
    int cnt = 0;
    while(1){
        FD_ZERO(&rdset);
        FD_ZERO(&wrset);
        FD_SET(fd1,&rdset);
        FD_SET(fd2,&wrset);
        int ret = select(fd2+1,&rdset,&wrset,NULL,NULL);
        printf("ret = %d\n", ret);
        if(FD_ISSET(fd1,&rdset)){
            read(fd1,buf,2048);
            printf("read ready, cnt = %d\n", cnt++);
        }
        if(FD_ISSET(fd2,&wrset)){
            write(fd2,buf,4096);
            printf("write ready, cnt = %d\n", cnt++);
        }
        //sleep(1);
    }
    return 0;
}

