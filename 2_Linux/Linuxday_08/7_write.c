#include <49func.h>
int main(int argc, char *argv[])
{
    // ./7_write 1.pipe
    ARGS_CHECK(argc,2);
    int fdw = open(argv[1],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open");
    char buf[4096] = {0};
    ssize_t total = 0;
    while(1){
        ssize_t sret = write(fdw,buf,sizeof(buf));
        ERROR_CHECK(sret,-1,"write");
        total += sret;
        printf("sret = %ld, total = %ld\n", sret, total);
    }
    return 0;
}

