#include <49func.h>
int main(int argc, char *argv[])
{
    // ./4_write_pipe 1.pipe
    ARGS_CHECK(argc,2);
    int fdw = open(argv[1], O_WRONLY);
    ERROR_CHECK(fdw,-1,"open");
    printf("write side is opened!\n");
    //sleep(20);
    //printf("sleep over!\n");
    sleep(1);
    write(fdw,"hello",5);
    sleep(20);
    printf("byebye!\n");
    close(fdw);
    return 0;
}

