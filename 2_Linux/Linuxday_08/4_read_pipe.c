#include <49func.h>
int main(int argc, char *argv[])
{
    // ./4_read_pipe 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1], O_RDONLY);
    ERROR_CHECK(fdr,-1,"open");
    printf("read side is opened!\n");
    char buf[4096] = {0};
    printf("sleep over!\n");
    //ssize_t sret = read(fdr,buf,sizeof(buf));
    //ERROR_CHECK(sret,-1,"read");
    //printf("sret = %ld,buf=%s\n", sret, buf);
    close(fdr);
    return 0;
}

