#include <49func.h>
void sigFunc(int signum){
    printf("before signum = %d\n", signum);
    sleep(5);
    // 检查一下是否存在未决的2号信号
    sigset_t pending;
    sigpending(&pending);
    if(sigismember(&pending,SIGQUIT)){
        printf("SIGQUIT is pending!\n");
    }
    else{
        printf("SIGQUIT is not pending!\n");
    }
    printf("after signum = %d\n", signum);
}
int main()
{
    signal(SIGINT,sigFunc);
    signal(SIGQUIT,sigFunc);
    while(1){
        sleep(1);
    }
    return 0;
}

