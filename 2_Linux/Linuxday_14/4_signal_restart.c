#include <49func.h>
void sigFunc(int signum){
    printf("signum = %d\n", signum);
}
int main()
{
    signal(SIGINT,sigFunc);
    char buf[4096] = {0};
    read(STDIN_FILENO,buf,sizeof(buf));
    printf("buf = %s\n", buf);
    return 0;
}

