#include <49func.h>
int main()
{
    sigset_t set,oldset;
    sigfillset(&set);//set包含所有的信号
    sigprocmask(SIG_BLOCK,&set,&oldset);//把所有信号加入屏蔽，旧的mask存入oldset中
    printf("block all!\n");
    sleep(10);
    printf("unblock all!\n");
    sigprocmask(SIG_SETMASK,&oldset,NULL);
    while(1){
        sleep(1);
    }
    return 0;
}

