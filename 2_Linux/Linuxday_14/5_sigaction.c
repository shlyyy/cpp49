#include <49func.h>
void sigFunc(int signum){
    printf("before signum = %d\n", signum);
    sleep(5);
    printf("after signum = %d\n", signum);
}
void sigFunc3(int signum, siginfo_t *info, void *p){//设计一个3参数的回调函数
    printf("signum = %d\n", signum);
    printf("pid = %d, uid = %d\n", info->si_pid, info->si_uid);
}
int main()
{
    struct sigaction act;
    memset(&act,0,sizeof(act));
    //act.sa_handler = sigFunc;
    act.sa_sigaction = sigFunc3;//使用sa_sigaction字段
    //act.sa_flags = SA_RESTART;
    //act.sa_flags = SA_RESTART|SA_RESETHAND; 
    //act.sa_flags = SA_RESTART|SA_NODEFER; 
    act.sa_flags = SA_RESTART|SA_SIGINFO;//使用3参数版本 SA_SIGINFO
    sigaction(SIGINT,&act,NULL);
    sigaction(SIGQUIT,&act,NULL);
    //while(1);
    char buf[4096] = {0};
    read(STDIN_FILENO,buf,sizeof(buf));
    printf("buf = %s\n", buf);
    return 0;
}

