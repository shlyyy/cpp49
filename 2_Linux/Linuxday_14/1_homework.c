#include <49func.h>
int main()
{
    int fds[2];
    pipe(fds);
    if(fork() == 0){
        close(fds[1]);
        char buf[1024] = {0};
        //ssize_t sret = read(fds[0],buf,sizeof(buf));
        //printf("sret = %ld\n", sret);
        //while(1){
        //    sleep(1);
        //    read(fds[0],buf,sizeof(buf));
        //    printf("read\n");
        //}
        sleep(10);
        printf("sleep over!\n");
        close(fds[0]);
    }
    else{
        close(fds[0]);
        //sleep(10);
        //printf("sleep over!\n");
        //close(fds[1]);
        char buf[4096] = {0};
        int cnt = 0;
        while(1){
            write(fds[1],buf,sizeof(buf));
            printf("write, cnt = %d\n", cnt++);
        }
        wait(NULL);
    }
    return 0;
}

