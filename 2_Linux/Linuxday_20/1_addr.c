#include <49func.h>
int main(int argc, char *argv[]){
    // ./1_addr 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;//ipv4
    addr.sin_port = htons(atoi(argv[2]));
    // argv[2] char * --> short 再 从小端h 到 大端n
    // inet_aton
    // inet_aton(argv[1],&addr.sin_addr); 
    // inet_addr
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    printf("port = %x, ip = %x\n", addr.sin_port, addr.sin_addr.s_addr);
}
