#include <49func.h>
// 版本1
//int recvFile(int sockfd){
//    char filename[1024] = {0};
//    recv(sockfd,filename,sizeof(filename),0);
//    int fd = open(filename,O_WRONLY|O_TRUNC|O_CREAT,0666);
//    char buf[4096] = {0};
//    ssize_t sret = recv(sockfd,buf,sizeof(buf),0);
//    write(fd,buf,sret);
//    return 0;
//}
typedef struct train_s {
    int length;
    char data[1000];                                                      
} train_t;
// 版本2
//int recvFile(int sockfd){
//    train_t train;
//    bzero(&train,sizeof(train));
//    recv(sockfd,&train.length, sizeof(train.length),0);
//    recv(sockfd,train.data,train.length,0);
//    int fd = open(train.data,O_WRONLY|O_TRUNC|O_CREAT,0666);
//    bzero(&train,sizeof(train));
//    recv(sockfd,&train.length, sizeof(train.length),0);
//    recv(sockfd,train.data,train.length,0);
//    write(fd,train.data,train.length);
//    return 0;
//}
// 版本3
//int recvFile(int sockfd){
//    train_t train;
//    bzero(&train,sizeof(train));
//    recv(sockfd,&train.length, sizeof(train.length),MSG_WAITALL);
//    recv(sockfd,train.data,train.length,MSG_WAITALL);
//    int fd = open(train.data,O_WRONLY|O_TRUNC|O_CREAT,0666);
//    while(1){
//        bzero(&train,sizeof(train));
//        recv(sockfd,&train.length, sizeof(train.length),MSG_WAITALL);
//        if(train.length != 1000){
//            printf("train.length = %d\n", train.length);
//        }
//        if(train.length == 0){
//            break;
//        }
//        recv(sockfd,train.data,train.length,MSG_WAITALL);
//        write(fd,train.data,train.length);
//    }
//    return 0;
//}
// 版本3.1
//int recvn(int sockfd,void *buf, int length){
//    int total = 0;
//    char *p = (char *)buf;
//    while(total < length){
//        ssize_t sret = recv(sockfd,p+total,length-total,0);
//        total += sret;
//    }
//    return 0;
//}
//int recvFile(int sockfd){
//    train_t train;
//    bzero(&train,sizeof(train));
////    recv(sockfd,&train.length, sizeof(train.length),MSG_WAITALL);
////    recv(sockfd,train.data,train.length,MSG_WAITALL);
//    recvn(sockfd,&train.length,sizeof(train.length));
//    recvn(sockfd,train.data,train.length);
//    int fd = open(train.data,O_WRONLY|O_TRUNC|O_CREAT,0666);
//    while(1){
//        bzero(&train,sizeof(train));
////        recv(sockfd,&train.length, sizeof(train.length),MSG_WAITALL);
//        recvn(sockfd,&train.length,sizeof(train.length));
//        if(train.length != 1000){
//            printf("train.length = %d\n", train.length);
//        }
//        if(train.length == 0){
//            break;
//        }
////        recv(sockfd,train.data,train.length,MSG_WAITALL);
//        recvn(sockfd,train.data,train.length);
//        write(fd,train.data,train.length);
//    }
//    return 0;
//}
// 版本4 & 版本5
int recvn(int sockfd,void *buf, int length){
    int total = 0;
    char *p = (char *)buf;
    while(total < length){
        ssize_t sret = recv(sockfd,p+total,length-total,0);
        total += sret;
    }
    return 0;
}
//int recvFile(int sockfd){
//    train_t train;
//    bzero(&train,sizeof(train));
//    recvn(sockfd,&train.length,sizeof(train.length));
//    recvn(sockfd,train.data,train.length);
//    int fd = open(train.data,O_WRONLY|O_TRUNC|O_CREAT,0666);
//    off_t filesize;
//    recvn(sockfd,&train.length,sizeof(train.length));
//    recvn(sockfd,train.data,train.length);
//    memcpy(&filesize,train.data,sizeof(off_t));
//    printf("filesize = %ld\n", filesize);
//    off_t cursize = 0;
//    off_t lastsize = 0;
//    while(1){
//        bzero(&train,sizeof(train));
//        recvn(sockfd,&train.length,sizeof(train.length));
//        if(train.length != 1000){
//            printf("train.length = %d\n", train.length);
//        }
//        if(train.length == 0){
//            break;
//        }
//        recvn(sockfd,train.data,train.length);
//        write(fd,train.data,train.length);
//        cursize += train.length;
//        if(cursize - lastsize > filesize/1000){
//            printf("%5.2lf%%\r",100.0*cursize/filesize);
//            fflush(stdout);
//            lastsize = cursize;    
//        }
//    }
//    printf("100.00%%\n");
//    return 0;
//}
int recvFile(int sockfd){
    train_t train;
    bzero(&train,sizeof(train));
    recvn(sockfd,&train.length,sizeof(train.length));
    recvn(sockfd,train.data,train.length);
    int fd = open(train.data,O_RDWR|O_TRUNC|O_CREAT,0666);
    off_t filesize;
    recvn(sockfd,&train.length,sizeof(train.length));
    recvn(sockfd,train.data,train.length);
    memcpy(&filesize,train.data,sizeof(off_t));
    printf("filesize = %ld\n", filesize);
    ftruncate(fd,filesize);
    char *p = (char *)mmap(NULL,filesize,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    ERROR_CHECK(p,MAP_FAILED,"mmap");
    recvn(sockfd,p,filesize);
    munmap(p,filesize);
    return 0;
}
int main(int argc, char *argv[])
{
    // ./client 192.168.118.128 1234 
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in addr;//服务端地址
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    int ret = connect(sockfd,(struct sockaddr *)&addr,sizeof(addr));
    ERROR_CHECK(ret,-1,"connect");
    recvFile(sockfd);
    close(sockfd);
    return 0;
}

