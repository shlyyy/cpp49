#include "head.h"
// 发送一个小文件 版本1 问题是粘包
//int transFile(int netfd){
//    send(netfd,"file1",5,0);
//    int fd = open("file1",O_RDONLY);
//    char buf[4096] = {0};
//    ssize_t sret = read(fd,buf,sizeof(buf));
//    send(netfd,buf,sret,0);
//    return 0;
//}
// 发送一个小文件 版本2 使用小火车协议
typedef struct train_s {
    int length;
    char data[1000];
} train_t;
//int transFile(int netfd){
//    train_t train = {5,"file1"};
//    send(netfd,&train,sizeof(train.length)+train.length,0);
//    int fd = open("file1",O_RDONLY);
//    ssize_t sret = read(fd,train.data,sizeof(train.data));
//    train.length = sret;
//    send(netfd,&train,sizeof(train.length)+train.length,0);
//    return 0;
//}
// 版本3 发送一个大文件 使用小火车协议
//int transFile(int netfd){
//    train_t train = {5,"file1"};
//    send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
//    int fd = open("file1",O_RDONLY);
//    while(1){
//        ssize_t sret = read(fd,train.data,sizeof(train.data));
//        if(sret == 0){
//            break;
//        }
//        train.length = sret;
//        sret = send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
//        if(train.length != 1000){
//            printf("train.length = %d\n", train.length);
//        }
//    }
//    train.length = 0;
//    send(netfd,&train,sizeof(train.length),MSG_NOSIGNAL);
//    return 0;
//}
// 版本4 发送一个大文件 使用小火车协议 先发送文件的长度
//int transFile(int netfd){
//    train_t train = {5,"file1"};
//    send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
//    int fd = open("file1",O_RDONLY);
//    struct stat statbuf;
//    fstat(fd,&statbuf);
//    printf("filesize = %ld\n", statbuf.st_size);
//    train.length = sizeof(statbuf.st_size);
//    memcpy(train.data,&statbuf.st_size,train.length);
//    send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
//    while(1){
//        ssize_t sret = read(fd,train.data,sizeof(train.data));
//        if(sret == 0){
//            break;
//        }
//        train.length = sret;
//        sret = send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
//        if(train.length != 1000){
//            printf("train.length = %d\n", train.length);
//        }
//    }
//    train.length = 0;
//    send(netfd,&train,sizeof(train.length),MSG_NOSIGNAL);
//    return 0;
//}
// 版本5 发送一个大文件 使用小火车协议 先发送文件的长度 使用mmap
//int transFile(int netfd){
//    train_t train = {5,"file1"};
//    send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
//    int fd = open("file1",O_RDWR);
//    struct stat statbuf;
//    fstat(fd,&statbuf);
//    printf("filesize = %ld\n", statbuf.st_size);
//    train.length = sizeof(statbuf.st_size);
//    memcpy(train.data,&statbuf.st_size,train.length);
//    send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
//    // mmap 
//    char *p = (char *)mmap(NULL,statbuf.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
//    ERROR_CHECK(p,MAP_FAILED,"mmap");
//    // send
//    off_t totalsize = 0;
//    while(totalsize < statbuf.st_size){
//        if(statbuf.st_size - totalsize > (off_t)sizeof(train.data)){
//            train.length = 1000;
//        }
//        else{
//            train.length = statbuf.st_size - totalsize;
//        }
//        send(netfd,&train.length,sizeof(train.length),MSG_NOSIGNAL);
//        send(netfd,p+totalsize,train.length,MSG_NOSIGNAL);
//        totalsize += train.length;
//    }
//    train.length = 0;
//    send(netfd,&train.length,sizeof(train.length),MSG_NOSIGNAL);
//    munmap(p,statbuf.st_size);
//    return 0;
//}
// 版本6 发送一个大文件 使用大火车发送文件内容 先发送文件的长度 使用mmap
int transFile(int netfd){
    train_t train = {5,"file1"};
    send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
    int fd = open("file1",O_RDWR);
    struct stat statbuf;
    fstat(fd,&statbuf);
    printf("filesize = %ld\n", statbuf.st_size);
    train.length = sizeof(statbuf.st_size);
    memcpy(train.data,&statbuf.st_size,train.length);
    send(netfd,&train,sizeof(train.length)+train.length,MSG_NOSIGNAL);
   // // mmap 
   // char *p = (char *)mmap(NULL,statbuf.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
   // ERROR_CHECK(p,MAP_FAILED,"mmap");
   // // send
   // send(netfd,p,statbuf.st_size,MSG_NOSIGNAL);
   // munmap(p,statbuf.st_size);
    sendfile(netfd,fd,NULL,statbuf.st_size);
    sleep(20);
    printf("sleep over!\n");
    return 0;
}
