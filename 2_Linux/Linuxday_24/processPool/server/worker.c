#include "head.h"
int makeWorker(int workerNum, workerdata_t * workerdataArr){
    pid_t pid;
    int fds[2];
    for(int i = 0; i < workerNum; ++i){
        socketpair(AF_LOCAL,SOCK_STREAM,0,fds); //先socketpair 再fork
        pid = fork();
        if(pid == 0){
            //只有子进程能进入
            //保证 if结构 永远无法离开
            close(fds[0]);
            eventLoop(fds[1]);
        }
        close(fds[1]);
        //只有父进程可以执行
        workerdataArr[i].pid = pid;
        workerdataArr[i].status = FREE;
        workerdataArr[i].socketfd = fds[0];
        printf("worker %d, pid = %d, socketfd = %d\n",i, pid, fds[0]);
    }
    return 0;
}
int eventLoop(int sockfd){
    while(1){
        // 接收任务
        int netfd;
        int exitflag;
        recvfd(sockfd,&netfd,&exitflag);
        if(exitflag == 1){
            printf("child is going to exit!\n");
            exit(0);
        }
        //写业务代码 通过netfd和客户端通信
        printf("child begins working!\n");
        transFile(netfd);
        close(netfd);
        printf("child finishes his job!\n");
        pid_t pid = getpid();
        write(sockfd,&pid,sizeof(pid));
    }
}
