#include <49func.h>
int main(int argc, char *argv[])
{
    if(fork() == 0){
        //return -1;
        while(1);
    }
    else{
        int wstatus;
        //wait(&wstatus);
        while(1){
            int ret = waitpid(-1,&wstatus,WNOHANG);
            if(ret != 0){
                if(WIFEXITED(wstatus)){
                    printf("exit normally! exit code = %d\n", WEXITSTATUS(wstatus));
                }
                else if(WIFSIGNALED(wstatus)){
                    printf("exit abnormally! terminal signal = %d\n", WTERMSIG(wstatus));
                }
                break;
            }
            else{
                printf("No child process has dead yet!\n");
                sleep(3);
            }
        }
    }
    return 0;
}

