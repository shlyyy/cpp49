#include <49func.h>
int main()
{
    printf("Hello\n");
    pid_t pid;
    pid = fork();
    if(pid != 0){
        //父进程
        printf("I am parent, pid = %d, ppid = %d\n",getpid(),getppid());
        sleep(1);
    }
    else{
        //子进程
        printf("I am child, pid = %d,ppid = %d\n", getpid(),getppid());
    }
    return 0;
}

