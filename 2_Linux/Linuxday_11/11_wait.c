#include <49func.h>
int main(int argc, char *argv[])
{
    if(fork() == 0){
        //return -1;
        while(1);
    }
    else{
        int wstatus;
        wait(&wstatus);
        if(WIFEXITED(wstatus)){
            printf("exit normally! exit code = %d\n", WEXITSTATUS(wstatus));
        }
        else if(WIFSIGNALED(wstatus)){
            printf("exit abnormally! terminal signal = %d\n", WTERMSIG(wstatus));
        }
    }
    return 0;
}

