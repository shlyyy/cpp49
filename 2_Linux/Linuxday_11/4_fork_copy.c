#include <49func.h>
int global = 1;
int main()
{
    int stack = 2;
    int *pHeap = (int *)malloc(sizeof(int));
    *pHeap = 3;
    if(fork() == 0){
        //子进程
        global += 3;
        stack += 3;
        *pHeap += 3;
        printf("I am child, global = %d, stack = %d, heap = %d\n",
              global,stack,*pHeap);
    }
    else{
        //父进程
        sleep(2);
        printf("I am parent, global = %d, stack = %d, heap = %d\n",
              global,stack,*pHeap);
    }

    return 0;
}

