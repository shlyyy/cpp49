#include <49func.h>
int main()
{
    printf("uid = %d, euid = %d\n", getuid(), geteuid());
    int fd = open("file1",O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    ssize_t sret = write(fd,"hello",5);
    ERROR_CHECK(sret,-1,"write");
    close(fd);
    return 0;
}

