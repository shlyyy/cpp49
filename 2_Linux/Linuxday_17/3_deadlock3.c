#include <49func.h>
int main()
{
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex,NULL);
    pthread_mutex_lock(&mutex);
    printf("lock once!\n");
    pthread_mutex_lock(&mutex);
    printf("lock twice!\n");
    pthread_mutex_unlock(&mutex);
    pthread_mutex_unlock(&mutex);
    return 0;
}

