#include <49func.h>
void *threadFunc(void *arg){
    pthread_mutex_t * pmutex = (pthread_mutex_t *)arg;
    sleep(1);
    pthread_mutex_lock(pmutex);
    printf("child thread!\n");
    pthread_mutex_unlock(pmutex);
}
int main()
{
    pthread_mutexattr_t mutexattr;
    pthread_mutexattr_init(&mutexattr);//初始化锁的属性
    pthread_mutexattr_settype(&mutexattr,PTHREAD_MUTEX_RECURSIVE);//设置锁的属性
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex,&mutexattr);//用锁的属性初始化锁
    
    // 
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&mutex);

    // 主线程先后对同一个锁加锁两次
    int ret = pthread_mutex_lock(&mutex);
    THREAD_ERROR_CHECK(ret,"mutex_lock 1");
    printf("lock once!\n");
    ret = pthread_mutex_lock(&mutex);
    THREAD_ERROR_CHECK(ret,"mutex_lock 2");
    printf("lock twice!\n");
    sleep(1);
    printf("sleep over 1\n");
    pthread_mutex_unlock(&mutex);
    sleep(1);
    printf("sleep over 2\n");
    pthread_mutex_unlock(&mutex);

    pthread_join(tid,NULL);
    return 0;
}

