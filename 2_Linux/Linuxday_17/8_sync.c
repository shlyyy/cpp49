#include <49func.h>
typedef struct shareRes_s {
    int flag; // 0-->B不能运行 1-->B可以运行
    pthread_mutex_t mutex; // 保护flag的锁
    pthread_cond_t cond; // 条件变量 提供一种无竞争的同步机制
} shareRes_t;
void *threadFunc(void *arg){
    shareRes_t * pShareRes = (shareRes_t *)arg;
    pthread_mutex_lock(&pShareRes->mutex);
    if(pShareRes->flag != 1){//B不能执行的话
        pthread_cond_wait(&pShareRes->cond,&pShareRes->mutex);
    }
    pthread_mutex_unlock(&pShareRes->mutex);
    printf("B begin!\n");
    sleep(3);
    printf("B end!\n");
    pthread_exit(NULL);
}
int main(){
    shareRes_t shareRes;
    shareRes.flag = 0;
    pthread_mutex_init(&shareRes.mutex,NULL);
    pthread_cond_init(&shareRes.cond,NULL);
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&shareRes);
    //sleep(5);
    printf("A begin!\n");
    sleep(3);
    printf("A end!\n");
    pthread_mutex_lock(&shareRes.mutex);
    shareRes.flag = 1;
    pthread_cond_signal(&shareRes.cond);
    pthread_mutex_unlock(&shareRes.mutex);
    pthread_join(tid,NULL);
    return 0;
}

