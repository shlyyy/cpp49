#include <49func.h>
int main(int argc, char *argv[])
{
    if(fork() == 0){
        printf("Child, pid = %d, pgid = %d\n", getpid(), getpgid(0));
        setpgid(0,0);
        printf("Child, pid = %d, pgid = %d\n", getpid(), getpgid(0));
    }
    else{
        printf("Parent, pid = %d, pgid = %d\n", getpid(), getpgid(0));
        wait(NULL);
    }
    return 0;
}

