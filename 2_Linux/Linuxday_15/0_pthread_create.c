#include <49func.h>
void *threadFunc(void *arg){
    printf("I am child thread\n");
}
int main()
{
    pthread_t tid;//将要用来保存子线程的tid
    pthread_create(&tid,NULL,threadFunc,NULL);
    //创建一个子线程，线程id填入tid，线程属性是默认属性，线程启动函数是threadFunc，传递的参数是NULL
    printf("I am main thread\n");
    usleep(100);
    return 0;
}

