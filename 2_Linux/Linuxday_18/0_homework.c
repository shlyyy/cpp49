#include <49func.h>
typedef struct shareRes_s {
    int flag; // 0-->A 1-->B 2-->C
    pthread_mutex_t mutex;
    pthread_cond_t cond;
}shareRes_t;
void *threadFunc(void *arg){
    shareRes_t * pShareRes = (shareRes_t *)arg;
    sleep(2);
    pthread_mutex_lock(&pShareRes->mutex);
    while(pShareRes->flag == 0){
        pthread_cond_wait(&pShareRes->cond,&pShareRes->mutex);
    }
    printf("B\n");
    pShareRes->flag = 2;
    pthread_cond_signal(&pShareRes->cond);
    pthread_mutex_unlock(&pShareRes->mutex);
    pthread_exit(NULL);
}
int main()
{
    shareRes_t shareRes;
    shareRes.flag = 0;
    pthread_mutex_init(&shareRes.mutex,NULL);
    pthread_cond_init(&shareRes.cond,NULL);
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&shareRes);
    
    printf("A\n");
    pthread_mutex_lock(&shareRes.mutex);
    shareRes.flag = 1;
    pthread_cond_signal(&shareRes.cond);
    sleep(2);
    while(shareRes.flag == 1){
        pthread_cond_wait(&shareRes.cond,&shareRes.mutex);
    }
    printf("C\n");
    pthread_mutex_unlock(&shareRes.mutex);
    pthread_join(tid,NULL);
    return 0;
}

