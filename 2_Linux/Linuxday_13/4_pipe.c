#include <49func.h>
int main()
{
    int fds[2];
    // pipe的参数一定是一个数组名 pipe(fds[2])
    pipe(fds);
    printf("fds[0] = %d, fds[1] = %d\n",fds[0], fds[1]);
    // [0] --> 读端
    // [1] --> 写端
    write(fds[1],"hello",5);
    char buf[1024] = {0};
    read(fds[0],buf,sizeof(buf));
    printf("buf = %s\n", buf);
    return 0;
}

