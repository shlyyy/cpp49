#include <49func.h>
void sigFunc(int signum){
    printf("before, signum = %d\n", signum);
    sleep(5);
    printf("after , signum = %d\n", signum);
}
int main()
{
    void (*p)(int);
    p = signal(SIGINT,sigFunc);
    ERROR_CHECK(p,SIG_ERR,"singal");
    while(1){
        sleep(1);
    }
    return 0;
}

