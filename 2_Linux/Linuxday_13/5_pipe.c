#include <49func.h>
int main()
{
    int fds[2];
    // pipe的参数一定是一个数组名 pipe(fds[2])
    pipe(fds);
    printf("fds[0] = %d, fds[1] = %d\n",fds[0], fds[1]);
    if(fork() == 0){
        //父向子发送数据
        // 子关闭写端
        close(fds[1]);
        char buf[1024] = {0};
        read(fds[0],buf,sizeof(buf));
        printf("buf = %s\n",buf);
        close(fds[0]);
        exit(0);
    }
    else{
        // 父关闭读端
        close(fds[0]);
        sleep(10);
        write(fds[1],"hello",5);
        wait(NULL);
        close(fds[1]);
        exit(0);
    }
    return 0;
}

