#include <49func.h>
int main(){
    if(fork() == 0){
        printf("Hello\n");
    }
    else{
        printf("World!\n");
        wait(NULL);
    }
}
