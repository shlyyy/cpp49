#include <49func.h>
int main(int argc, char *argv[])
{
    // ./2_homework_transB 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);//读管道
    ERROR_CHECK(fdr,-1,"open fdr");
    mkdir("./storage",0777);
    char buf[4096] = {0};
    int length;
    // 收文件名
    read(fdr,&length,sizeof(int));
    read(fdr,buf,length);
    char filepath[8192] = {0};
    sprintf(filepath,"%s/%s","./storage",buf);
    // 新建文件
    int fdw = open(filepath,O_WRONLY|O_CREAT, 0666);
    ERROR_CHECK(fdw,-1,"open fdw");
    // 收文件的内容
    memset(buf,0,sizeof(buf));
    read(fdr,&length,sizeof(int));
    ssize_t sret = read(fdr,buf,length);
    printf("sret = %ld\n", sret);
    write(fdw,buf,sret);
    close(fdw);
    close(fdr);
    return 0;
}

