#include <49func.h>
int main(int argc, char *argv[])
{
    // ./2_homework_transA file1 1.pipe
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr"); //读磁盘文件
    int fdw = open(argv[2],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open fdw"); //写管道
    // 发文件名
    int length = strlen(argv[1]);
    write(fdw,&length,sizeof(int));
    write(fdw,argv[1],length);
    // 发文件的内容
    char buf[4096] = {0};
    ssize_t sret = read(fdr,buf,sizeof(buf));
    length = sret;
    write(fdw,&length,sizeof(int));
    write(fdw,buf,sret);
    close(fdw);
    close(fdr);
    return 0;
}

