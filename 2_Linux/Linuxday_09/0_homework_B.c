#include <49func.h>
int main(int argc, char *argv[])
{
    // ./0_homework_B 1.pipe
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDONLY);
    ERROR_CHECK(fd,-1,"open");
    char buf[4096] = {0};
    read(fd,buf,sizeof(buf));
    printf("buf = %s\n",buf);
    close(fd);
    return 0;
}

