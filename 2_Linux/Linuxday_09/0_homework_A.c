#include <49func.h>
int main(int argc, char *argv[])
{
    // ./0_homework_A 1.pipe
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_WRONLY);
    ERROR_CHECK(fd,-1,"open");
    printf("1 helloworld!\n");

    int newfd = 10;
    dup2(STDOUT_FILENO,newfd);
    dup2(fd,STDOUT_FILENO);
    printf("2 helloworld!\n");
    
    dup2(newfd,STDOUT_FILENO);
    printf("3 helloworld!\n");
    close(fd);
    return 0;
}

