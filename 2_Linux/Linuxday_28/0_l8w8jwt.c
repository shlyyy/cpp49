#include <49func.h>
#include "l8w8jwt/encode.h"
int main(void)
{
    char* jwt;
    size_t jwt_length;
    struct l8w8jwt_encoding_params params;
    l8w8jwt_encoding_params_init(&params);
    params.alg = L8W8JWT_ALG_HS512;
    // 不变的
    params.sub = "Gordon Freeman";
    params.iss = "Black Mesa";
    params.aud = "Administrator";
    params.iat = 0;
    params.exp = 0x7fffffff;
    // 用户不同 secret_key
    params.secret_key = (unsigned char*)"Lisi";
    params.secret_key_length = strlen(params.secret_key);
    params.out = &jwt;
    params.out_length = &jwt_length;
    int r = l8w8jwt_encode(&params);
    printf("\n l8w8jwt example HS512 token: %s \n", r == L8W8JWT_SUCCESS ? jwt : " (encoding failure) ");
    /* Always free the output jwt string! */
    l8w8jwt_free(jwt);
    return 0;
}
