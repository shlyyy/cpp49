
#include "unixhead.h"

#include <iostream>

using std::cout;
using std::endl;

#include <workflow/WFFacilities.h>

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig number %d is comming\n", num);
    waitGroup.done();
}

struct SeriesContext
{
    int fd;
    char * buff;
    size_t sz;
};

void seriesCallback(const SeriesWork * series)
{
    SeriesContext * context = (SeriesContext*)series->get_context();
    if(context) {
        delete [] context->buff;
        delete context;
    }
}

void fileIOCallback(WFFileIOTask * iotask)
{
    //错误的检测
    //
    //
    //获取读取到的数据
    cout << "fileIOCallback is running" << endl;
    SeriesContext * context = (SeriesContext*)series_of(iotask)->get_context();
    context->sz = iotask->get_retval();
    cout << "context->sz:" << context->sz << endl;

    std::string content(context->buff, context->sz);
    cout << "content:" << content << endl;

    close(context->fd);
}

void test0()
{
    signal(SIGINT, sighandler);
    int fd = open("postform.html", O_RDONLY);
    if(fd < 0) {
        perror("open");
        return;
    }

    //fstat(fd,)

    char * buff = new char[1024]();
    SeriesContext * context = new SeriesContext();
    context->fd = fd;
    context->buff = buff;
    context->sz = 0;

    auto fileIOTask = WFTaskFactory::create_pread_task(fd, buff, 1024, 0, fileIOCallback);
    auto series = Workflow::create_series_work(fileIOTask, seriesCallback);
    series->set_context(context);
    series->start();

    waitGroup.wait();
}


int main()
{
    test0();
    return 0;
}

