
#include <signal.h>

#include <iostream>

using std::cout;
using std::endl;
using std::string;

#include <workflow/WFFacilities.h>
#include <workflow/WFHttpServer.h>
#include <workflow/HttpUtil.h>
#include <workflow/RedisMessage.h>

static WFFacilities::WaitGroup waitGroup(1);

struct SeriesContext
{
    string _password;
    protocol::HttpResponse * _resp;
};

void sighandler(int num)
{
    printf("sig number %d is comming\n", num);
    waitGroup.done();
}

void seriesCallback(const SeriesWork * series)
{
    cout << "seriesCallback is running" << endl;
    SeriesContext * context = (SeriesContext*)series->get_context();
    if(context) {
        delete context;
        context = nullptr;
    }
}

void httpCallback(WFHttpTask * serverTask)
{
    cout << "httpCallback is running" << endl;
    //生成响应信息
    auto resp = serverTask->get_resp();
    resp->set_http_version("HTTP/1.1");
    resp->set_status_code("200");
    resp->set_reason_phrase("OK");

    /* resp->add_header_pair("Server", "Workflow http server"); */
    /* resp->add_header_pair("Content-Type", "text/plain"); */
}

void redisCallback(WFRedisTask *);

void process(WFHttpTask * serverTask)
{
    cout << "process is running" << endl;
    //0.1设置serverTask的回调函数
    serverTask->set_callback(httpCallback);
    //0.2设置序列的回调函数
    series_of(serverTask)->set_callback(seriesCallback);

    //http://192.168.30.128:8888/login?name=liubei&password=123
    //1. 解析请求
    auto req = serverTask->get_req();
    string uri = req->get_request_uri();
    cout << "uri:" << uri << endl;
    cout << "method:" << req->get_method() << endl;
    string path = uri.substr(0, uri.find("?"));
    string nameKV = uri.substr(0, uri.find("&"));
    string passwdKV = uri.substr(uri.find("&") + 1);
    string name = nameKV.substr(nameKV.find("=") + 1);
    string passwd = passwdKV.substr(passwdKV.find("=") + 1);
    cout << "path:" << path << endl;
    cout << "name:" << name << endl;
    cout << "passwd:" << passwd << endl;

    //设置共享数据
    SeriesContext * context = new SeriesContext;
    context->_password = passwd;
    context->_resp = serverTask->get_resp();
    series_of(serverTask)->set_context(context);

    //2. 进行登录的验证
    if(path == "/login" && req->get_method() == string("GET")) {
        //创建Redis任务
        cout << "111 " << endl;
        auto redisTask = WFTaskFactory::create_redis_task(
                            "redis://127.0.0.1:6379", 1, redisCallback);
        string command("HGET");
        std::vector<string> params{"student", name};
        redisTask->get_req()->set_request(command, params);
        series_of(serverTask)->push_back(redisTask);
    } else {
        cout << "222" << endl;
    }
}

void test0()
{
    signal(SIGINT, sighandler);
    WFHttpServer server(process);

    if(server.start(8888) == 0) {
        waitGroup.wait();
        server.stop();//停止服务器
    } else {
        cout << "server start failed" << endl;
    }
}


int main()
{
    test0();
    return 0;
}

void redisCallback(WFRedisTask * redisTask)
{
    //1. 错误的检测
    //
    //2. 进行验证,并生成响应信息
    SeriesContext * context = (SeriesContext *)series_of(redisTask)->get_context();

    protocol::RedisValue result;
    redisTask->get_resp()->get_result(result);
    if(result.is_string()) {
        cout << "value is string: " << result.string_value() << endl;
        if(result.string_value() == context->_password) {
            //登录验证成功
            context->_resp->append_output_body("Login Success!");
        } else {
            //登录验证失败
            context->_resp->append_output_body("Login Failded!");
        }
    } else {
        //登录验证失败
        context->_resp->append_output_body("Login Failded!");
    }
    context->_resp->add_header_pair("Server", "Workflow http server");
    context->_resp->add_header_pair("Content-Type", "text/plain");
    context->_resp->set_status_code("201");
}
