#include "unixHeader.h"
#include <iostream>

#include <workflow/WFFacilities.h>
#include <wfrest/HttpServer.h>


using namespace std;
using namespace wfrest;

static WFFacilities::WaitGroup waitGroup(1);

void test0()
{
    HttpServer server;

    if(server.track().start(8888) == 0) {
        server.list_routes();
        waitGroup.wait();
        server.stop();
    } else {
        printf("server cannnot start\n");
    }
}


int main()
{
    test0();
    return 0;
}

