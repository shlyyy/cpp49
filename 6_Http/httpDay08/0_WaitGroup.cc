
#include <signal.h>

#include <iostream>

using std::cout;
using std::endl;

#include <workflow/WFFacilities.h>

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig number %d is comming\n", num);
    waitGroup.done();
}

void test0()
{
    signal(SIGINT, sighandler);
    waitGroup.wait();

}


int main()
{
    test0();
    return 0;
}

