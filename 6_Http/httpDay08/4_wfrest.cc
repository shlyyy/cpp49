#include <iostream>
using std::cout;
using std::endl;

#include <workflow/WFFacilities.h>
#include <wfrest/HttpServer.h>

using namespace wfrest;


static WFFacilities::WaitGroup waitGroup(1);


void test0()
{
    HttpServer server;

    //简化了解析请求的操作
    server.GET("/hello",[](const HttpReq *, HttpResp * resp){
        //resp->append_output_body("welcome to wfrest");
        resp->String("welcome to wfrest");
    });

    server.GET("/redirect", [](const HttpReq * , HttpResp * resp){
        resp->set_status_code("301");
        //resp->add_header_pair("Location", "/hello");
        resp->headers["Location"] = "/hello";
    });

    server.GET("/query_list",[](const HttpReq * req, HttpResp * ){
        auto queryList = req->query_list();
        for(auto & elem : queryList) {
            printf("%s: %s\n", elem.first.c_str(), elem.second.c_str());
        }
        //获取某一个key对应的value
        printf("%s\n", req->query("username").c_str());

    });

    server.GET("/login",[](const HttpReq *, HttpResp * resp){
        resp->File("./postform.html");
    });


    if(server.track().start(8888) == 0) {
        server.list_routes();
        waitGroup.wait();
    } else {
        cout << "Server start failed" << endl;
    }
}


int main()
{
    test0();
    return 0;
}

