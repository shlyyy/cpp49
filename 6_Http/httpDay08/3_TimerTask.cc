
#include <signal.h>

#include <iostream>

using std::cout;
using std::endl;

#include <workflow/WFFacilities.h>

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig number %d is comming\n", num);
    waitGroup.done();
}

void timerCallback(WFTimerTask * timertask)
{
    cout << "timerCallback is running" << endl;
    //to-do something;
    //通过再次添加一个定时器任务，让其周期性的调用
    auto nextTask = WFTaskFactory::create_timer_task(3000 * 1000, timerCallback);
    series_of(timertask)->push_back(nextTask);
}

void test0()
{
    signal(SIGINT, sighandler);
    auto timerTask = WFTaskFactory::create_timer_task(3000 * 1000, timerCallback);
    timerTask->start();

    waitGroup.wait();
}


int main()
{
    test0();
    return 0;
}

