
#include <signal.h>

#include <iostream>

using namespace std;

#include <workflow/WFFacilities.h>
#include <workflow/MySQLMessage.h>
#include <workflow/MySQLResult.h>
#include <workflow/MySQLUtil.h>

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig number %d is comming\n", num);
    waitGroup.done();
}

void mysqlCallback(WFMySQLTask * mysqltask)
{
    cout << "mysqlCallback is running" << endl;
    //错误的检测
    int state = mysqltask->get_state();
    int error = mysqltask->get_error();
    if(state != WFT_STATE_SUCCESS) {
        printf("occurs error: %s\n", WFGlobal::get_error_string(state, error));
        return;
    }
    //语法的检测
    auto resp = mysqltask->get_resp();
    if(resp->get_packet_type() == MYSQL_PACKET_ERROR) {
        printf("ERROR %d, %s\n", resp->get_error_code(),
               resp->get_error_msg().c_str());
        return;
    }
    cout << "111" << endl;

    protocol::MySQLResultCursor cursor(resp);
    if(cursor.get_cursor_status() == MYSQL_STATUS_OK) {
        //写操作的处理
        printf("Query OK, %lld row affected. Insert id is %lld\n",
               cursor.get_affected_rows(),
               cursor.get_insert_id());
    }
}

void test0()
{
    signal(SIGINT, sighandler);


    string url("mysql://root:1234@localhost");
    //创建任务
    auto mysqlTask = WFTaskFactory::create_mysql_task(url, 1, mysqlCallback);
    //设置任务的属性
    auto req = mysqlTask->get_req();
    req->set_query("INSERT INTO cloudisk.tbl_user_token(user_name, user_token) VALUES('Rose', 'token_abc')");

    //交给框架来运行
    mysqlTask->start();

    waitGroup.wait();

}


int main()
{
    test0();
    return 0;
}

