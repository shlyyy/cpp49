#include <iostream>
using std::cout;
using std::endl;

#include <workflow/WFFacilities.h>
#include <workflow/MySQLResult.h>
#include <wfrest/HttpServer.h>
#include <wfrest/HttpMsg.h>
#include <wfrest/json.hpp>

using namespace wfrest;
using namespace std;


static WFFacilities::WaitGroup waitGroup(1);


void test0()
{
    HttpServer server;

    //简化了解析请求的操作
    server.GET("/hello",[](const HttpReq *, HttpResp * resp){
        //resp->append_output_body("welcome to wfrest");
        resp->String("welcome to wfrest");
    });

    server.GET("/redirect", [](const HttpReq * , HttpResp * resp){
        resp->set_status_code("301");
        //resp->add_header_pair("Location", "/hello");
        resp->headers["Location"] = "/hello";
    });

    server.GET("/query_list",[](const HttpReq * req, HttpResp * ){
        auto queryList = req->query_list();
        for(auto & elem : queryList) {
            printf("%s: %s\n", elem.first.c_str(), elem.second.c_str());
        }
        //获取某一个key对应的value
        printf("%s\n", req->query("username").c_str());

    });

    server.GET("/login",[](const HttpReq *, HttpResp * resp){
        resp->File("./postform.html");
    });

    server.POST("/login",[](const HttpReq * req, HttpResp * ){
        //存储x-www-form-urlencoded数据
        auto & formKV = req->form_kv();
        for(auto & elem : formKV) {
            printf("%s: %s\n", elem.first.c_str(), elem.second.c_str());
        }
    });

    server.POST("/formdata",[](const HttpReq * req, HttpResp * ){
        //存储form-data数据
        auto & formdata = req->form();
        for(auto & elem : formdata) {
            printf("%s: %s -> %s\n", 
                   elem.first.c_str(), 
                   elem.second.first.c_str(), //如果是传递文件，这里是文件名
                   elem.second.second.c_str());
        }
    });

    server.GET("/formdata",[](const HttpReq *, HttpResp * resp){
        resp->File("./index.html");
    });

    server.GET("/series",[](const HttpReq *, HttpResp * resp, SeriesWork * series){
        auto timerTask = WFTaskFactory::create_timer_task(3000 * 1000, [resp](WFTimerTask*){
            resp->String("series timer test");
        });
        series->push_back(timerTask);
    });

    server.GET("/mysqltest0",[](const HttpReq *, HttpResp * resp){
        resp->MySQL("mysql://root:1234@localhost", 
                    "SHOW DATABASES");
    });

    server.GET("/mysqltest1",[](const HttpReq *, HttpResp * resp){
        std::string mysqlurl("mysql://root:1234@localhost");
        std::string sql("select * from cloudisk.tbl_user_token");
        //带Json参数的版本表示结果集存储到了其中
        resp->MySQL(mysqlurl, sql, [resp](Json * json){
            std::string cell = (*json)["result_set"][0]["rows"][0][1];
            resp->String(cell);
        });
    });

    using namespace protocol;
    server.GET("/mysqltest2",[](const HttpReq *, HttpResp * resp){
        std::string mysqlurl("mysql://root:1234@localhost");
        std::string sql("select * from cloudisk.tbl_user_token");
        //带Json参数的版本表示结果集存储到了其中
        resp->MySQL(mysqlurl, sql, [resp](MySQLResultCursor* cursor){
            vector<vector<MySQLCell>> rows;
            cursor->fetch_all(rows);
            resp->String(rows[0][2].as_string());
        });
    });

    server.GET("/mysql_series",[](const HttpReq *, HttpResp * resp, SeriesWork * series){
        std::string mysqlurl("mysql://root:1234@localhost");
        std::string sql("select * from cloudisk.tbl_user_token");
        auto mysqlTask = WFTaskFactory::create_mysql_task(mysqlurl,
                            1, [resp](WFMySQLTask * mysqltask){
            MySQLResultCursor cursor(mysqltask->get_resp());
            vector<vector<MySQLCell>> rows;
            cursor.fetch_all(rows);
            resp->String(rows[1][1].as_string());
        });
        mysqlTask->get_req()->set_query(sql);
        series->push_back(mysqlTask);
    });

    server.GET("/redistest0",[](const HttpReq *, HttpResp * resp){
        string redisurl("redis://127.0.0.1:6379");
        string command("SET");
        vector<string> params{"49wfrest", "yes"};
        //将Redis的结果集全部返回给客户端
        resp->Redis(redisurl, command, params);
    });
    
    server.GET("/redistest1",[](const HttpReq *, HttpResp * resp){
        string redisurl("redis://127.0.0.1:6379");
        string command("GET");
        vector<string> params{"49wfrest"};
        //将Redis的结果集全部返回给客户端
        resp->Redis(redisurl, command, params, [resp](Json * json){
            resp->String((*json)["49wfrest"]);
        });
    });

    if(server.track().start(8888) == 0) {
        server.list_routes();
        waitGroup.wait();
    } else {
        cout << "Server start failed" << endl;
    }
}


int main()
{
    test0();
    return 0;
}

