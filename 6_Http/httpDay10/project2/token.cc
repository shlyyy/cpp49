#include "token.hpp"

#include <openssl/md5.h>

#include <iostream>
using std::cout;
using std::endl;

string Token::getToken() const
{
    string tmp = _username + _salt;

    unsigned char md[16] = {0};
    MD5((const unsigned char *)tmp.c_str(), tmp.size(), md);
    string result;
    char fragment[3] = {0};
    for(int i = 0; i < 16; ++i) {
        sprintf(fragment, "%02x", md[i]);
        result += fragment;
    }
    //获取时间戳
    time_t tm = time(nullptr);
    struct tm * ptm = localtime(&tm);
    char timestamp[20] = {0};
    sprintf(timestamp, "%04d%02d%02d%02d%02d%02d", 
            ptm->tm_year + 1900,
            ptm->tm_mon + 1,
            ptm->tm_mday,
            ptm->tm_hour,
            ptm->tm_min,
            ptm->tm_sec);
    return result.append(timestamp);
}

