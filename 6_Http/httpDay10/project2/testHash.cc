#include "hash.hpp"
#include <iostream>
using std::cout;
using std::endl;

void test0()
{
   string filename("tbl_sql.sql"); 
   Hash hash(filename);
   cout << hash.sha1() << " " << filename << endl;
}


int main()
{
    test0();
    return 0;
}

