
//自定义头文件
//#include "myhead.h"
//
//C的头文件
#include <signal.h>

//C++的头文件
#include <iostream>

using std::cout;
using std::endl;

//第三方库头文件
#include <workflow/WFFacilities.h>

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig number %d is comming\n", num);
    waitGroup.done();
}

void test0()
{
    signal(SIGINT, sighandler);
    waitGroup.wait();

}


int main()
{
    test0();
    return 0;
}

