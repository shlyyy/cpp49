
//自定义头文件
//#include "myhead.h"
//
//C的头文件
#include <signal.h>

//C++的头文件
#include <iostream>

using std::cout;
using std::endl;

//第三方库头文件
#include <workflow/WFFacilities.h>
#include <workflow/WFTaskFactory.h>

static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig number %d is comming\n", num);
    waitGroup.done();
}

void httpCallback(WFHttpTask * httptask)
{
    //当回调函数被运行时，已经完成了数据的接收和发送了
    cout << "httpCallback is running" << endl;
}

void test0()
{
    signal(SIGINT, sighandler);

    //1.创建了一个http任务
    WFHttpTask * httpTask = WFTaskFactory::create_http_task("http://www.baidu.com"
                                    ,1, 1, httpCallback);

    //2. 设置任务的属性
    auto req = httpTask->get_req();
    req->add_header_pair("User-Agent", "workflow http client");

    //3.将任务交给框架来执行,是由框架来调度
    httpTask->start();

    waitGroup.wait();

}


int main()
{
    test0();
    return 0;
}

