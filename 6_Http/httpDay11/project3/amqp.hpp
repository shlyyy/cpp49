#ifndef __WD_amqp_HPP__ 
#define __WD_amqp_HPP__ 

#include <SimpleAmqpClient/SimpleAmqpClient.h>

#include <string>
#include <iostream>
using std::string;
using std::cout;
using std::endl;

struct AmqpInfo
{
    string amqpurl = "amqp://guest:guest@localhost:5672";
    string exchange = "uploader.trans";
    string amqpQue = "uploader.trans.que";
    string routingKey = "oss";
};

class Publisher
{
public:
    Publisher() 
    :
     _channel(AmqpClient::Channel::Create())
    {}

    bool doPublish(const string & msg) {
        AmqpClient::BasicMessage::ptr_t message =
            AmqpClient::BasicMessage::Create(msg);
        _channel->BasicPublish(_info.exchange, _info.routingKey, message);
        return true;
    }

    ~Publisher() {}

private:
    AmqpInfo _info;
    AmqpClient::Channel::ptr_t _channel;
};

class Consumer
{
public:
    Consumer()
    : _channel(AmqpClient::Channel::Create())
    {
        _channel->BasicConsume(_info.amqpQue);
    }

    bool doConsume(string & msg) {
        AmqpClient::Envelope::ptr_t envelop;

        bool flag = _channel->BasicConsumeMessage(envelop, 3000);
        if(flag == false) {
            cout << "consume timeout" << endl;
            return false;
        } else {
            msg = envelop->Message()->Body();
            return true;
        }
    }

private:
    AmqpInfo _info;
    AmqpClient::Channel::ptr_t _channel;
};

#endif

