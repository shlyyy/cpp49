#include "amqp.hpp"
#include "oss.hpp"

#include <iostream>

using std::cout;
using std::endl;

#include <nlohmann/json.hpp>

using Json=nlohmann::json;
void test0()
{
    OssUploader ossUploader;
    Consumer consumer;
    string msg;
    while(1) {
        if(consumer.doConsume(msg)) {
            cout << "msg:\n" << msg << endl;
            //解析消息队列中的数据
            Json uploaderInfo = Json::parse(msg);
            string osskey = uploaderInfo["osskey"];
            string filepath = uploaderInfo["filepath"];

            //执行OSS备份操作
            ossUploader.doUploadFile(osskey, filepath); 
        }
    }
}


int main()
{
    test0();
    return 0;
}

