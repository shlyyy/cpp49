#include "oss.hpp"
#include <iostream>
using std::cout;
using std::endl;


OssUploader::OssUploader()
: _ossinfo()
, _conf()
, _client(_ossinfo.endPoint, _ossinfo.accessKeyId, _ossinfo.accessKeySecret, _conf)
{
    AlibabaCloud::OSS::InitializeSdk();
}


OssUploader::~OssUploader()
{
    AlibabaCloud::OSS::ShutdownSdk();
}

bool OssUploader::doUploadFile(const string & key, const string & filename)
{
    auto outcome = _client.PutObject(_ossinfo.bucketName, key, filename);
    if(!outcome.isSuccess()) {
        std::cout << "PutObject fail" <<
        ",code:" << outcome.error().Code() <<
        ",message:" << outcome.error().Message() <<
        ",requestId:" << outcome.error().RequestId() << std::endl;
        return false;
    } else {
        return true;
    }
}
