#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <SimpleAmqpClient/SimpleAmqpClient.h>

struct AmqpInfo
{
    string amqpurl = "amqp://guest:guest@localhost:5672";
    string exchange = "uploader.trans";
    string amqpQue = "uploader.trans.que";
    string routingKey = "oss";
};

void test_publisher()
{
    AmqpInfo amqpinfo;
    //创建通道
    AmqpClient::Channel::ptr_t channel = AmqpClient::Channel::Create();
    //创建消息对象
    AmqpClient::BasicMessage::ptr_t message = AmqpClient::BasicMessage::Create("hello,rabbitmq");
    //发布消息
    channel->BasicPublish(amqpinfo.exchange, amqpinfo.routingKey, message);
    pause();
}

void test_consumer()
{
    AmqpInfo amqpinfo;
    //创建通道
    AmqpClient::Channel::ptr_t channel = AmqpClient::Channel::Create();
    //指定获取某一个队列中的消息
    channel->BasicConsume(amqpinfo.amqpQue);

    //创建一个信封
    AmqpClient::Envelope::ptr_t envelop;
    //消费消息
    while(1) {
        bool flag = channel->BasicConsumeMessage(envelop, 3000);
        if(flag == false) {
            cout << "get message timeout" << endl;
        } else {
            string msg = envelop->Message()->Body();
            cout << "msg:" << msg << endl;
        }
    }   
}


int main()
{
    /* test_publisher(); */
    test_consumer();
    return 0;
}
