#ifndef __WD_oss_HPP__ 
#define __WD_oss_HPP__ 

#include <string>
using std::string;

#include <alibabacloud/oss/OssClient.h>

struct OssInfo
{
    string accessKeyId = "LTAI5t6CJLdHYEP2vAmRa7PT";
    string accessKeySecret = "BXvRPQBYm8w7DGuJuzjRibKUpIR4eT";
    string endPoint = "oss-cn-hangzhou.aliyuncs.com";
    string bucketName = "bucket-lwh-test";
};

class OssUploader
{
public:
    OssUploader();
    ~OssUploader();

    bool doUploadFile(const string & key, const string & filename);

private:
    OssInfo _ossinfo;
    AlibabaCloud::OSS::ClientConfiguration _conf;
    AlibabaCloud::OSS::OssClient _client;
};

#endif

