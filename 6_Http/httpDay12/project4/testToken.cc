#include "token.hpp"
#include <iostream>
using std::cout;
using std::endl;

void test0()
{
    Token token("Jackie", "12345678");
    cout << "token:" << token.getToken() << endl;
}


int main()
{
    test0();
    return 0;
}

