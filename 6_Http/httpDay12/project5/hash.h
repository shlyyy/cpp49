#ifndef __WD_hash_HPP__ 
#define __WD_hash_HPP__ 

#include <string>
using std::string;

class Hash
{
public:
    Hash(const string & filename) 
    : _filename(filename)
    {}

    string sha1() const;

private:
    string _filename;
};

#endif

