#ifndef __WD_MQ_HPP__ 
#define __WD_MQ_HPP__ 

#include <string>

#include <SimpleAmqpClient/SimpleAmqpClient.h>

using std::string;

struct AmqpInfo{
    string url = "amqp://guest:guest@localhost:5672";
    string exchanger = "uploadserver.trans";
    string ossqueue = "uploadserver.trans.oss";
    string routingkey = "oss";
};

class MessagePublisher
{
public:
    MessagePublisher(const AmqpInfo & info, const string & msg)
    : _info(info)
    , _msg(msg)
    , _channel(AmqpClient::Channel::Create())
    {
    }

    void doPublish()
    {
        AmqpClient::BasicMessage::ptr_t message = AmqpClient::BasicMessage::Create(_msg);
        _channel->BasicPublish(_info.exchanger, _info.routingkey, message);
    }

private:
    AmqpInfo _info;
    string _msg;
    AmqpClient::Channel::ptr_t _channel;
};

class MessageConsumer
{
public:
    MessageConsumer(const AmqpInfo & info)
    : _info(info)
    , _channel(AmqpClient::Channel::Create())
    {
        _channel->BasicConsume(_info.ossqueue);
    }

    bool doConsume(string & msg) {
        AmqpClient::Envelope::ptr_t envelope;
        bool flag = _channel->BasicConsumeMessage(envelope, 3000);
        if(flag == false) {
            printf("consume message timeout\n");
            return false;
        }
        msg = envelope->Message()->Body();
        return true;
    }
private:
    AmqpInfo _info;
    AmqpClient::Channel::ptr_t _channel;
};

#endif

