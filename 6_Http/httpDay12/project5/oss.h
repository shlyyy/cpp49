#ifndef __WD_OssUploader_HPP__ 
#define __WD_OssUploader_HPP__ 

#include <string>
#include <alibabacloud/oss/OssClient.h>

using std::string;

struct OssInfo 
{
    string accessKeyId  = "LTAI5t6CJLdHYEP2vAmRa7PT";
    string accessKeySecret = "BXvRPQBYm8w7DGuJuzjRibKUpIR4eT";
    string endpoint = "oss-cn-hangzhou.aliyuncs.com";
    string bucketname = "bucket-lwh-test";
};

class OssUploader
{
public:
    OssUploader(const OssInfo & info = OssInfo());
        
    ~OssUploader();

    void doUpload(const string & key, const string & filename);
    string genreateDownloadUrl(const string & key);

private:
    OssInfo _ossinfo;
    AlibabaCloud::OSS::ClientConfiguration _conf;
    AlibabaCloud::OSS::OssClient _ossclient;
};

#endif

