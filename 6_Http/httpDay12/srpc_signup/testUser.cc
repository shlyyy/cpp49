#include "user.pb.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;

void test0()
{
    ReqSignup request;
    request.set_username("admin");
    request.set_password("123");

    string serializeStr;
    request.SerializeToString(&serializeStr);
    cout << "serialize:" << serializeStr << endl;
    for(size_t i = 0; i < serializeStr.size(); ++i) {
        printf("%02x", serializeStr[i]);
    }
    cout << endl;
}


int main()
{
    test0();
    return 0;
}

