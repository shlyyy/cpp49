#include <iostream>

using std::cout;
using std::endl;

class Base
{
public:
    Base()
    {

    }

private:
    //3、将基类的拷贝构造函数与赋值运算符函数设置为私有的
    //或者直接删除
    Base(const Base &rhs);
    Base &operator=(const Base &rhs);

};

class Derived
: public Base
{
public:
    Derived()
    {

    }

/* private: */
    //1、可以将拷贝构造函数与赋值运算符函数设置为私有的
    /* Derived(const Derived &rhs); */
    /* Derived &operator=(const Derived &rhs); */

    //2、可以将拷贝构造函数与赋值运算符函数删除
    /* Derived(const Derived &rhs) = delete; */
    /* Derived &operator=(const Derived &rhs) = delete; */

};
void test()
{
    Derived d1;
    Derived d2 = d1;//error

    Derived d3;
    d3 = d1;//error
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

