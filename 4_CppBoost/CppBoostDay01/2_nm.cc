#include <iostream>

using std::cout;
using std::endl;


//不论是C的编译器还是C++的编译器，
//都可以将add函数按照C的方式进行编译
#ifdef __cplusplus
extern "C"
{
#endif 

int add(int x, int y)
{
    return  x + y;
}

#ifdef __cplusplus
}//end of extern "C"

#endif

int main(int argc, char **argv)
{
    return 0;
}

