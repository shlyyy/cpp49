#ifndef __TIMERFD_H__
#define __TIMERFD_H__


#include <functional>

using std::function;

class TimerFd
{
    using TimerFdCallback = function<void()>;

public:
    TimerFd(int initSec, int peridocSec, TimerFdCallback &&cb);
    ~TimerFd();

    //运行与停止
    void start();
    void stop();

private:
    //在该函数中执行read，让A线程执行该函数
    void handleRead();

    //创建_timerfd
    int createTimerFd();
    //创建定时器
    void setTimerFd(int initSec, int peridocSec);

private:
    int _timerfd;//TimerFd系统调用创建的文件描述符
    int _initSec;//定时器的初始时间
    int _peridocSec;//定时器的周期时间
    bool _isStarted;//标识TimerFd是否在运行
    TimerFdCallback _cb;//需要执行的任务
};

#endif
