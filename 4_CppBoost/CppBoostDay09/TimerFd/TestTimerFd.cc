#include "TimerFd.h"
#include "Thread.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

class MyTask
{
public:
    void process()
    {
        cout << "MyTask is running" << endl;
    }
};

void test()
{
    MyTask task;
    TimerFd tfd(1, 6, std::bind(&MyTask::process, &task));

    //自己创建的线程执行handleRead，也就是执行read操作
    Thread th(std::bind(&TimerFd::start, &tfd));
    th.start();

    sleep(30);

    tfd.stop();
    th.join();
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

