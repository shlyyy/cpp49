#ifndef __EVENTFD_H__
#define __EVENTFD_H__

#include <functional>

using std::function;

class EventFd
{
    using EventFdCallback = function<void()>;

public:
    /* EventFd(const EventFdCallback &cb); */
    EventFd(EventFdCallback &&cb);
    ~EventFd();

    //运行与停止
    void start();
    void stop();

    //该函数中执行write，让B线程执行该函数
    void wakeup();
private:
    //在该函数中执行read，让A线程执行该函数
    void handleRead();

    //创建_evfd
    int createEventFd();

private:
    int _evfd;//eventfd系统调用创建的文件描述符
    bool _isStarted;//标识EventFd是否在运行
    EventFdCallback _cb;//需要执行的任务


};

#endif
