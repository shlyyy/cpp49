#include "EventFd.h"
#include "Thread.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

class MyTask
{
public:
    void process()
    {
        cout << "MyTask is running" << endl;
    }
};

void test()
{
    MyTask task;
    EventFd efd(std::bind(&MyTask::process, &task));

    //自己创建的线程执行handleRead，也就是执行read操作
    Thread th(std::bind(&EventFd::start, &efd));
    th.start();

    /* efd.start();//read=====>handleRead */

    int cnt = 20;
    while(cnt-- > 0)
    {
        cout << "cnt = " << cnt << endl;
        efd.wakeup();//主线程执行wakeup，也就是执行write操作
        sleep(1);
    }


    efd.stop();
    th.join();
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

