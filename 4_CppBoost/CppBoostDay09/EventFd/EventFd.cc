#include "EventFd.h"
#include <unistd.h>
#include <poll.h>
#include <stdio.h>
#include <sys/eventfd.h>

#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

EventFd::EventFd(EventFdCallback &&cb)
: _evfd(createEventFd())
, _isStarted(false)
, _cb(std::move(cb))//注册回调函数
{

}

EventFd::~EventFd()
{
    close(_evfd);
}

//运行与停止
void EventFd::start()
{
    struct pollfd pfd;
    pfd.fd = _evfd;
    pfd.events = POLLIN;

    _isStarted = true;

    while(_isStarted)
    {
        int nready = poll(&pfd, 1, 5000);
        if(-1 == nready && errno == EINTR)
        {
            continue;
        }
        else if(-1 == nready)
        {
            cerr << "-1 == nready" << endl;
            return;
        }
        else if(0 == nready)
        {
            cout << ">>poll timeout" << endl;
        }
        else
        {
            if(pfd.revents & POLLIN)
            {
                //如果另外的线程发了通知，handleRead
                //就会被通知，就可以执行任务
                handleRead();
                if(_cb)
                {
                    _cb();//回调函数的执行
                }
            }
        }
    }
}

void EventFd::stop()
{
    _isStarted = false;
}

//该函数中执行write，让B线程执行该函数
void EventFd::wakeup()
{
    uint64_t one = 1;
    ssize_t ret = write(_evfd, &one, sizeof(uint64_t));
    if(ret != sizeof(uint64_t))
    {
        perror("write");
        return;
    }
}
//在该函数中执行read，让A线程执行该函数
void EventFd::handleRead()
{
    uint64_t one = 1;
    ssize_t ret = read(_evfd, &one, sizeof(uint64_t));
    if(ret != sizeof(uint64_t))
    {
        perror("read");
        return;
    }
}
//创建_evfd
int EventFd::createEventFd()
{
    int fd = eventfd(10, 0);//eventfd系统调用创建文件描述符
    if(fd < 0)
    {
        perror("eventfd");
        return fd;
    }
    return fd;
}
