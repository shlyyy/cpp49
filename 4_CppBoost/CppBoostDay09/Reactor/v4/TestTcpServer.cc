#include "ThreadPool.h"
#include "TcpServer.h"
#include "TcpConnection.h"
#include <iostream>

using std::cout;
using std::endl;

ThreadPool *gPool = nullptr;

class MyTask
{
public:
    MyTask(const string &msg, const TcpConnectionPtr &con)
    : _msg(msg)
    , _con(con)
    {

    }
    void process()
    {
        //业务逻辑的处理转接到process函数中，而
        //process是在线程池中进行
        _msg;
        //....
        //...
        //...
        //Q:线程池何时将数据发送给EventLoop/Reactor
        //A:线程池将业务逻辑处理完毕之后就可以发送给EventLoop
        //
        //Q:涉及到线程间通信的问题？
        //A:线程池处理好了数据之后，需要通知EventLoop，告诉EventLoop
        //需要接收数据了
        //
        //Q:线程之间如何通信呢？
        //A：eventfd机制
        _con->sendInLoop(_msg);
        //_con是TcpConnection的对象，需要将数据_msg发送给EventLoop/Reactor
        //那么肯定需要让TcpConnection知道EventLoop的存在，并且为了降低两个
        //类之间的耦合程度，将EventLoop类型的引用或者指针交给TcpConnection
    }
private:
    string _msg;
    TcpConnectionPtr _con;
};

//1、连接建立做的事件
void onNewConnection(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has connected!" << endl;
}

//2、消息到达做的事件
void onMessage(const TcpConnectionPtr &con)
{
    string msg = con->receive();//接收客户端的数据
    cout << ">>recv msg from client " << msg << endl;

    //接收到客户端的msg之后，是可以进行业务逻辑的处理
    MyTask task(msg, con);
    //添加了Task，添加到了线程池里面去了,执行了_taskQue
    //中的push
    gPool->addTask(std::bind(&MyTask::process, task));

    /* con->send(msg);//没有必要 */
}

//3、连接断开做的事件
void onClose(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has closed!" << endl;
}

void test()
{
    ThreadPool pool(4, 10);
    pool.start();//工作线程会阻塞在_taskQue中pop函数，没有任务
    gPool = &pool;

    TcpServer server("127.0.0.1", 8888);
    server.setAllCallback(std::move(onNewConnection)
                          , std::move(onMessage)//此处
                          , std::move(onClose));
    server.start();
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

