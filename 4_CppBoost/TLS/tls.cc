#include <unistd.h>
#include <pthread.h>

#include <iostream>
using std::cout;
using std::endl;


int gNumber = 1;
//__thread变量是每一个线程都有一份独立的拷贝
__thread int tNumber = 10;


void * threadfunc1(void * arg)
{
    tNumber = 11;
    printf("thread1 gNumber: %d, &gNumber: %p\n", gNumber, &gNumber);
    printf("thread1 tNumber: %d, &tNumber: %p\n", tNumber, &tNumber);
}

void * threadfunc2(void * arg)
{
    tNumber = 22;
    printf("thread2 gNumber: %d, &gNumber: %p\n", gNumber, &gNumber);
    printf("thread2 tNumber: %d, &tNumber: %p\n", tNumber, &tNumber);

}

void test0()
{
    pthread_t pthid1, pthid2;
    pthread_create(&pthid1, nullptr, threadfunc1, nullptr);
    pthread_create(&pthid2, nullptr, threadfunc2, nullptr);

    tNumber = 33;
    printf("main thread gNumber: %d, &gNumber: %p\n", gNumber, &gNumber);
    printf("main thread tNumber: %d, &tNumber: %p\n", tNumber, &tNumber);

    sleep(2);
}


int main()
{
    test0();
    return 0;
}

