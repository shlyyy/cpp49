#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyTask
{
public:
    void process(int x)
    {
        while(1)
        {
            cout << "MyTask is isRunning!" << endl;
            sleep(x);
        }
    }
};

void func(int x, int y)
{
    while(1)
    {
        cout << "Func is isRunning!" << endl;
        sleep(x + y);
    }
}

void test()
{
    MyTask task;
    Thread th(bind(&MyTask::process, &task, 1));

    th.start();
    th.join();
}

void test2()
{
    Thread th(bind(&func, 1, 1));

    th.start();
    th.join();
}

int main(int argc, char **argv)
{
    test2();
    return 0;
}

