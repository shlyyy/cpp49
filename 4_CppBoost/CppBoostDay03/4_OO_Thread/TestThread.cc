#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyThread
: public Thread
{
/* public: */
private:
    void run() override
    {
        while(1)
        {
            cout << "MyThread is isRunning!" << endl;
            sleep(1);
        }
    }

    /* void test() */
    /* { */
    /*     run(); */
    /* } */
};

void test()
{
    MyThread myth;
    myth.start();
    /* myth.run(); */
    myth.join();
}

void test2()
{
    /* Thread th;//error,抽象类是不能创建对象的 */
    /* Thread *pth;//ok,抽象类是可以创建该种类型的指针或者引用 */
    /* MyThread *pmyth = new MyThread(); */
    /* Thread *pmyth = new MyThread(); */
    unique_ptr<Thread> pmyth(new MyThread());

    pmyth->start();
    pmyth->join();

    /* delete pmyth; */
}

int main(int argc, char **argv)
{
    test2();
    return 0;
}

