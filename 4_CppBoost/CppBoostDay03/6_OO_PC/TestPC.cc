#include "Producer.h"
#include "Consumer.h"
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

void test()
{
    TaskQueue taskQue(10);
    unique_ptr<Thread> pro(new Producer(taskQue));
    unique_ptr<Thread> pro1(new Producer(taskQue));
    unique_ptr<Thread> con(new Consumer(taskQue));

    //生产者与消费者运行起来
    pro->start();
    pro1->start();
    con->start();

    //让主线程等待生产者与消费者的退出
    pro->join();
    pro1->join();
    con->join();
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

