#include "TaskQueue.h"

TaskQueue::TaskQueue(size_t queSize)
: _queSize(queSize)
, _que()
, _mutex()
, _notEmpty(_mutex)
, _notFull(_mutex)
{

}

TaskQueue::~TaskQueue()
{

}

//任务队列空与满
bool TaskQueue::empty() const
{
#if 0
    if(0 == _que.size())
    {
        return true;
    }
    else
    {
        return false;
    }
#endif
    return 0 == _que.size();
}

bool TaskQueue::full() const
{
    return _queSize == _que.size();
}

//添加任务与执行任务
void TaskQueue::push(const int &value)
{
    //1、先加锁
    _mutex.lock();

    //2、判断是不是满的
    if(full())
    {
        //如果TaskQueue是满的，就不能存任务，
        //生产者线程就在该条件变量上睡眠
        _notFull.wait();
    }
    //3、存任务
    _que.push(value);

    //3.1、生产者生产任务之后，需要唤醒消费者
    _notEmpty.notify();

    //4、释放锁
    _mutex.unlock();
}

int TaskQueue::pop()
{
    //1、加锁
    _mutex.lock();

    //2、是不是空的
    if(empty())
    {
        //如果任务队列为空，那么消费者
        //线程就需要睡眠
        _notEmpty.wait();
    }
    //3、取任务
    int tmp = _que.front();
    _que.pop();

    //3.1、唤醒生产者线程，让其生产任务
    _notFull.notify();

    //4、解锁
    _mutex.unlock();
    return tmp;
}
