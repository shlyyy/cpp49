#include <math.h>
#include <iostream>
#include <string>
#include <memory>

using std::cout;
using std::endl;
using std::string;
using std::unique_ptr;

//面向对象的设计原则：开放闭合原则
//对扩展开放，对修改关闭
//
//抽象类，作为接口使用
class Figure
{
public:
    //纯虚函数
    virtual void display() const = 0;
    virtual double area() const = 0;

    virtual ~Figure(){}
};

class Rectangle
: public Figure
{
public:
    Rectangle(double length = 0, double width = 0)
    : _length(length)
    , _width(width)
    {
        cout << "Rectangle(double = 0, double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Rectangle";
    }

    double area() const override
    {
        return _length * _width;
    }

    ~Rectangle()
    {
        cout << "~Rectangle()" << endl;
    }
private:
    double _length;
    double _width;
};

class Circle
: public Figure
{
public:
    Circle(double radius = 0)
    : _radius(radius)
    {
        cout << "Circle(double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Circle";
    }

    double area() const override
    {
        return 3.14 * _radius *_radius;;
    }

    ~Circle()
    {
        cout << "~Circle()" << endl;
    }
private:
    double _radius;
};

class Triangle
: public Figure
{
public:
    Triangle(double a = 0, double b = 0, double c = 0)
    : _a(a)
    , _b(b)
    , _c(c)
    {
        cout << "Triangle(double = 0, double = 0, double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Triangle";
    }

    double area() const override
    {
        //海伦公式
        double tmp = (_a + _b + _c)/2;

        return sqrt(tmp * (tmp - _a) * (tmp - _b) * (tmp - _c));
    }

    ~Triangle()
    {
        cout << "~Triangle()" << endl;
    }
private:
    double _a;
    double _b;
    double _c;
};

void func(Figure *pfig)
{
    pfig->display();
    cout << "的面积 : " << pfig->area() << endl;
}

//1、满足单一职责原则
//2、满足开放闭合原则
//3、满足依赖倒置原则
//
//缺点：工厂的个数会随着产品的的增加而急剧增加

//工厂方法
//抽象的类
class Factory
{
public:
    //纯虚函数
    virtual Figure *create() = 0;
    virtual ~Factory() {}
};

class RectangleFactory
: public Factory
{
public:
    virtual Figure *create() override
    {
        //将构造函数中的参数放在配置文件中
        //读取配置文件
        //解析配置文件
        //将所需要的数据读出来传到构造函数中
        return new Rectangle(10, 20);
    }
};

class CircleFactory
: public Factory
{
public:
    virtual Figure *create() override
    {
        //将构造函数中的参数放在配置文件中
        //读取配置文件
        //解析配置文件
        //将所需要的数据读出来传到构造函数中
        return new Circle(10);
    }
};

class TriangleFactory
: public Factory
{
public:
    virtual Figure *create() override
    {
        //将构造函数中的参数放在配置文件中
        //读取配置文件
        //解析配置文件
        //将所需要的数据读出来传到构造函数中
        return new Triangle(3, 4, 5);
    }
};

int main(int argc, char **argv)
{
    unique_ptr<Factory> rect(new RectangleFactory());
    unique_ptr<Figure> prec(rect->create());

    unique_ptr<Factory> cir(new CircleFactory());
    unique_ptr<Figure> pcir(cir->create());

    unique_ptr<Factory> tri(new TriangleFactory());
    unique_ptr<Figure> ptri(tri->create());

    func(prec.get());
    func(pcir.get());
    func(ptri.get());

    return 0;
}

