#include "Task.h"
#include "ThreadPool.h"
#include <stdlib.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyTask
: public Task
{
public:
    void process() override
    {
        //业务逻辑
        ::srand(::clock());
        int number = rand()%100;
        cout << "ThreadPool. number = " << number << endl;
    }

};

void test()
{
    unique_ptr<Task> task(new MyTask());
    ThreadPool pool(4, 10);

    pool.start();//四个子线程就会去睡眠

    int cnt = 20;
    while(cnt-- > 0)
    {
        pool.addTask(task.get());
        cout << "cnt = " << cnt << endl;
    }

    pool.stop();
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

