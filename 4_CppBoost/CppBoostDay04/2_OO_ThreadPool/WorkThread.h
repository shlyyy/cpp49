#ifndef __WORKTHREAD_H__
#define __WORKTHREAD_H__

#include "Thread.h"
#include "ThreadPool.h"

class ThreadPool;

class WorkThread
: public Thread
{
public:
    WorkThread(ThreadPool &pool)
    : _pool(pool)
    {

    }
    ~WorkThread()
    {

    }

    //就去执行线程中的doTask函数
    void run() override
    {
        //doTask是线程池中的私有成员函数，所以需要将
        //WorkThread作为ThreadPool的友元
        _pool.doTask();
    }

private:
    ThreadPool &_pool;

};

#endif
