#include "Producer.h"
#include "Consumer.h"
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

void test()
{
    TaskQueue taskQue(10);
    unique_ptr<Thread> pro(new Producer(taskQue));
    unique_ptr<Thread> pro1(new Producer(taskQue));
    unique_ptr<Thread> con(new Consumer(taskQue));

    //生产者与消费者运行起来
    pro->start();
    pro1->start();
    con->start();

    //让主线程等待生产者与消费者的退出
    pro->join();
    pro1->join();
    con->join();
}

void test2()
{
    MutexLock mutex1;

    /* MutexLock mutex2 = mutex1;//error, 拷贝操作 */

    MutexLock mutex3;

    /* mutex3 = mutex1;//error, 赋值操作 */

    TaskQueue taskQue(10);
    Producer pro1(taskQue);
    /* Producer pro2 = pro1;//error,拷贝操作报错 */
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

