#ifndef __CONSUMER_H__
#define __CONSUMER_H__

#include "Thread.h"
#include "TaskQueue.h"

#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <iostream>

using std::cout;
using std::endl;


class Consumer
: public Thread
{
public:
    Consumer(TaskQueue &taskQue)
    : _taskQue(taskQue)
    {
    }

    ~Consumer() {}

    void run() override
    {
        int cnt = 20;
        while(cnt--)
        {
            int tmp = _taskQue.pop();
            cout << "Consumer consume tmp = " << tmp << endl;
            sleep(1);
        }
    }

private:
    TaskQueue &_taskQue;
};
#endif
